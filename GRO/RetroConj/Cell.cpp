/////////////////////////////////////////////////////////////////////////////////////////
//
// gro is protected by the UW OPEN SOURCE LICENSE, which is summarized here.
// Please see the file LICENSE.txt for the complete license.
//
// THE SOFTWARE (AS DEFINED BELOW) AND HARDWARE DESIGNS (AS DEFINED BELOW) IS PROVIDED
// UNDER THE TERMS OF THISS OPEN SOURCE LICENSE (“LICENSE”).  THE SOFTWARE IS PROTECTED
// BY COPYRIGHT AND/OR OTHER APPLICABLE LAW.  ANY USE OF THISS SOFTWARE OTHER THAN AS
// AUTHORIZED UNDER THISS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
//
// BY EXERCISING ANY RIGHTS TO THE SOFTWARE AND/OR HARDWARE PROVIDED HERE, YOU ACCEPT AND
// AGREE TO BE BOUND BY THE TERMS OF THISS LICENSE.  TO THE EXTENT THISS LICENSE MAY BE
// CONSIDERED A CONTRACT, THE UNIVERSITY OF WASHINGTON (“UW”) GRANTS YOU THE RIGHTS
// CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
//
// TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
//
//

#ifndef NOGUI
#include <GroThread.h>
#endif

#include "Micro.h"
#include "Operon.h"
#include <iostream>
#include <list>
#include <vector>
#include <string>
#include "Rands.h"

static int max_id = 0;

Cell::Cell ( World * w ) : world ( w ), gro_program(NULL), marked(false), selected(false) {

    parameters = w->get_param_map();
    compute_parameter_derivatives();
    w->init_actions_map();

    program = w->get_program();
    space = w->get_space();
    body = NULL;

    int i;
    int j;
    for ( i=0; i<MAX_STATE_NUM; i++ ) q[i] = 0;
    for ( i=0; i<MAX_REP_NUM; i++ ) rep[i] = 0;

    divided = false;
    daughter = false;

    num_plasmids = get_param("num_plasmids");
    num_proteins = get_param("num_proteins");
    num_analog_molecules = get_param("num_analog_molecules");

    plasmids = new bool[num_plasmids];
    received_plasmids = new bool[num_plasmids];
    eex = new bool[num_plasmids];
    just_conjugated = new bool[num_plasmids];
    just_conjugated_reset = new bool[num_plasmids];
    analog_molecules_list = new bool[num_analog_molecules];
    //n_plasmids = n;
    plasmidToConjugate = -1;
    conjugationMode = 0;
    markForConj = false;
	nConj = 0;
	
    prot_to_degr=0;
    rna_to_degr=0;

    for ( i=0; i<num_plasmids; i++ )
    {
        plasmids[i] = false;
        received_plasmids[i] = false;
        eex[i] = false;
        just_conjugated[i] = false;
        just_conjugated_reset[i] = false;
    }

    for( i=0; i<num_analog_molecules; i++)
    {
        analog_molecules_list[i] = false;
    }

    internal_proteins = new bool[num_proteins];
    internal_RNAs = new bool[num_proteins];
    proteins_operon = (bool **)malloc(world->num_operons()*sizeof(bool *));
    rna_operon = (bool **)malloc(world->num_operons()*sizeof(bool *));
    last_state = (bool **)malloc(world->num_operons()*sizeof(bool *));
    prot_actv_times = (float **)malloc(world->num_operons()*sizeof(float *));
    prot_degr_times = (float **)malloc(world->num_operons()*sizeof(float *));
    degr_prot = new float[num_proteins];
    rna_actv_times = (float **)malloc(world->num_operons()*sizeof(float *));
    rna_degr_times = (float **)malloc(world->num_operons()*sizeof(float *));
    degr_rna = new float[num_proteins];

    for ( i=0; i<num_proteins; i++ ){
        internal_proteins[i]=false;
        degr_prot[i]=-1;
        internal_RNAs[i]=false;
        degr_rna[i]=-1;
    }

    for ( i=0; i<world->num_operons(); i++ ){
        proteins_operon[i]=(bool *)malloc(num_proteins*sizeof(bool *));
        last_state[i]=(bool *)malloc(num_proteins*sizeof(bool *));
        prot_actv_times[i]=(float *)malloc(num_proteins*sizeof(float *));
        prot_degr_times[i]=(float *)malloc(num_proteins*sizeof(float *));
        rna_operon[i]=(bool *)malloc(num_proteins*sizeof(bool *));
        rna_actv_times[i]=(float *)malloc(num_proteins*sizeof(float *));
        rna_degr_times[i]=(float *)malloc(num_proteins*sizeof(float *));
    }

    for ( i=0; i<world->num_operons(); i++ ){
        for(j=0;j<num_proteins;j++){
            proteins_operon[i][j] = false;
            last_state[i][j] = false;
            prot_actv_times[i][j]=-1;
            prot_degr_times[i][j]=-1;
            rna_operon[i][j] = false;
            rna_actv_times[i][j]=-1;
            rna_degr_times[i][j]=-1;
        }
    }

    cross_input_coefficient = 1.0;
    cross_output_coefficient = 1.0;

    set_id ( max_id++ );

}

Cell::~Cell ( void ) {

    // Falta desasociar del espacio!
    //delete body;
    delete plasmids;
    delete received_plasmids;
    delete eex;
    delete just_conjugated;
    delete just_conjugated_reset;
    delete analog_molecules_list;

    if ( gro_program != NULL ) {
        delete gro_program;
    }

}

bool Cell::hasPlasmid ( int k ) const
{
    return ( this->plasmids[k] );
}

void Cell::setPlasmid ( int k, bool b )
{
    this->plasmids[k] = b;
    //this->eex[k] = b;
}

bool Cell::hasEEX ( int k ) const
{
    return ( this->eex[k] );
}

void Cell::setEEX ( int k, bool b )
{
    this->eex[k] = b;
}

bool Cell::receivedPlasmid ( int k ) const
{
    return ( this->received_plasmids[k] );
}

void Cell::set_receivedPlasmid ( int k, bool v )
{
    this->received_plasmids[k] = v;
}

void Cell::acquirePlasmid( int k )
{
    this ->plasmids[k]=true;
    this->received_plasmids[k] = true;
    //this->eex[k] = true;
}

void Cell::losePlasmid( int k )
{
    int j = 0, p = 0, i = 0;
    bool found = false;
    std::list<int> kProteins;
    std::list<int> kOperons;
    std::list<int>::iterator itKProteins;

    for(p=0; p<get_param("num_proteins"); p++)
    {
        for(j=0; j<world->num_operons(); j++)
        {
            if(world->get_plasmids_matrix(k,j))
            {
                if(world->get_operon(j)->get_output_protein(p))
                {
                        kProteins.push_back(p);
                }
            }
        }
    }

    for(itKProteins = kProteins.begin(); itKProteins != kProteins.end(); itKProteins++)
    {
        for(i=0; i<get_param("num_plasmids"); i++)
        {
            if(plasmids[i] && i != k)
            {
                for(j=0; j<world->num_operons(); j++)
                {
                    if(world->get_plasmids_matrix(i,j))
                    {
                        if(world->get_operon(j)->get_output_protein(*itKProteins))
                        {
                                found = true;
                        }
                    }
                }
                if(!found)
                {
                    for (j=0;j<world->num_operons();j++)
                    {
                        if (world->get_plasmids_matrix(k,j))
                        {
                            if(world->get_operon(j)->get_output_protein(*itKProteins))
                            {
                                   //proteins_operon[j][p] = false;
                                   world->get_operon(j)->degradate(this,j,*itKProteins);
                            }
                        }
                    }
                }
            }
        }
        found = false;
    }


    /*for (j=0;j<world->num_operons();j++)
    {
        if (world->get_plasmids_matrix(k,j))
        {
            for(p=0; p<world->get_param("num_proteins"); p++)
            {
                if(world->get_operon(j)->get_output_protein(p))
                {
                   //proteins_operon[j][p] = false;
                   world->get_operon(j)->degradate(this,j,p);
                }
            }
        }
    }*/


    this->plasmids[k]=false;
    this->received_plasmids[k] = false;
    //this->eex[k] = false;
}

void Cell::jc_reset ( void )
{
    int i = 0;
    for (i = 0; i < this->num_plasmids; i++)
    {
        if( just_conjugated[i] && !just_conjugated_reset[i] )
        {
            just_conjugated_reset[i] = true;
        }
        else if ( just_conjugated[i] && just_conjugated_reset[i] )
        {
            just_conjugated[i] = false;
            just_conjugated_reset[i] = false;
        }
        else
        {
            return;
        }

    }
}

void Cell::set_was_just_conjugated ( int k, bool v )
{
    this->just_conjugated_reset[k] = v;
}

bool Cell::was_just_conjugated ( int k ) const
{
    // return ( (this->just_conjugated_reset[k-1] || this->just_conjugated[k-1]) );
    return ( this->just_conjugated_reset[k] );
}

void Cell::set_conjugated_indicator ( int k, bool b )
{
    this->just_conjugated[k] = b;
}

void Cell::set_nConj ( double n )
{
    this->nConj = n;
}

double Cell::get_nConj ()
{
    return(this->nConj);
}

bool Cell::get_conjugated_indicator ( int k ) const
{
    return this->just_conjugated[k];
}

void Cell::markForConjugation( int k, double nconjgt )
{
    this->plasmidToConjugate = k;
	this->nConj = nconjgt;
    this->conjugationMode = 1;
    this->markForConj = true;
}

bool Cell::markedForConj()
{
	return(this->markForConj);
}

int Cell::get_plasmidToConj()
{
	return(this->plasmidToConjugate);
}

float Cell::get_gt_inst()
{
    return this->gt_inst;
}

//void Cell::conjugate(int n_pl, double cf)
void Cell::conjugate(int n_pl, double n_conj)
{

    double r = fRand(0.00000000000000, 1.000000000000000);
    double p = 0;
    unsigned int size;
    int nc = 4;
    double dete = world->get_sim_dt();
    //double dete = 0.1;
    //printf("r: %lf ", r);
    //printf("Gt inst: %f, dt: %f, \n", this->gt_inst);
    ceBody** neighbors = ceGetNearBodies(this->body,0.5,&size);
    if(size < nc)
    {
        //LO HACE MAL
        //printf("Primer caso: n_conj: %lf, dete: %lf, gt_inst: %f, size: %d, nc: %d\n", n_conj, dete, this->gt_inst, size, nc);
        p = ((n_conj * dete)/(this->gt_inst))*((double)((double)size/(double)nc));
        //p = (double)((cf*dete)*(size/nc));
    }
    else
    {
        //LO HACE BIEN
        //printf("Segundo caso: n_conj: %lf, dete: %lf, gt_inst: %f\n", n_conj, dete, this->gt_inst);
        p = ((n_conj * dete)/(this->gt_inst));
        //p = (double)(cf*dete);
    }
    //printf("p: %lf \n", p);
    if(r < p)
    {
        //printf("Entro... a conjugar...\n");
        int target = rand()%size;
        if(!(((Cell*)(neighbors[target]->data))->marked_for_death()) &&
                !(this->marked_for_death()) && this->hasPlasmid(n_pl))
        {
            ((Cell*)(neighbors[target]->data))->acquirePlasmid(n_pl);
            ((Cell*)(neighbors[target]->data))->set_conjugated_indicator(n_pl, true);
        }
        free(neighbors);

    }
}

void Cell::conjugate_directed(int n_pl, double n_conj)
{

    double r = fRand(0.00000000000000, 1.000000000000000);
    double p = 0;
    unsigned int size;
    int nc = 4;
    //double dete = world->get_sim_dt();
    double dete = 0.1;
    int seen = 0;
    int i = 0;
    //printf("r: %lf ", r);
    //printf("Gt inst: %f, dt: %f, \n", this->gt_inst);
    ceBody** neighbors = ceGetNearBodies(this->body,0.5,&size);

    if(size < nc)
    {
        //LO HACE MAL
        //printf("Primer caso: n_conj: %lf, dete: %lf, gt_inst: %f, size: %d, nc: %d\n", n_conj, dete, this->gt_inst, size, nc);
        p = ((n_conj * dete)/(this->gt_inst))*((double)((double)size/(double)nc));
        //p = (double)((cf*dete)*(size/nc));
    }
    else
    {
        //LO HACE BIEN
        //printf("Segundo caso: n_conj: %lf, dete: %lf, gt_inst: %f\n", n_conj, dete, this->gt_inst);
        p = ((n_conj * dete)/(this->gt_inst));
        //p = (double)(cf*dete);
    }

    if(size > 0)
    {
        int target = rand() % size;
        seen++;
        while((
                  !(((Cell*)(neighbors[target]->data))->marked_for_death()) &&
                  !(this->marked_for_death()) && this->hasPlasmid(n_pl) &&
                  (((Cell*)(neighbors[target]->data))->hasEEX(n_pl))) &&
                  seen < size)
        {
            target = (target + 1) % size;
            seen++;
        }
        if(!(((Cell*)(neighbors[target]->data))->marked_for_death()) &&
                !(this->marked_for_death()) && this->hasPlasmid(n_pl) &&
                !(((Cell*)(neighbors[target]->data))->hasEEX(n_pl)))
        {
            ((Cell*)(neighbors[target]->data))->acquirePlasmid(n_pl);
            ((Cell*)(neighbors[target]->data))->set_conjugated_indicator(n_pl, true);
        }
    }


    //printf("p: %lf \n", p);

    // Completa
    /*if(mode == 0)
    {
        if (this->markForConj)
        {
            this->markForConj = false;
            unsigned int size;
            ceBody** neighbors = ceGetNearBodies(this->body,0.5,&size);
            if(size > 0)
            {
                int target = rand()%size;
                if(!(((Cell*)(neighbors[target]->data))->marked_for_death()) &&
                        !(this->marked_for_death()) &&
                        !(((Cell*)(neighbors[target]->data))->hasEEX(this->plasmidToConjugate)))
                {
                    ((Cell*)(neighbors[target]->data))->acquirePlasmid(this->plasmidToConjugate);
                    ((Cell*)(neighbors[target]->data))->set_conjugated_indicator(this->plasmidToConjugate, true);
                }
            }
            else
            {
                std::cout << size << std::endl;
            }
            free(neighbors);
        }
    }
    else
    {
        int seen = 0;
        int i = 0;
        if (this->markForConj)
        {
            this->markForConj = false;
            unsigned int size;
            ceBody** neighbors = ceGetNearBodies(this->body,0.5,&size);
            if(size > 0)
            {
                int target = rand() % size;
                seen++;
                while((
                          !(((Cell*)(neighbors[target]->data))->marked_for_death()) &&
                          !(this->marked_for_death()) &&
                          (((Cell*)(neighbors[target]->data))->hasEEX(this->plasmidToConjugate))) &&
                      seen < size)
                {
                    target = (target + 1) % size;
                    seen++;
                }
                if(!(((Cell*)(neighbors[target]->data))->marked_for_death()) &&
                        !(this->marked_for_death()) &&
                        !(((Cell*)(neighbors[target]->data))->hasEEX(this->plasmidToConjugate)))
                {
                    ((Cell*)(neighbors[target]->data))->acquirePlasmid(this->plasmidToConjugate);
                    ((Cell*)(neighbors[target]->data))->set_conjugated_indicator(this->plasmidToConjugate, true);
                }
            }
            else
            {
                std::cout << size << std::endl;
            }
        }
    }*/

}

void Cell::set_initial_protein(int p, bool b){//la proteina inicial se tiene que empezar a degradar a no ser que otra la active
    int i,j;
    bool found = false;
    if(b){
        internal_proteins[p]=b;
        for (i=0;i<num_plasmids;i++){ //miramos todos los operones que tenemos para ver cual da esta proteina y actualizar la matriz
            if (plasmids[i]){
                for (j=0;j<world->num_operons();j++){
                    if (world->get_plasmids_matrix(i,j)){
                        if(world->get_operon(j)->get_output_protein(p)){
                            found = true;
                            proteins_operon[j][p] = true;
                            if(world->get_operon(j)->get_tf(p)==-1){
                                last_state[j][p] = true;
                            }
                            prot_degr_times[j][p] = world->get_prot_down_time(p);
                            rna_degr_times[j][p] = world->get_rna_down_time(p);
                            //std::cout<<"Se va a degradar la P"<<p+1<<" del operon"<<j+1<<" en el minuto "<< prot_degr_times[j][p]<<". A no ser que la mantenga activada alguna otra proteina "<<std::endl;
                            //std::cout<<"Se va a degradar el RNA"<<p+1<<" del operon"<<j+1<<" en el minuto "<< rna_degr_times[j][p]<<". A no ser que la mantenga activada alguna otra proteina "<<std::endl;

                        }
                    }
                }
                if(!found){
                    degr_prot[p]= world->get_prot_down_time(p);
                    degr_rna[p]=world->get_rna_down_time(p);
                    prot_to_degr++;
                    internal_proteins[p]=true;
                    //std::cout<<"Se va a degradar la P"<<p+1<<" en el minuto "<< degr_prot[p]<<std::endl;
                    //std::cout<<"Se va a degradar el RNA"<<p+1<<" en el minuto "<< degr_rna[p]<<std::endl;

                }
            }
        }
    }
}

void Cell::set_initial_rna(int p, bool b){//la proteina inicial se tiene que empezar a degradar a no ser que otra la active
    //internal_mRNAs[i]=b;
    int i,j;
    bool found;
    if(b){
        internal_RNAs[p]=b;
        for (i=0;i<num_plasmids;i++){ //miramos todos los operones que tenemos para ver cual da este mrna y actualizar la matriz
            if (plasmids[i]){
                for (j=0;j<world->num_operons();j++){
                    if (world->get_plasmids_matrix(i,j)){
                        if(world->get_operon(j)->get_output_rna(p) || world->get_operon(j)->get_output_protein(p)){ //ya sea porque genera rna o porque genera proteina
                            found = true;
                            rna_operon[j][p] = true;
                            rna_degr_times[j][p] = world->get_rna_down_time(p);
                            //std::cout<<"Se va a degradar el RNA"<<p+1<<" del operon"<<j+1<<" en el minuto "<< rna_degr_times[j][p]<<". A no ser que lo mantenga activado alguna otra proteina "<<std::endl;
                        }
                    }
                }
                if(!found){
                    degr_rna[p]= world->get_rna_down_time(p);
                    rna_to_degr++;
                    internal_RNAs[p]=true;
                    //std::cout<<"Se va a degradar el RNA"<<p+1<<" en el minuto "<< degr_rna[p]<<std::endl;
                }
            }
        }
    }
}


void Cell::set_initial_last_state(){ //para que en los casos de represiones el estado anterior de la proteina sea true, y así entra en la logica
    int i,op,tf;
    for (i=0;i<num_plasmids;i++){
        if (plasmids[i]){
            for (op=0;op<world->num_operons();op++){
                if (world->get_plasmids_matrix(i,op)){
                    for (tf=0;tf<num_proteins;tf++){
                        if(world->get_operon(op)->get_tf(tf)==-1){
                            last_state[op][tf]=true;
                        }
                    }
                }
            }
        }
    }
}

void Cell::update_internal_proteins(){ //Este metodo es para actualizar la lista de proteinas que estan activadas en toda la bacteria
    int i;
    int p;
    for(i=0;i<num_proteins;i++){ //primero pongo la lista entera a false
        internal_proteins[i]=false;
        internal_RNAs[i]=false;
    }

    for(i=0;i<world->num_operons();i++){
        for(p=0;p<num_proteins;p++){
            if(proteins_operon[i][p])
                internal_proteins[p]=internal_proteins[p]||proteins_operon[i][p];
            if(rna_operon[i][p])
                internal_RNAs[p]=internal_RNAs[p]||rna_operon[i][p];
        }
    }
}


void Cell::testProteins() //Comprueba tiempos,cambios en las proteinas internas y mira las proteinas para ver que funcion se ejecuta
{
    int num_operons;
    num_operons = world->num_operons();
    int i,op,pl;
    std::vector<int> result;
    bool * checked = new bool[num_operons];
    int num_checked=0;
    int rnd;

    for (op=0;op<num_operons;op++){
        for (pl=0;pl<num_plasmids;pl++){
            if (plasmids[pl] && world->get_plasmids_matrix(pl,op)){
                //world->get_operon(op)->update(this,op);
                result.push_back(op); //meto los operones que tengo en una lista
                pl=num_plasmids;
            }

        }
        checked[op]=false; //aprovecho el bucle para inicializar este array a false
    }

    while(num_checked<result.size()){ //recorro la lista aleatoriamente
        rnd = rand()%result.size();
        if(!checked[rnd]){
            world->get_operon(result[rnd])->update(this,result[rnd]);
            checked[rnd]=true;
            num_checked++;
        }
    }

    //Mirar las proteinas que se tienen que degradar pero no tienen ningun operon asociado
    if(prot_to_degr>0){
        for(i=0;i<num_proteins;i++){
            if(degr_prot[i]!=-1 && world->get_time() >= degr_prot[i]){ // ya es hora de reprimir la proteina i
                prot_to_degr--;
                degr_prot[i]=-1;
                internal_proteins[i]=false;
            }
        }
    }

    check_action();
}

bool Cell::can_express_protein(int i){
    //consulta la matriz de moleculas para saber si el tf puede unirse al promotor
    // si hay algun 1 en una columna entonces solo puede expresarse si esta esa molecula
    // si hay algun -1 puede expresarse siempre y cuando esa molecula no este
    // si son todo 0, puede expresarse

    bool result=false;
    int k=0;
    bool found = false;
    bool need_inductor=false;

    while(!found && k<world->get_param("num_molecules")){
        if(world->get_molecules_matrix(k,i)==1){
            if(world->get_molecule(k)){
                return true;
            }
            need_inductor=true;
        }
        k++;
    }

    while(!found && k<world->get_param("num_analog_molecules")){
        if(world->get_analog_molecules_matrix(k,i)==1){
            if(this->get_analog_molecule(k)){
                return true;
            }
            need_inductor=true;
        }
        k++;
    }

    if(!need_inductor && !found){
        result = true;
        for(k=0;k<world->get_param("num_molecules");k++){
            if(world->get_molecule(k) && world->get_molecules_matrix(k,i)==-1){
                return false;
            }
        }
    }

    if(!need_inductor && !found){
        result = true;
        for(k=0;k<world->get_param("num_analog_molecules");k++){
            if(this->get_analog_molecule(k) && world->get_analog_molecules_matrix(k,i)==-1){
                return false;
            }
        }
    }

    return result;
}

int Cell::check_gen_condition(int *cond)
{
    int i = 0;
    int result = 1;
    bool prot_state;
    for(i = 0; i<num_proteins; i++)
    {
        prot_state = this->get_internal_protein(i);
        switch(cond[i])
        {
            case -1:
                if(prot_state)
                {
                    result = 0;
                    i = num_proteins+20;
                }
                break;
            case 1:
                if(!prot_state)
                {
                    result = 0;
                    i = num_proteins+20;
                }
                break;
            default:
                break;
        }
    }
    return result;
}

void Cell::print_state()
{
/*
    int i;
    for(i=0;i<num_proteins;i++){
        if(i==num_proteins-1)
            std::cout<<internal_proteins[i]<<" ]"<<std::endl;
        else if (i==0)
            std::cout<<"proteinas: [t="<<world->get_time()<<"| "<<internal_proteins[i]<<" ,";
        else
            std::cout<<internal_proteins[i]<<" ,";
    }

    for(i=0;i<num_proteins;i++){
        if(i==num_proteins-1)
            std::cout<<internal_RNAs[i]<<" ]"<<std::endl;
        else if (i==0)
            std::cout<<"RNAs: [t="<<world->get_time()<<"| "<<internal_RNAs[i]<<" ,";
        else
            std::cout<<internal_RNAs[i]<<" ,";
    }

    for(int j =0;j<world->get_num_actions();j++){
        for(int i=0;i<num_proteins;i++){
            if(i==num_proteins-1)
                std::cout<<world->get_prot_action_matrix(j,i)<<" ]"<<std::endl;
            else if (i==0)
                std::cout<<"matriz act [ "<<world->get_num_actions()<<"|"<<world->get_prot_action_matrix(j,i)<<" ,";
            else
                std::cout<<world->get_prot_action_matrix(j,i)<<" ,";
        }
    }
*/

}


void Cell::check_action() // p es el indice de la proteina que acaba de ponerse a true
{
    bool result;
    int num_actions = world->get_num_actions();
    for(int row=0;row<num_actions;++row){
        result = true;
        for(int col=0; col<num_proteins;++col){
            /*if(world->get_prot_action_matrix(row,col) && !internal_proteins[col]){
                col = num_proteins;
                result=false;
            }*/
            if((world->get_prot_action_matrix(row,col) == -1 && internal_proteins[col]) ||
                 (world->get_prot_action_matrix(row,col) == 1 && !internal_proteins[col]))
            {
                    col = num_proteins;
                    result = false;
            }
        }
        if(result){
            FnPointer fp = world->get_action(world->get_action_name(row));
            std::list<std::string> pl = world->get_action_param(row);
            ((*this).*fp)(pl);
        }
        /*if(result){
            FnPointer fp = world->get_action(world->get_action_name(row));
            if(die_name.compare(world->get_action_name(row)) != 0)
            {
                std::list<std::string> pl = world->get_action_param(row);
                ((*this).*fp)(pl);
            }
            else
            {
                ((*this).*fp)();
            }

        }*/

    }
}


void Cell::paint_from_list(std::list<string> ls){
    std::list<std::string>::iterator i = ls.begin();
    int gfp = std::atoi((*i).c_str());i++;
    int rfp = std::atoi((*i).c_str());i++;
    int yfp = std::atoi((*i).c_str());i++;
    int cfp = std::atoi((*i).c_str());
    this->set_rep(GFP,gfp);
    this->set_rep(RFP,rfp);
    this->set_rep(YFP,yfp);
    this->set_rep(CFP,cfp);
    //std::cout<< "GFP"<<std::endl;
}

void Cell::delta_paint_from_list(std::list<string> ls){
    std::list<std::string>::iterator i = ls.begin();
    int gfp = 0, rfp = 0, yfp = 0, cfp = 0;
    int d_gfp = std::atoi((*i).c_str());i++;
    int d_rfp = std::atoi((*i).c_str());i++;
    int d_yfp = std::atoi((*i).c_str());i++;
    int d_cfp = std::atoi((*i).c_str());
    gfp = this->get_rep(GFP);
    rfp = this->get_rep(RFP);
    yfp = this->get_rep(YFP);
    cfp = this->get_rep(CFP);
    if(gfp + d_gfp < 0)
    {
        gfp = 0;
    }
    else
    {
        gfp = gfp + d_gfp;
    }
    if(rfp + d_rfp < 0)
    {
        rfp = 0;
    }
    else
    {
        rfp = rfp + d_rfp;
    }
    if(yfp + d_yfp < 0)
    {
        yfp = 0;
    }
    else
    {
        yfp = yfp + d_yfp;
    }
    if(cfp + d_cfp < 0)
    {
        cfp = 0;
    }
    else
    {
        cfp = cfp + d_cfp;
    }
    this->set_rep(GFP,gfp);
    this->set_rep(RFP,rfp);
    this->set_rep(YFP,yfp);
    this->set_rep(CFP,cfp);
    //std::cout<< "GFP"<<std::endl;
}


void Cell::set_eex_from_list(std::list<string> ls)
{
    std::list<std::string>::iterator i = ls.begin();
    std::map<std::string,int> plasmids_Map = this->world->get_plasmids_map();
    std::string plasmid_to_eex ((*i).c_str());
    int j = plasmids_Map[plasmid_to_eex];
    if(j>=0)
    {
        this->setEEX(j,true);
    }
}

void Cell::remove_eex_from_list(std::list<string> ls)
{
    std::list<std::string>::iterator i = ls.begin();
    std::map<std::string,int> plasmids_Map = this->world->get_plasmids_map();
    std::string plasmid_to_eex ((*i).c_str());
    int j = plasmids_Map[plasmid_to_eex];
    if(j>=0)
    {
        this->setEEX(j,false);
    }
}

void Cell::conjugate_from_list(std::list<std::string> ls){
    std::list<std::string>::iterator i = ls.begin();
    std::map<std::string,int> plasmids_Map = this->world->get_plasmids_map();
    std::string plasmid_to_conj ((*i).c_str());
    //int j = (std::atoi((*i).c_str()))-1;
    int j = plasmids_Map[plasmid_to_conj];
    i++;
    double n_conj = std::atof((*i).c_str());
    //double d_n_conj = (double)n_conj;
    //print_state();
    conjugate(j, n_conj);
    //std::cout<< "Funcion conjugate con parametro ("<<j<<")."<<std::endl;
}

void Cell::conjugate_directed_from_list(std::list<std::string> ls){
    std::list<std::string>::iterator i = ls.begin();
    std::map<std::string,int> plasmids_Map = this->world->get_plasmids_map();
    std::string plasmid_to_conj ((*i).c_str());
    //int j = (std::atoi((*i).c_str()))-1;
    int j = plasmids_Map[plasmid_to_conj];
    i++;
    double n_conj = std::atof((*i).c_str());
    //double d_n_conj = (double)n_conj;
    //print_state();
    conjugate_directed(j, n_conj);
    //std::cout<< "Funcion conjugate con parametro ("<<j<<")."<<std::endl;
}

void Cell::lose_plasmid_from_list(std::list<std::string> ls){
    std::list<std::string>::iterator i = ls.begin();
    std::map<std::string,int> plasmids_Map = this->world->get_plasmids_map();
    std::string plasmid_to_lose ((*i).c_str());
    //int j = (std::atoi((*i).c_str()))-1;
    int j = plasmids_Map[plasmid_to_lose];
    if(j>=0)
    {
        //this->setPlasmid(j-1,false);
        this->losePlasmid(j);
    }
    //print_state();
    //std::cout<< "Funcion lose_plasmid con parametros ("<<j<<", "<<k<<", "<<l<<")."<<std::endl;
}

void Cell::die_from_list(std::list<std::string> ls){
    std::list<std::string>::iterator i = ls.begin();
    int j = std::atoi((*i).c_str());
    //print_state();
    if ( this != NULL && !this->marked_for_death())
        this->mark_for_death();
    else
        printf ( "Warning: Called die() from outside a cell program. No action taken\n" );
    //std::cout<< "Die!!"<<std::endl;
}

/*void Cell::die_from_list()
{
    //print_state();
    if ( this != NULL && !this->marked_for_death())
        this->mark_for_death();
    else
        printf ( "Warning: Called die() from outside a cell program. No action taken\n" );
    //std::cout<< "Die!!"<<std::endl;
}*/



void Cell::conj_and_paint_from_list(std::list<std::string> ls){
    std::list<std::string>::iterator i = ls.begin();
    std::map<std::string,int> plasmids_Map = this->world->get_plasmids_map();
    std::string plasmid_to_conj ((*i).c_str());
    //int j = (std::atoi((*i).c_str()))-1;
    int j = plasmids_Map[plasmid_to_conj];
    i++;
    double n_conj = std::atof((*i).c_str());
    i++;
    //double d_n_conj = (double)n_conj;
    //print_state();
    conjugate(j, n_conj);
    int gfp = std::atoi((*i).c_str());i++;
    int rfp = std::atoi((*i).c_str());i++;
    int yfp = std::atoi((*i).c_str());i++;
    int cfp = std::atoi((*i).c_str());
    this->set_rep(GFP,gfp);
    this->set_rep(RFP,rfp);
    this->set_rep(YFP,yfp);
    this->set_rep(CFP,cfp);
}

void Cell::change_gt_from_list(std::list<std::string> ls){
    std::list<std::string>::iterator i = ls.begin();
    float growth_rate = std::atof((*i).c_str());
    //print_state();
    this->set_param( "ecoli_growth_rate", growth_rate);
    //std::cout<< "Funcion conjugate con parametro ("<<j<<")."<<std::endl;
}

void Cell::emit_cross_feeding_signal_from_list(std::list<std::string> ls)
{
    std::list<std::string>::iterator i = ls.begin();
    int signal_id = std::atoi((*i).c_str());
    float signal_conc = this->world->get_cross_feeding_max_conc(signal_id);
    float dv_max = 0, dv = 0;
    /*******/
    //float coef = 0;
    //Otra cosa: solucion parche - Randomio es una variable aleatoria [0...1] que distribuye exponencial.
    //Es aprox 0.7 TOPS, en la practica el max debiera ser 1! No he visto ninguno pasar de 0.70
    //Segun la wikipedia, la media de una distribucion exponencial es 1/lambda, en este caso: dt, por lo que estoy ahora
    //multiplicando por ese valor.
    //dv_max = get_param ( "ecoli_growth_rate" ) * this->get_volume() * this->world->get_sim_dt();
    dv_max = get_param ( "ecoli_growth_rate_max" ) * this->get_volume() * this->world->get_sim_dt();
    //cout << "dv_max: " << dv_max << endl;
    dv = this->get_d_vol();
    //cout << "dv: " << dv << endl;
    this->cross_output_coefficient = dv/dv_max;
    if(this->cross_output_coefficient > 1.0)
    {
        this->cross_output_coefficient = 1.0;
    }
    //cout << "ratio: " << coef << endl;
    this->world->emit_signal(this,signal_id,(this->cross_output_coefficient*signal_conc));
    //cout << "final signal output: " << coef*signal_conc << endl;
    //print_state();
}

void Cell::get_cross_feeding_signal_from_list(std::list<std::string> ls)
{
    std::list<std::string>::iterator i = ls.begin();
    int signal_id = std::atoi((*i).c_str()); i++;
    int benefit = std::atoi((*i).c_str());
    float signal_level = world->get_signal_value ( this, signal_id );
    float growth_rate = get_param( "ecoli_growth_rate_max" );
    /*********/
    //float coefficient = 0;

    switch(benefit)
    {
        //Aqui es donde los valores de lectura son muy bajos, OJO!
        case 1:
            this->cross_input_coefficient = (signal_level/(this->world->get_cross_feeding_max_conc(signal_id)));
            break;
        case -1:
            this->cross_input_coefficient = (1 - (signal_level/(this->world->get_cross_feeding_max_conc(signal_id))));
            break;
        default:
            this->cross_input_coefficient = 1;
            break;
    }
    if(this->cross_input_coefficient > 1)
    {
        this->cross_input_coefficient = 1;
        //cout << "ALERT!!! COEF > 1!!!" << endl;
    }
    else if(this->cross_input_coefficient < 0)
    {
        this->cross_input_coefficient = 0;
        //cout << "ALERT!!! COEF < 0!!!" << endl;
    }

    if (growth_rate > 0.0346575 || this->cross_input_coefficient > 1 || this->cross_input_coefficient < 0)
    {
        cout << "ALERT!!!! - GET_CF_FUNCTION: growth_rate: " << growth_rate << ", coefficient: " << this->cross_input_coefficient << endl;
    }

    /*growth_rate = coefficient*growth_rate;
    this->set_param( "ecoli_growth_rate", growth_rate);*/
}

void Cell::state_to_file(FILE *fp)
{
    int i = 0;
    fprintf ( fp, "%f, %d, %f, %f, %f, %f, %f, %d, %d, %d, %d, ",
              this->world->get_time(),
              this->get_id(), this->get_x(), this->get_y(), this->get_theta(), this->get_volume(), this->get_gt_inst(),
              this->get_rep(GFP), this->get_rep(RFP), this->get_rep(YFP), this->get_rep(CFP));

    for(i = 0; i<(num_proteins-1); i++)
    {
        fprintf ( fp, "%d, ", this->get_internal_protein(i));
    }
    fprintf ( fp, "%d\n", this->get_internal_protein(i));
}
