/////////////////////////////////////////////////////////////////////////////////////////
//
// gro is protected by the UW OPEN SOURCE LICENSE, which is summarized here.
// Please see the file LICENSE.txt for the complete license.
//
// THE SOFTWARE (AS DEFINED BELOW) AND HARDWARE DESIGNS (AS DEFINED BELOW) IS PROVIDED
// UNDER THE TERMS OF THISS OPEN SOURCE LICENSE (“LICENSE”).  THE SOFTWARE IS PROTECTED
// BY COPYRIGHT AND/OR OTHER APPLICABLE LAW.  ANY USE OF THISS SOFTWARE OTHER THAN AS
// AUTHORIZED UNDER THISS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
//
// BY EXERCISING ANY RIGHTS TO THE SOFTWARE AND/OR HARDWARE PROVIDED HERE, YOU ACCEPT AND
// AGREE TO BE BOUND BY THE TERMS OF THISS LICENSE.  TO THE EXTENT THISS LICENSE MAY BE
// CONSIDERED A CONTRACT, THE UNIVERSITY OF WASHINGTON (“UW”) GRANTS YOU THE RIGHTS
// CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
//
// TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
//
//

#ifndef CELL_H
#define CELL_H


#include "CellEngine.h"
#include "CECellSignal.h"
#include "ccl.h"
#include "Defines.h"
#include "Utility.h"
#include "Operon.h"

#ifndef NOGUI
#include "Theme.h"
#include "GroPainter.h"
#endif

#include <iostream>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <list>
#include <vector>
#include <string>
#include <map>


class World;
class MicroProgram;


////////////////////////////////////////////////////////////////////////////////
// Cell
//

class Cell {

public:

    Cell (World * w);
    ~Cell ( void );

    void init ( const int * q0, const int * rep0, float frac );

    inline void set ( int i, int x ) { q[i] = x; }
    inline int get ( int i ) { return q[i]; }
    inline int * get_q ( void ) { return q; }
    inline int get_rep ( int i ) { return rep[i]; }

    inline void inc ( int i, int x ) { q[i] += x; }
    inline void dec ( int i, int x ) { q[i] -= x; }

    inline void inc ( int i ) { q[i]++; }
    inline void dec ( int i ) { q[i]--; }

    inline void set_rep ( int i, int x ) { rep[i] = x; }

    inline void select ( void ) { selected = true; }
    inline void deselect ( void ) { selected = false; }
    inline bool is_selected ( void ) { return selected; }

    void set_prog ( MicroProgram * p ) { program = p; }

    float get_x ( void ) { return body->center.x; }
    float get_y ( void ) { return body->center.y; }
    float get_vec_x ( void ) { return body->halfDimensionAABB.x; }
    float get_vec_y ( void ) { return body->halfDimensionAABB.y; }
    float get_theta ( void ) { return body->rotation; }

    virtual void update ( void ) {}
    virtual Value * eval ( Expr * ) { return NULL; }

#ifndef NOGUI
    virtual void render ( Theme *, GroPainter * ) {}
#endif

    virtual Cell * divide ( void ) { return NULL; }

    ceBody * get_body ( void ) { return body; }

    void set_id ( int i ) { id = i; }
    int get_id ( void ) { return id; }

    virtual float get_fluorescence ( int i ) { return rep[i]; }

    virtual float get_size ( void ) { return 1; }
    virtual float get_volume ( void ) { return 0.000001; }
    virtual float get_length ( void ) {}

    Program * get_gro_program ( void ) { return gro_program; }
    void set_gro_program ( Program * p ) { gro_program = p; }

    // getters and setters for parameters ////////////
    inline void set_param ( std::string str, float val ) { parameters[str] = val; }
    inline float get_param ( std::string str ) { return parameters[str]; }
    inline std::map<std::string,float> get_param_map ( void ) { return parameters; }
    inline void set_param_map ( std::map<std::string,float> p ) { parameters = p; }

    virtual void compute_parameter_derivatives ( void ) {  } // To avoid computing simple functions of parameters
    // over and over again, this function is called whenever gro
    // changes a parameter
    // end parameters ///////////////////////////////

    void mark_for_death ( void )
    {
        marked = true;
        //ceDestroyBody(this->get_body()) ;
    }
    //inline void mark_for_death ( void ) { marked = true; }
    inline bool marked_for_death ( void ) { return marked; }

    inline void set_division_indicator ( bool val ) { divided = val; }
    inline void set_daughter_indicator ( bool val ) { daughter = val; }

    virtual bool get_force_divide ( void ) { return false; }
    virtual void set_force_divide ( bool ) {}

    virtual int get_div_count ( void ) { return 0; }
    virtual void set_div_count ( int ) {}

    inline bool just_divided ( void ) { return divided; }
    inline bool is_daughter ( void ) { return daughter; }

    bool hasPlasmid ( int ) const;
    bool hasEEX (int) const;
    void setPlasmid ( int, bool );
    void setEEX ( int, bool );
    bool receivedPlasmid ( int ) const;
    void set_receivedPlasmid ( int, bool );
    void acquirePlasmid ( int );
    void losePlasmid ( int );
    void jc_reset ( void );
    void set_was_just_conjugated ( int, bool );
    bool was_just_conjugated ( int ) const;
    void set_conjugated_indicator ( int, bool );
    bool get_conjugated_indicator ( int ) const;
    inline bool * getPlasmids ( void ) { return plasmids; }
    inline int nPlasmids ( void ) { return num_plasmids; }
    inline int conjMode ( void ) { return conjugationMode; }
    //inline bool was_just_conjugated ( int n ) { return just_conjugated_reset[n]; }
    //inline void set_conjugated_indicator ( int n, bool val ) { just_conjugated[n] = val; }

    inline float get_d_vol ( void ) { return d_vol; }
    inline float get_available( void ) { return my_available; }

    virtual void force_divide ( void ) {}

    void markForConjugation (int , int mode);
    void conjugate(int, double);
    void transfer_plasmid(int);
    void conjugate_directed(int, double);

    float get_gt_inst(void);

    void set_initial_protein(int, bool);
    void set_initial_last_state(void);

    void set_initial_rna(int, bool);

    bool get_internal_protein(int i) {return internal_proteins[i];}
    void set_internal_protein(int i,bool b) {internal_proteins[i]=b;}

    bool get_internal_RNA(int i) {return internal_RNAs[i];}
    void set_internal_RNA(int i,bool b) {internal_RNAs[i]=b;}

    bool get_protein_operon(int op,int p) {return proteins_operon[op][p];}
    void set_protein_operon(int op,int p,bool b) {proteins_operon[op][p]=b;}

    bool get_rna_operon(int op,int p) {return rna_operon[op][p];}
    void set_rna_operon(int op,int p,bool b) {rna_operon[op][p]=b;}

    void activate_protein(int op, int i) {proteins_operon[op][i]=true;}
    void repress_protein(int op, int i) {proteins_operon[op][i]=false;}

    void activate_rna(int op, int i) {rna_operon[op][i]=true;}
    void repress_rna(int op, int i) {rna_operon[op][i]=false;}

    bool get_last_state(int op,int p) {return last_state[op][p];}
    void set_last_state(int op,int p) { last_state[op][p]=internal_proteins[p];}

    void set_last_state_value(int op,int p,int i) { last_state[op][p]=i;}

    bool get_last_state_rna(int op,int p) {return last_state_rna[op][p];}
    void set_last_state_rna(int op,int p) { last_state_rna[op][p]=internal_RNAs[p];}

    void set_last_state_value_rna(int op,int p,int i) { last_state_rna[op][p]=i;}

    void set_prot_degr_time(int op,int i,double t){prot_degr_times[op][i]=t;}
    double get_prot_degr_time(int op,int i){return prot_degr_times[op][i];}

    void set_prot_actv_time(int op,int i,double t){prot_actv_times[op][i]=t;}
    double get_prot_actv_time(int op,int i){return prot_actv_times[op][i];}

    void set_rna_degr_time(int op,int i,double t){rna_degr_times[op][i]=t;}
    double get_rna_degr_time(int op,int i){return rna_degr_times[op][i];}

    void set_rna_actv_time(int op,int i,double t){rna_actv_times[op][i]=t;}
    double get_rna_actv_time(int op,int i){return rna_actv_times[op][i];}

    void set_num_proteins(int n){num_proteins = n;}
    int get_num_proteins(){return num_proteins;}

    void set_degr_prot(int i, float v){degr_prot[i] = v;}
    float get_degr_prot(int i){return degr_prot[i];}

    void set_degr_rna(int i, float v){degr_rna[i] = v;}
    float get_degr_rna(int i){return degr_rna[i];}

    void set_analog_molecule(int i, int val) {analog_molecules_list[i]=val;}
    bool get_analog_molecule(int i) {return analog_molecules_list[i];}

    int check_gen_condition(int *);
    int check_plasmid_condition(int *);

    void update_internal_proteins(void);

    bool can_express_protein(int i);

    bool ribo_is_open(int);

    void testProteins();

    void print_state();

    void check_action();
    
    float get_n_cells_d(float);
    float area_concentration(int, float);

    void paint_from_list(std::list<std::string> ls);
    void conjugate_from_list(std::list<std::string> ls);
    void conjugate_directed_from_list(std::list<std::string> ls);
    void lose_plasmid_from_list(std::list<string> ls);
    void set_eex_from_list(std::list<string> ls);
    void remove_eex_from_list(std::list<string> ls);
    void die_from_list(std::list<string> ls);//{this->mark_for_death();}
    //void die_from_list();
    void conj_and_paint_from_list(std::list<std::string> ls);
    void delta_paint_from_list(std::list<string> ls);
    void change_gt_from_list(std::list<std::string> ls);
    void emit_cross_feeding_signal_from_list(std::list<std::string> ls);
    void get_cross_feeding_signal_from_list(std::list<std::string> ls);


    void s_absorb_signal_area(std::list<std::string> ls);
    void s_absorb_signal(std::list<std::string> ls);
    void s_emit_signal_area(std::list<std::string> ls);
    void s_emit_signal(std::list<std::string> ls);
    void s_get_signal_area(std::list<std::string> ls);
    void s_get_signal(std::list<std::string> ls);
    void s_absorb_QS(std::list<std::string> ls);
    /*void s_set_signal_multiple(std::list<std::string> ls);*/
    void s_set_signal(std::list<std::string> ls);
    void s_set_signal_rect(std::list<std::string> ls);
    void s_emit_cross_feeding_signal_from_list(std::list<std::string> ls);
    void s_get_cross_feeding_signal_from_list(std::list<std::string> ls);
    void s_absorb_cross_feeding_signal_from_list(std::list<std::string> ls);


    void state_to_file(FILE *fp);

protected:

    ceSpace * space;
    ceBody * body;

    int q[MAX_STATE_NUM],
    rep[MAX_REP_NUM];

    World * world;
    MicroProgram * program;
    Program * gro_program;
    int id;

    // these ought to be gotten rid of
    float growth_rate,
    old_growth_rate,
    division_size_mean,
    division_size_variance;

    int num_plasmids;
    int num_proteins;
    int num_analog_molecules;
    int num_ribos;
    //Como un long visto como bit??? (int32) -> mas eficiente
    bool * plasmids; //lista para saber que plasmidos tengo
    bool * received_plasmids;
    bool * eex;
    bool * just_conjugated;
    bool * just_conjugated_reset;
    int plasmidToConjugate;
    int conjugationMode;

    //proteins_operons:
    //porque puede ser que un operon exprese P1 y otro reprima P1 a la vez. El primer operon deberia seguir expresando, pero tal y como esta ahora si el que
    // reprime llega mas tarde, entonces se reprimiria y nos quedariamos sin P1.
    //last_state:
    //necesito que sea matriz porque si hay varios operones que generan la misma proteina, el last_State cambia y el segundo operon coge un last_State erroneo
    //activ y degr_times:
    //tambien tienen que ser matrices porque si varios operones activan o reprimen la misma proteina puede que se active o reprima en el operon incorrecto
    //porque se chequea antes el tiempo.

    bool ** proteins_operon; //matriz con las proteinas que expresa cada operon
    bool ** rna_operon; //matriz con las proteinas que expresa cada operon
    bool * internal_proteins; // lista con las proteinas que tiene la celula. Se cogen por columnas de proteins_by_operon
    bool * internal_RNAs;
    bool * ribo_open;
    bool ** last_state; //matriz con los ultimos estados de cada proteina por operon
    bool ** last_state_rna; //matriz con los ultimos estados de cada RNA por operon
    float ** prot_actv_times; // tiempos para que suba la expresion de proteinas
    float ** prot_degr_times; // tiempos para que baje la expresion de proteinas
    float ** rna_actv_times; // tiempos para que suba la expresion de RNAs
    float ** rna_degr_times;// tiempos para que baje la expresion de RNAs
    float * degr_prot; // tiempos para que baje la expresion de las proteinas que no tienen ningun operon que las generen
    float * degr_rna;
    int prot_to_degr;
    int rna_to_degr;

    bool * analog_molecules_list; // Que moleculas se leen desde la bacteria... esto esta pensado para senales analogicas

    bool marked, divided, daughter, selected;
    bool markForConj;

    std::map<std::string,float> parameters;

    float in_vol, gt_inst;
    float d_vol;
    float my_available;
    float monod;

    float cross_output_coefficient;
    float cross_input_coefficient;

};

#endif // CELL_H
