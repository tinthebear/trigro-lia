/////////////////////////////////////////////////////////////////////////////////////////
//
// gro is protected by the UW OPEN SOURCE LICENSE, which is summarized here.
// Please see the file LICENSE.txt for the complete license.
//
// THE SOFTWARE (AS DEFINED BELOW) AND HARDWARE DESIGNS (AS DEFINED BELOW) IS PROVIDED
// UNDER THE TERMS OF THISS OPEN SOURCE LICENSE (“LICENSE”).  THE SOFTWARE IS PROTECTED
// BY COPYRIGHT AND/OR OTHER APPLICABLE LAW.  ANY USE OF THISS SOFTWARE OTHER THAN AS
// AUTHORIZED UNDER THISS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
//
// BY EXERCISING ANY RIGHTS TO THE SOFTWARE AND/OR HARDWARE PROVIDED HERE, YOU ACCEPT AND
// AGREE TO BE BOUND BY THE TERMS OF THISS LICENSE.  TO THE EXTENT THISS LICENSE MAY BE
// CONSIDERED A CONTRACT, THE UNIVERSITY OF WASHINGTON (“UW”) GRANTS YOU THE RIGHTS
// CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
//
// TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
//
//

#include "EColi.h"
#include "Programs.h"
#include "Operon.h"
#include <iostream>
#include <math.h>
#include <limits>

#ifdef _WIN32
#include "Rands.h"
#endif //_WIN32

using namespace std;

#define FMULT 0.125

void EColi::compute_parameter_derivatives ( void ) {

    lambda = sqrt ( 10 * get_param ( "ecoli_growth_rate" ) * get_param ( "ecoli_growth_rate" )  / get_param ( "ecoli_division_size_variance" ) );
    //printf("EColi growth rate: %f , EColi division size variance: %f\n", get_param ( "ecoli_growth_rate" ), get_param ( "ecoli_division_size_variance" ));
    div_vol = get_param ( "ecoli_division_size_mean" ) - get_param ( "ecoli_growth_rate" ) * 10 / lambda;
    //printf("EColi division size mean: %f , EColi growth rate: %f\n", get_param ( "ecoli_division_size_mean" ), get_param ( "ecoli_growth_rate" ));
    //printf("div_vol: %f , lambda: %f\n", div_vol, lambda);
    /*if(div_vol > 3.14)
    {
        cout << "Cell id: " << this->get_id() << ", div_vol: " << div_vol << ", lambda: " << lambda << endl;
        this->select();
    }*/

}

EColi::EColi ( World * w, float x, float y, float a, float v ) : Cell ( w ), volume ( v ) {

    compute_parameter_derivatives();

    float size = DEFAULT_ECOLI_SCALE*get_length();
    this->set_param("ecoli_growth_rate", DEFAULT_ECOLI_GROWTH_RATE);
    this->old_growth_rate = DEFAULT_ECOLI_GROWTH_RATE;


    body = ceCreateBody(w->get_space(), size, ceGetVector2(x,y), a);

    ceSetData(body, this);
   // cout << body->id << ": " << size << endl;

    int i;
    for ( i=0; i<MAX_STATE_NUM; i++ ) q[i] = 0;
    for ( i=0; i<MAX_REP_NUM; i++ ) rep[i] = 0;

    div_count = 0;
    force_div = false;

    this->in_vol = v;
    /*******/
    if(this->div_vol > 0 && this->in_vol > 0 && get_param ( "ecoli_growth_rate" ) > 0)
    {
        this->gt_inst = (log(this->div_vol/this->in_vol))/get_param ( "ecoli_growth_rate" );
    }
    else if (get_param ( "ecoli_growth_rate" ) < 0)
    {
        set_param("ecoli_growth_rate",0);
        cout << "Alert!!! - CONSTRUCTOR 1" << endl;
    }
    else
    {
        this->gt_inst = std::numeric_limits<float>::max();
    }
    //this->d_vol = 0;
    this->d_vol = get_param ( "ecoli_growth_rate" ) * volume * this->world->get_sim_dt();
    this->my_available = 0;


}

EColi::EColi ( World * w, float v, ceBody* body ) : Cell ( w ), volume ( v ) {

    compute_parameter_derivatives();

    this->set_param("ecoli_growth_rate", DEFAULT_ECOLI_GROWTH_RATE);
    this->old_growth_rate = DEFAULT_ECOLI_GROWTH_RATE;

    this->body = body;
    ceSetData(body, this);

    int i;
    for ( i=0; i<MAX_STATE_NUM; i++ ) q[i] = 0;
    for ( i=0; i<MAX_REP_NUM; i++ ) rep[i] = 0;

    div_count = 0;
    force_div = false;

    this->in_vol = v;
     /*******/
    if(this->div_vol > 0 && this->in_vol > 0 && get_param ( "ecoli_growth_rate" ) > 0 )
    {
        this->gt_inst = (log(this->div_vol/this->in_vol))/get_param ( "ecoli_growth_rate" );
    }
    else if (get_param ( "ecoli_growth_rate" ) < 0)
    {
        set_param("ecoli_growth_rate",0);
        cout << "Alert!!! - CONSTRUCTOR 2" << endl;
    }
    else
    {
        this->gt_inst = std::numeric_limits<float>::max();
    }
    this->d_vol = get_param ( "ecoli_growth_rate" ) * volume * this->world->get_sim_dt();
    this->my_available = 0;
}

#ifndef NOGUI

void EColi::render ( Theme * theme, GroPainter * painter ) {

    float length = body->length;
    ceVector2 center = body->center;

    double
            gfp = ( rep[GFP] / volume - world->get_param ( "gfp_saturation_min" ) ) / ( world->get_param ( "gfp_saturation_max" ) - world->get_param ( "gfp_saturation_min" ) ),
            rfp = ( rep[RFP] / volume - world->get_param ( "rfp_saturation_min" ) ) / ( world->get_param ( "rfp_saturation_max" ) - world->get_param ( "rfp_saturation_min" ) ),
            yfp = ( rep[YFP] / volume - world->get_param ( "yfp_saturation_min" ) ) / ( world->get_param ( "yfp_saturation_max" ) - world->get_param ( "yfp_saturation_min" ) ),
            cfp = ( rep[CFP] / volume - world->get_param ( "cfp_saturation_min" ) ) / ( world->get_param ( "cfp_saturation_max" ) - world->get_param ( "cfp_saturation_min" ) );

    theme->apply_ecoli_edge_color ( painter, is_selected() );

    QColor col;

    col.setRgbF( qMin(1.0,rfp + yfp),
                 qMin(1.0,gfp + yfp + cfp),
                 qMin(1.0,cfp),
                 0.75);


    painter->translate(center.x, center.y);
    //painter->rotate(body->rotation/CE_RADIAN*0.5);
    painter->rotate(body->rotation*CE_RADIAN);
    painter->translate(-center.x, -center.y);

    painter->setBrush(col);
    painter->drawRoundedRect(center.x - length/2, center.y - WIDTH/2, length, WIDTH, WIDTH/2, WIDTH/2);
    //painter->drawRoundedRect(center.x - length/2, center.y - WIDTH/2, (length - 2), (WIDTH - 2), (WIDTH/2 - 1), (WIDTH/2 -1));

    painter->translate(center.x, center.y);
    //painter->rotate(-body->rotation/CE_RADIAN*0.5);
    painter->rotate(-body->rotation*CE_RADIAN);
    painter->translate(-center.x, -center.y);

    //printf("%f\n",body->rotation);

}
#endif

void EColi::update ( void ) {

    float threshold = get_param("nutrient_consumption_rate");
    float monod = 1.0;
    //float randomio = 0.0;
    //cout << "dt - ECOLI: " << world->get_sim_dt() << endl;
    float randomio = rand_exponential ( 1 / world->get_sim_dt() );
    //cout << "randomio - ECOLI: " << randomio << endl;
    float dvolume = 0;
    float frand_val = 0;

    if(get_param("nutrients") == 1.0)
    {
        if (available <= 0) {
             available = 0;
             monod = 0;
             dvolume = 0;
         } else {
            monod = ( available/(available+threshold) );
            dvolume = get_param ( "ecoli_growth_rate" ) * monod * randomio * this->cross_input_coefficient * volume;
            //dvolume = get_param ( "ecoli_growth_rate_max" ) * monod * randomio * volume;
         }
         this->d_vol = get_param ( "ecoli_growth_rate" ) * monod * randomio * this->cross_input_coefficient * volume;
         //this->d_vol = get_param ( "ecoli_growth_rate_max" ) * monod * randomio * volume;
         this->my_available = available;
    }
    else
    {
        monod = 1.0;
        dvolume = get_param ( "ecoli_growth_rate" ) * monod * randomio * this->cross_input_coefficient * volume;
        //dvolume = get_param ( "ecoli_growth_rate_max" ) * monod * randomio * volume;
        //OJO
        this->d_vol = get_param ( "ecoli_growth_rate" ) * monod * randomio * this->cross_input_coefficient * volume;
        //this->d_vol = get_param ( "ecoli_growth_rate_max" ) * monod * randomio * volume;
    }

    //PESTAÑA
    //this->in_vol = this->volume;

    /*cout << "D_vol - ECOLI: " << this->d_vol << endl;
    cout << "In_vol - ECOLI: " << this->in_vol << endl;
    cout << "Div_vol - ECOLI: " << this->div_vol << endl;
    cout << "monod - ECOLI: " << monod << endl;
    cout << "ecoli_growth_rate - ECOLI: " << get_param ( "ecoli_growth_rate" ) << endl;
    cout << "ecoli_growth_rate_max - ECOLI: " << get_param ( "ecoli_growth_rate_max" ) << endl;*/

    /*if(this->div_vol < this->in_vol && div_vol > 0)
    {
        this->gt_inst = 0;
    }
    //CEJA
    else*/
    if (get_param ( "ecoli_growth_rate" ) == 0)
    {
        this->gt_inst = std::numeric_limits<float>::max();
    }
    else if (get_param ( "ecoli_growth_rate" ) < 0)
    {
        set_param("ecoli_growth_rate",0);
        cout << "Alert!!! - UPDATE" << endl;
    }
    else if (div_vol > 0 && in_vol > 0 && get_param("ecoli_growth_rate") > 0)
    {
        this->gt_inst = (log(this->div_vol/this->in_vol))/(get_param ( "ecoli_growth_rate" )* monod * this->cross_input_coefficient);
        //this->gt_inst = (log(this->div_vol/this->in_vol))/(get_param ( "ecoli_growth_rate_max" )*monod);
    }

    /*cout << "gt_inst - ECOLI: " << this->gt_inst << endl;
    cout << "dvolume - ECOLI: " << dvolume << endl;*/

    volume += dvolume;

    /************/
    /*if(volume > 4.00)
    {
        cout << "Cell id: " << this->get_id() << ", volume: " << this->volume << ", dvolume:" << dvolume << ", div_vol:" << div_vol << ", ecoli_growth_rate:" << get_param ( "ecoli_growth_rate" ) <<endl;
    }
    if(this->gt_inst < 0)
    {
        this->select();
    }*/

    //printf("%u: %f\n",body->id,dvolume);
    ceGrowBody(body, 10*dvolume/(0.25*CE_PI));
    // OJO!!! ESTO LO SOLUCIONO!!! El problema era lambda. No se recalculaba.
    compute_parameter_derivatives();
    frand_val = frand();

    if ( volume > div_vol && frand_val < lambda * world->get_sim_dt() )
         div_count++;

    if ( program != NULL )
         program->update ( world, this );
}

Value * EColi::eval ( Expr * e ) {

    if ( program != NULL )
        program->eval ( world, this, e );

}

EColi * EColi::divide ( void ) {

    // si la madre tiene alguna proteina a 1, la hija tambien. (Comentario Alfonso Glip)

    if ( div_count >= 10 || force_div ) {

        int r = frand() > 0.5 ? 1 : -1;

        div_count = 0;
        force_div = false;

        float frac = 0.5 + 0.1 * ( frand() - 0.5 );
        float oldvol = volume;
        float oldsize = DEFAULT_ECOLI_SCALE * get_length();

        volume = frac * oldvol;
        in_vol = volume;
        float a = body->rotation;
        float da = 0.25 * (frand()-0.5);

        // OJO!!! Suponemos da en radianes!!!
        ceBody* daughterBody = ceDivideBody(body, da, frac);

        float dvol = (1-frac)*oldvol;

        EColi * daughter = new EColi ( world, dvol, daughterBody );

        int i,j;

        for( i=0; i<this->nPlasmids(); i++)
        {
            daughter->plasmids[i] = this->plasmids[i];
            daughter->received_plasmids[i] = this->received_plasmids[i];
            daughter->just_conjugated[i] = this->just_conjugated[i];
            daughter->just_conjugated_reset[i] = this->just_conjugated_reset[i];
            daughter->eex[i] = this->eex[i];
        }

        for( i=0; i<this->num_proteins; i++)
        {
            daughter->internal_proteins[i]=false;
            daughter->internal_RNAs[i]=this->internal_RNAs[i];
            //daughter->internal_RNAs[i]=false;
            daughter->degr_prot[i]=this->degr_prot[i];
            daughter->degr_rna[i]=this->degr_rna[i];
        }

        for ( i=0; i<world->num_operons(); i++ ){
            for(j=0;j<num_proteins;j++){
                daughter->last_state[i][j] =this->last_state[i][j];
                daughter->last_state_rna[i][j] =this->last_state_rna[i][j];
                //daughter->last_state[i][j] = false;
                daughter->rna_operon[i][j] =this->rna_operon[i][j];
                daughter->prot_actv_times[i][j]=this->prot_actv_times[i][j];
                daughter->prot_degr_times[i][j]=this->prot_degr_times[i][j];
                daughter->rna_actv_times[i][j]=this->rna_actv_times[i][j];
                daughter->rna_degr_times[i][j]=this->rna_degr_times[i][j];
                //this->last_state[i][j]=false;

                daughter->proteins_operon[i][j] = false;
                if(this->proteins_operon[i][j]){
                    Operon * op = world->get_operon(i);
                    double up_p = op->get_prot_up_time(j);
                    double up_p_err = op->get_prot_up_time_err(j);
                    double up_r = op->get_rna_up_time(j);
                    double up_r_err = op->get_rna_up_time_err(j);
                    daughter->prot_actv_times[i][j]=world->get_time() + (drand48() * ((up_p + up_p_err)-(up_p-up_p_err))+(up_p-up_p_err))/2; //la mitad del tiempo de activacion normal
                    daughter->rna_actv_times[i][j]=world->get_time() + (drand48() * ((up_r + up_r_err)-(up_r-up_r_err))+(up_r-up_r_err))/2; //la mitad del tiempo de activacion normal
                    this->prot_actv_times[i][j]=world->get_time() + (drand48() * ((up_p + up_p_err)-(up_p-up_p_err))+(up_p-up_p_err))/2; //la mitad del tiempo de activacion normal
                    this->rna_actv_times[i][j]=world->get_time() + (drand48() * ((up_r + up_r_err)-(up_r-up_r_err))+(up_r-up_r_err))/2; //la mitad del tiempo de activacion normal
                    this->proteins_operon[i][j] = false;
                }
            }
        }


        daughter->set_param_map ( get_param_map() );
        daughter->compute_parameter_derivatives();

        if ( gro_program != NULL ) {
            daughter->set_gro_program ( split_gro_program ( gro_program, frac ) );
        }

        daughter->init ( q, rep, 1-frac );



        for ( i=0; i<MAX_STATE_NUM; i++ ) q[i] = (int) ceil(frac*q[i]);
        for ( i=0; i<MAX_REP_NUM; i++ ) rep[i] = (int) ceil(frac*rep[i]);

        set_division_indicator(true);
        daughter->set_division_indicator(true);
        daughter->set_daughter_indicator(true);

        return daughter;

    } else return NULL;

}

