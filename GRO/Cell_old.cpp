/////////////////////////////////////////////////////////////////////////////////////////
//
// gro is protected by the UW OPEN SOURCE LICENSE, which is summarized here.
// Please see the file LICENSE.txt for the complete license.
//
// THE SOFTWARE (AS DEFINED BELOW) AND HARDWARE DESIGNS (AS DEFINED BELOW) IS PROVIDED
// UNDER THE TERMS OF THIS OPEN SOURCE LICENSE (“LICENSE”).  THE SOFTWARE IS PROTECTED
// BY COPYRIGHT AND/OR OTHER APPLICABLE LAW.  ANY USE OF THIS SOFTWARE OTHER THAN AS
// AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
//
// BY EXERCISING ANY RIGHTS TO THE SOFTWARE AND/OR HARDWARE PROVIDED HERE, YOU ACCEPT AND
// AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE.  TO THE EXTENT THIS LICENSE MAY BE
// CONSIDERED A CONTRACT, THE UNIVERSITY OF WASHINGTON (“UW”) GRANTS YOU THE RIGHTS
// CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
//
// TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
//
//

#ifndef NOGUI
#include <GroThread.h>
#endif

#include "Micro.h"
#include "Operon.h"
#include <iostream>
#include <string>
#include "Rands.h"

static int max_id = 0;

Cell::Cell ( World * w ) : world ( w ), gro_program(NULL), marked(false), selected(false) {

    parameters = w->get_param_map();
    compute_parameter_derivatives();
    w->init_actions_map();

    program = w->get_program();
    space = w->get_space();
    body = NULL;

    int i;
    int j;
    for ( i=0; i<MAX_STATE_NUM; i++ ) q[i] = 0;
    for ( i=0; i<MAX_REP_NUM; i++ ) rep[i] = 0;

    divided = false;
    daughter = false;

    num_plasmids = get_param("num_plasmids");
    num_proteins = get_param("num_proteins");

    plasmids = new bool[num_plasmids];
    received_plasmids = new bool[num_plasmids];
    eex = new bool[num_plasmids];
    just_conjugated = new bool[num_plasmids];
    just_conjugated_reset = new bool[num_plasmids];
    //n_plasmids = n;
    plasmidToConjugate = -1;
    conjugationMode = 0;
    markForConj = false;
    prot_to_degr=0;
    rna_to_degr=0;

    for ( i=0; i<num_plasmids; i++ )
    {
        plasmids[i] = false;
        received_plasmids[i] = false;
        eex[i] = false;
        just_conjugated[i] = false;
        just_conjugated_reset[i] = false;
    }

    internal_proteins = new bool[num_proteins];
    internal_RNAs = new bool[num_proteins];
    proteins_operon = (bool **)malloc(world->num_operons()*sizeof(bool *));
    rna_operon = (bool **)malloc(world->num_operons()*sizeof(bool *));
    last_state = (bool **)malloc(world->num_operons()*sizeof(bool *));
    prot_actv_times = (float **)malloc(world->num_operons()*sizeof(float *));
    prot_degr_times = (float **)malloc(world->num_operons()*sizeof(float *));
    degr_prot = new float[num_proteins];
    rna_actv_times = (float **)malloc(world->num_operons()*sizeof(float *));
    rna_degr_times = (float **)malloc(world->num_operons()*sizeof(float *));
    degr_rna = new float[num_proteins];

    for ( i=0; i<num_proteins; i++ ){
        internal_proteins[i]=false;
        degr_prot[i]=-1;
        internal_RNAs[i]=false;
        degr_rna[i]=-1;
    }

    for ( i=0; i<world->num_operons(); i++ ){
        proteins_operon[i]=(bool *)malloc(num_proteins*sizeof(bool *));
        last_state[i]=(bool *)malloc(num_proteins*sizeof(bool *));
        prot_actv_times[i]=(float *)malloc(num_proteins*sizeof(float *));
        prot_degr_times[i]=(float *)malloc(num_proteins*sizeof(float *));
        rna_operon[i]=(bool *)malloc(num_proteins*sizeof(bool *));
        rna_actv_times[i]=(float *)malloc(num_proteins*sizeof(float *));
        rna_degr_times[i]=(float *)malloc(num_proteins*sizeof(float *));
    }

    for ( i=0; i<world->num_operons(); i++ ){
        for(j=0;j<num_proteins;j++){
            prot_actv_times[i][j]=-1;
            prot_degr_times[i][j]=-1;
            rna_actv_times[i][j]=-1;
            rna_degr_times[i][j]=-1;
        }
    }

    set_id ( max_id++ );

}

Cell::~Cell ( void ) {

    // Falta desasociar del espacio!
    //delete body;
    delete plasmids;
    delete received_plasmids;
    delete eex;
    delete just_conjugated;
    delete just_conjugated_reset;

    if ( gro_program != NULL ) {
        delete gro_program;
    }

}

bool Cell::hasPlasmid ( int k ) const
{
    return ( this->plasmids[k] );
}

void Cell::setPlasmid ( int k, bool b )
{
    this->plasmids[k] = b;
    //this->eex[k] = b;
}

bool Cell::hasEEX ( int k ) const
{
    return ( this->eex[k] );
}

void Cell::setEEX ( int k, bool b )
{
    this->eex[k] = b;
}

bool Cell::receivedPlasmid ( int k ) const
{
    return ( this->received_plasmids[k] );
}

void Cell::acquirePlasmid( int k )
{
    this ->plasmids[k]=true;
    this->received_plasmids[k] = true;
    //this->eex[k] = true;
}

void Cell::losePlasmid( int k )
{
    this->plasmids[k]=false;
    this->received_plasmids[k] = false;
    //this->eex[k] = false;
}

void Cell::jc_reset ( void )
{
    int i = 0;
    for (i = 0; i < this->num_plasmids; i++)
    {
        if( just_conjugated[i] && !just_conjugated_reset[i] )
        {
            just_conjugated_reset[i] = true;
        }
        else if ( just_conjugated[i] && just_conjugated_reset[i] )
        {
            just_conjugated[i] = false;
            just_conjugated_reset[i] = false;
        }
        else
        {
            return;
        }

    }
}

bool Cell::was_just_conjugated ( int k ) const
{
    // return ( (this->just_conjugated_reset[k-1] || this->just_conjugated[k-1]) );
    return ( this->just_conjugated_reset[k] );
}

void Cell::set_conjugated_indicator ( int k, bool b )
{
    this->just_conjugated[k] = b;
}

void Cell::markForConjugation( int k, int mode )
{
    this->plasmidToConjugate = k;
    this->conjugationMode = mode;
    this->markForConj = true;
}

//void Cell::conjugate(int n_pl, double cf)
void Cell::conjugate(int n_pl, double n_conj)
{

    double r = fRand(0.00000000000000, 1.000000000000000);
    double p = 0;
    unsigned int size;
    int nc = 6;
    //double dete = world->get_sim_dt();
    double dete = 0.1;
    printf("r: %lf ", r);
    //printf("Gt inst: %f, dt: %f, \n", this->gt_inst);
    ceBody** neighbors = ceGetNearBodies(this->body,0.5,&size);
    if(size < nc)
    {
        //LO HACE MAL
        printf("Primer caso: n_conj: %lf, dete: %lf, gt_inst: %f, size: %d, nc: %d\n", n_conj, dete, this->gt_inst, size, nc);
        p = ((n_conj * dete)/(this->gt_inst))*((double)((double)size/(double)nc));
        //p = (double)((cf*dete)*(size/nc));
    }
    else
    {
        //LO HACE BIEN
        printf("Segundo caso: n_conj: %lf, dete: %lf, gt_inst: %f\n", n_conj, dete, this->gt_inst);
        p = ((n_conj * dete)/(this->gt_inst));
        //p = (double)(cf*dete);
    }
    printf("p: %lf \n", p);
    if(r < p)
    {
        printf("Entro... a conjugar...\n");
        int target = rand()%size;
        if(!(((Cell*)(neighbors[target]->data))->marked_for_death()) &&
                !(this->marked_for_death()))
        {
            ((Cell*)(neighbors[target]->data))->acquirePlasmid(n_pl);
            ((Cell*)(neighbors[target]->data))->set_conjugated_indicator(n_pl, true);
        }
        free(neighbors);

    }

    // Completa
    /*if(mode == 0)
    {
        if (this->markForConj)
        {
            this->markForConj = false;
            unsigned int size;
            ceBody** neighbors = ceGetNearBodies(this->body,0.5,&size);
            if(size > 0)
            {
                int target = rand()%size;
                if(!(((Cell*)(neighbors[target]->data))->marked_for_death()) &&
                        !(this->marked_for_death()) &&
                        !(((Cell*)(neighbors[target]->data))->hasEEX(this->plasmidToConjugate)))
                {
                    ((Cell*)(neighbors[target]->data))->acquirePlasmid(this->plasmidToConjugate);
                    ((Cell*)(neighbors[target]->data))->set_conjugated_indicator(this->plasmidToConjugate, true);
                }
            }
            else
            {
                std::cout << size << std::endl;
            }
            free(neighbors);
        }
    }
    else
    {
        int seen = 0;
        int i = 0;
        if (this->markForConj)
        {
            this->markForConj = false;
            unsigned int size;
            ceBody** neighbors = ceGetNearBodies(this->body,0.5,&size);
            if(size > 0)
            {
                int target = rand() % size;
                seen++;
                while((
                          !(((Cell*)(neighbors[target]->data))->marked_for_death()) &&
                          !(this->marked_for_death()) &&
                          (((Cell*)(neighbors[target]->data))->hasEEX(this->plasmidToConjugate))) &&
                      seen < size)
                {
                    target = (target + 1) % size;
                    seen++;
                }
                if(!(((Cell*)(neighbors[target]->data))->marked_for_death()) &&
                        !(this->marked_for_death()) &&
                        !(((Cell*)(neighbors[target]->data))->hasEEX(this->plasmidToConjugate)))
                {
                    ((Cell*)(neighbors[target]->data))->acquirePlasmid(this->plasmidToConjugate);
                    ((Cell*)(neighbors[target]->data))->set_conjugated_indicator(this->plasmidToConjugate, true);
                }
            }
            else
            {
                std::cout << size << std::endl;
            }
        }
    }*/

}

void Cell::set_initial_protein(int p, bool b){//la proteina inicial se tiene que empezar a degradar a no ser que otra la active
    int i,j;
    bool found;
    if(b){
        internal_proteins[p]=b;
        for (i=0;i<num_plasmids;i++){ //miramos todos los operones que tenemos para ver cual da esta proteina y actualizar la matriz
            if (plasmids[i]){
                for (j=0;j<world->num_operons();j++){
                    if (world->get_plasmids_matrix(i,j)){
                        if(world->get_operon(j)->get_output_protein(p)){
                            found = true;
                            proteins_operon[j][p] = true;
                            if(world->get_operon(j)->get_tf(p)==-1){
                                last_state[j][p] = true;
                            }
                            prot_degr_times[j][p] = world->get_prot_down_time(p);
                            rna_degr_times[j][p] = world->get_rna_down_time(p);
                            std::cout<<"Se va a degradar la P"<<p+1<<" del operon"<<j+1<<" en el minuto "<< prot_degr_times[j][p]<<". A no ser que la mantenga activada alguna otra proteina "<<std::endl;
                            std::cout<<"Se va a degradar el RNA"<<p+1<<" del operon"<<j+1<<" en el minuto "<< rna_degr_times[j][p]<<". A no ser que la mantenga activada alguna otra proteina "<<std::endl;

                        }
                    }
                }
                if(!found){
                    degr_prot[p]= world->get_prot_down_time(p);
                    degr_rna[p]=world->get_rna_down_time(p);
                    prot_to_degr++;
                    internal_proteins[p]=true;
                    std::cout<<"Se va a degradar la P"<<p+1<<" en el minuto "<< degr_prot[p]<<std::endl;
                    std::cout<<"Se va a degradar el RNA"<<p+1<<" en el minuto "<< degr_rna[p]<<std::endl;

                }
            }
        }
    }
}

void Cell::set_initial_rna(int p, bool b){//la proteina inicial se tiene que empezar a degradar a no ser que otra la active
    //internal_mRNAs[i]=b;
    int i,j;
    bool found;
    if(b){
        internal_RNAs[p]=b;
        for (i=0;i<num_plasmids;i++){ //miramos todos los operones que tenemos para ver cual da este mrna y actualizar la matriz
            if (plasmids[i]){
                for (j=0;j<world->num_operons();j++){
                    if (world->get_plasmids_matrix(i,j)){
                        if(world->get_operon(j)->get_output_rna(p) || world->get_operon(j)->get_output_protein(p)){ //ya sea porque genera rna o porque genera proteina
                            found = true;
                            rna_operon[j][p] = true;
                            rna_degr_times[j][p] = world->get_rna_down_time(p);
                            std::cout<<"Se va a degradar el RNA"<<p+1<<" del operon"<<j+1<<" en el minuto "<< rna_degr_times[j][p]<<". A no ser que lo mantenga activado alguna otra proteina "<<std::endl;
                        }
                    }
                }
                if(!found){
                    degr_rna[p]= world->get_rna_down_time(p);
                    rna_to_degr++;
                    internal_RNAs[p]=true;
                    std::cout<<"Se va a degradar el RNA"<<p+1<<" en el minuto "<< degr_rna[p]<<std::endl;
                }
            }
        }
    }
}


void Cell::set_initial_last_state(){ //para que en los casos de represiones el estado anterior de la proteina sea true, y así entra en la logica
    int i,op,tf;
    for (i=0;i<num_plasmids;i++){
        if (plasmids[i]){
            for (op=0;op<world->num_operons();op++){
                if (world->get_plasmids_matrix(i,op)){
                    for (tf=0;tf<num_proteins;tf++){
                        if(world->get_operon(op)->get_tf(tf)==-1){
                            last_state[op][tf]=true;
                        }
                    }
                }
            }
        }
    }
}

void Cell::update_internal_proteins(){ //Este metodo es para actualizar la lista de proteinas que estan activadas en toda la bacteria
    int i;
    int p;
    for(i=0;i<num_proteins;i++){ //primero pongo la lista entera a false
        internal_proteins[i]=false;
        internal_RNAs[i]=false;
    }

    for(i=0;i<world->num_operons();i++){
        for(p=0;p<num_proteins;p++){
            if(proteins_operon[i][p])
                internal_proteins[p]=internal_proteins[p]||proteins_operon[i][p];
            if(rna_operon[i][p])
                internal_RNAs[p]=internal_RNAs[p]||rna_operon[i][p];
        }
    }
}


void Cell::testProteins() //Comprueba tiempos,cambios en las proteinas internas y mira las proteinas para ver que funcion se ejecuta
{
    int num_operons;
    num_operons = world->num_operons();
    int i,op,pl;

    for (op=0;op<num_operons;op++){
        for (pl=0;pl<num_plasmids;pl++){
            if (plasmids[pl] && world->get_plasmids_matrix(pl,op)){
                world->get_operon(op)->update(this,op);
                pl=num_plasmids;
            }

        }
    }

    //Mirar las proteinas que se tienen que degradar pero no tienen ningun operon asociado
    if(prot_to_degr>0){
        for(i=0;i<num_proteins;i++){
            if(degr_prot[i]!=-1 && world->get_time() >= degr_prot[i]){ // ya es hora de reprimir la proteina i
                std::cout<<"Se ha degradado la P"<<i+1<<" en el minuto "<< world->get_time()<<std::endl;
                prot_to_degr--;
                degr_prot[i]=-1;
                internal_proteins[i]=false;
                //check_action(i);
            }
        }
    }

    check_action();
}

bool Cell::can_express_protein(int i){
    //consulta la matriz de moleculas para saber si el tf puede unirse al promotor
    // si hay algun 1 en una columna entonces solo puede expresarse si esta esa molecula
    // si hay algun -1 puede expresarse siempre y cuando esa molecula no este
    // si son todo 0, puede expresarse

    bool result=false;
    int k=0;
    bool found = false;
    bool need_inductor=false;

    while(!found && k<world->get_param("num_molecules")){
        if(world->get_molecules_matrix(k,i)==1){
            if(world->get_molecule(k)){
                return true;
            }
            need_inductor=true;
        }
        k++;
    }

    if(!need_inductor && !found){
        result = true;
        for(k=0;k<world->get_param("num_molecules");k++){
            if(world->get_molecule(k) && world->get_molecules_matrix(k,i)==-1){
                return false;
            }
        }
    }

    return result;
}

void Cell::print_state()
{
    int i;
    for(i=0;i<num_proteins;i++){
        if(i==num_proteins-1)
            std::cout<<internal_proteins[i]<<" ]"<<std::endl;
        else if (i==0)
            std::cout<<"proteinas: [t="<<world->get_time()<<"| "<<internal_proteins[i]<<" ,";
        else
            std::cout<<internal_proteins[i]<<" ,";
    }
    /*
    for(i=0;i<num_proteins;i++){
        if(i==num_proteins-1)
            std::cout<<internal_RNAs[i]<<" ]"<<std::endl;
        else if (i==0)
            std::cout<<"RNAs: [t="<<world->get_time()<<"| "<<internal_RNAs[i]<<" ,";
        else
            std::cout<<internal_RNAs[i]<<" ,";
    }

    for(int j =0;j<world->get_num_actions();j++){
        for(i=0;i<num_proteins;i++){
            if(i==num_proteins-1)
                std::cout<<world->get_prot_action_matrix(j,i)<<" ]"<<std::endl;
            else if (i==0)
                std::cout<<"matriz act [ "<<world->get_num_actions()<<"|"<<world->get_prot_action_matrix(j,i)<<" ,";
            else
                std::cout<<world->get_prot_action_matrix(j,i)<<" ,";
        }
    }
    */
}


void Cell::check_action() // p es el indice de la proteina que acaba de ponerse a true
{
    //std::vector<bool> result;
    bool result = true;
    int num_actions = world->get_num_actions();
    for(int row=0;row<num_actions;++row){
        for(int col=0; col<num_proteins;++col){
            if(world->get_prot_action_matrix(row,col) && !internal_proteins[col]){
                col = num_proteins;
                result=false;
            }
        }
        if(result){
            FnPointer fp = world->get_action(world->get_action_name(row));
            std::list<std::string> pl = world->get_action_param(row);
            ((*this).*fp)(pl);
        }

    }
}


void Cell::paint_from_list(std::list<string> ls){
    std::list<std::string>::iterator i = ls.begin();
    int gfp = std::atoi((*i).c_str());i++;
    int rfp = std::atoi((*i).c_str());i++;
    int yfp = std::atoi((*i).c_str());i++;
    int cfp = std::atoi((*i).c_str());
    this->set_rep(GFP,gfp);
    this->set_rep(RFP,rfp);
    this->set_rep(YFP,yfp);
    this->set_rep(CFP,cfp);
    //std::cout<< "GFP"<<std::endl;
}

void Cell::conjugate_from_list(std::list<std::string> ls){
    std::list<std::string>::iterator i = ls.begin();
    int j = std::atoi((*i).c_str());
    i++;
    double n_conj = std::atof((*i).c_str());
    print_state();
    conjugate(j, n_conj);
    std::cout<< "Funcion conjugate con parametro ("<<j<<")."<<std::endl;
}
void Cell::lose_plasmid_from_list(std::list<std::string> ls){
    std::list<std::string>::iterator i = ls.begin();
    int j = std::atoi((*i).c_str()); i++;
    int k = std::atoi((*i).c_str()); i++;
    int l = std::atoi((*i).c_str());
    //print_state();
    //std::cout<< "Funcion lose_plasmid con parametros ("<<j<<", "<<k<<", "<<l<<")."<<std::endl;
}
void Cell::die_from_list(std::list<std::string> ls){
    std::list<std::string>::iterator i = ls.begin();
    int j = std::atoi((*i).c_str());
    //print_state();
    if ( this != NULL && !this->marked_for_death())
        this->mark_for_death();
    else
        printf ( "Warning: Called die() from outside a cell program. No action taken\n" );
    //std::cout<< "Die!!"<<std::endl;
}

void Cell::conj_and_paint_from_list(std::list<std::string> ls){
    std::list<std::string>::iterator i = ls.begin();
    int j = std::atoi((*i).c_str());
    i++;
    double n_conj = std::atof((*i).c_str()); i++;
    //print_state();
    conjugate(j, n_conj);
    int gfp = std::atoi((*i).c_str());i++;
    int rfp = std::atoi((*i).c_str());i++;
    int yfp = std::atoi((*i).c_str());i++;
    int cfp = std::atoi((*i).c_str());
    this->set_rep(GFP,gfp);
    this->set_rep(RFP,rfp);
    this->set_rep(YFP,yfp);
    this->set_rep(CFP,cfp);
}
