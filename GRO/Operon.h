#ifndef OPERON_H
#define OPERON_H

#include "Defines.h"
#include "Cell.h"
#include "Micro.h"
//#include "statistics.h"

using namespace std;

class World;
class Cell;

class Operon{

public:

    Operon (World *w, bool *p, bool*r, int *tf, int * rb, bool constit, float *upP, float *errP,float *upR, float *errR, int cir, double *prI, double *prR);
    Operon (World *w);

    void add_protein (int i, bool b) { proteins[i]=b; }
    void add_RNA (int i, bool b) { rnas[i]=b; }
    void add_transFac (int i, int t) { transFactor[i]=t; }
    void add_RBS (int i, int r) { ribswitchs[i]=r; }
    void add_prot_up_time (int i, float f) { prot_up_times[i]=f; }
    void add_prot_time_error (int i, float f) { prot_time_error[i]=f; }
    void add_rna_up_time (int i, float f) { rna_up_times[i]=f; }
    void add_rna_time_error (int i, float f) { rna_time_error[i]=f; }
    void add_circuit (int c) { circuit = c; }
    void set_constitutive (bool b) {constitutive = b;}
    void add_prob_ind_output1_input1 (float p) { prob_ind_output1_input1 = p; }
    void add_prob_ind_output0_input1 (float p) { prob_ind_output0_input1 = p; }
    void add_prob_ind_output1_input0 (float p) { prob_ind_output1_input0 = p; }
    void add_prob_ind_output0_input0 (float p) { prob_ind_output0_input0 = p; }
    void add_prob_rep_output1_input1 (float p) { prob_rep_output1_input1 = p; }
    void add_prob_rep_output0_input1 (float p) { prob_rep_output0_input1 = p; }
    void add_prob_rep_output1_input0 (float p) { prob_rep_output1_input0 = p; }
    void add_prob_rep_output0_input0 (float p) { prob_rep_output0_input0 = p; }

    bool get_output_protein(int i){return proteins[i];}
    bool get_output_rna(int i){return rnas[i];}
    int get_tf(int i){return transFactor[i];}
    bool is_constitutive (void) {return constitutive;}
    float get_prot_up_time(int i){return prot_up_times[i];}
    float get_rna_up_time(int i){return rna_up_times[i];}
    float get_prot_up_time_err(int i){return prot_time_error[i];}
    float get_rna_up_time_err(int i){return rna_time_error[i];}

    int get_operon_id(){return operon_id;}
    void set_operon_name(std::string n){name = n;}
    string get_operon_name(){return name;}

    void update(Cell *,int);

    void yes_gate(Cell *,int, int, int, bool, int);
    void and_gate(Cell *,int, int, int, int, bool, int);
    void or_gate(Cell *,int, int, int, int, bool, int);

    void activate(Cell *, int, int, int);
    void degradate(Cell *, int, int, int);

    void check_times(Cell *, int, int);
    void check_action(Cell *, int);

protected:

    int operon_id;

    World * world;

    int num_proteins;

    bool * proteins; // proteinas de salida
    bool * rnas; // mRNAs de salida
    int * transFactor; // 0: No es TF de este operon, 1: es promotor, -1: es represor
    int * ribswitchs; //array con el numero de ribowitch que esta delante de cada gen
    bool constitutive;
    float * prot_up_times; // Tiempos de activación de todas las proteinas
    float * prot_time_error; //Desviacion tipica de los tiempos de subida de las proteinas
    float * rna_up_times; // Tiempos de activación de los mrna
    float * rna_time_error; //Desviacion tipica de los tiempos de subida de los mrna
    int circuit; // YES, AND, OR
    double prob_ind_output1_input1; // prob de que output=1 dado que input=1 en inductores
    double prob_ind_output0_input1; // prob de que output=0 dado que input=1 en inductores
    double prob_ind_output1_input0; // prob de que output=1 dado que input=0 en inductores
    double prob_ind_output0_input0; // prob de que output=0 dado que input=0 en inductores
    double prob_rep_output1_input1; // prob de que output=1 dado que input=1 en represores
    double prob_rep_output0_input1; // prob de que output=0 dado que input=1 en represores
    double prob_rep_output1_input0; // prob de que output=1 dado que input=0 en represores
    double prob_rep_output0_input0; // prob de que output=0 dado que input=0 en represores

    std::string name;

};


#endif // OPERON_H
