/////////////////////////////////////////////////////////////////////////////////////////
//
// gro is protected by the UW OPEN SOURCE LICENSE, which is summarized here.
// Please see the file LICENSE.txt for the complete license.
//
// THE SOFTWARE (AS DEFINED BELOW) AND HARDWARE DESIGNS (AS DEFINED BELOW) IS PROVIDED
// UNDER THE TERMS OF THIS OPEN SOURCE LICENSE (“LICENSE”).  THE SOFTWARE IS PROTECTED
// BY COPYRIGHT AND/OR OTHER APPLICABLE LAW.  ANY USE OF THIS SOFTWARE OTHER THAN AS
// AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
//
// BY EXERCISING ANY RIGHTS TO THE SOFTWARE AND/OR HARDWARE PROVIDED HERE, YOU ACCEPT AND
// AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE.  TO THE EXTENT THIS LICENSE MAY BE
// CONSIDERED A CONTRACT, THE UNIVERSITY OF WASHINGTON (“UW”) GRANTS YOU THE RIGHTS
// CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
//
// TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
//
//

#include "EColi.h"
#include "Programs.h"
#include <iostream>
#include <math.h>

using namespace std;

#define FMULT 0.125

void EColi::compute_parameter_derivatives ( void ) {

    lambda = sqrt ( 10 * get_param ( "ecoli_growth_rate" ) * get_param ( "ecoli_growth_rate" )  / get_param ( "ecoli_division_size_variance" ) );
    div_vol = get_param ( "ecoli_division_size_mean" ) - get_param ( "ecoli_growth_rate" ) * 10 / lambda;

}

EColi::EColi ( World * w, float x, float y, float a, float v ) : Cell ( w ), volume ( v ) {

    compute_parameter_derivatives();

    float size = DEFAULT_ECOLI_SCALE*get_length();

    body = ceCreateBody(w->get_space(), size, ceGetVector2(x,y), a);
    ceSetData(body, this);

    int i;
    for ( i=0; i<MAX_STATE_NUM; i++ ) q[i] = 0;
    for ( i=0; i<MAX_REP_NUM; i++ ) rep[i] = 0;

    div_count = 0;
    force_div = false;

    this->in_vol = v;
    this->gt_inst = (log(this->div_vol/this->in_vol))/get_param ( "ecoli_growth_rate" );

}

EColi::EColi ( World * w, float v, ceBody* body ) : Cell ( w ), volume ( v ) {

    compute_parameter_derivatives();

    this->body = body;
    ceSetData(body, this);

    int i;
    for ( i=0; i<MAX_STATE_NUM; i++ ) q[i] = 0;
    for ( i=0; i<MAX_REP_NUM; i++ ) rep[i] = 0;

    div_count = 0;
    force_div = false;

    this->in_vol = v;
    this->gt_inst = (log(this->div_vol/this->in_vol))/get_param ( "ecoli_growth_rate" );
}

#ifndef NOGUI

void EColi::render ( Theme * theme, GroPainter * painter ) {

    float length = body->length;
    ceVector2 center = body->center;

    double
            gfp = ( rep[GFP] / volume - world->get_param ( "gfp_saturation_min" ) ) / ( world->get_param ( "gfp_saturation_max" ) - world->get_param ( "gfp_saturation_min" ) ),
            rfp = ( rep[RFP] / volume - world->get_param ( "rfp_saturation_min" ) ) / ( world->get_param ( "rfp_saturation_max" ) - world->get_param ( "rfp_saturation_min" ) ),
            yfp = ( rep[YFP] / volume - world->get_param ( "yfp_saturation_min" ) ) / ( world->get_param ( "yfp_saturation_max" ) - world->get_param ( "yfp_saturation_min" ) ),
            cfp = ( rep[CFP] / volume - world->get_param ( "cfp_saturation_min" ) ) / ( world->get_param ( "cfp_saturation_max" ) - world->get_param ( "cfp_saturation_min" ) );

    theme->apply_ecoli_edge_color ( painter, is_selected() );

    QColor col;

    col.setRgbF( qMin(1.0,rfp + yfp),
                 qMin(1.0,gfp + yfp + cfp),
                 qMin(1.0,cfp),
                 0.75);


    painter->translate(center.x, center.y);
    //painter->rotate(body->rotation/CE_RADIAN*0.5);
    painter->rotate(body->rotation*CE_RADIAN);
    painter->translate(-center.x, -center.y);

    painter->setBrush(col);
    painter->drawRoundedRect(center.x - length/2, center.y - WIDTH/2, length, WIDTH, WIDTH/2, WIDTH/2);

    painter->translate(center.x, center.y);
    //painter->rotate(-body->rotation/CE_RADIAN*0.5);
    painter->rotate(-body->rotation*CE_RADIAN);
    painter->translate(-center.x, -center.y);

    //printf("%f\n",body->rotation);

}
#endif

void EColi::update ( void ) {

    //float dvolume = get_param ( "ecoli_growth_rate" ) * rand_exponential ( 1 / world->get_sim_dt() ) * volume;
    //float dvolume = get_param ( "ecoli_growth_rate" ) * rand_exponential ( 1 / world->get_sim_dt() );
    float dvolume = get_param ( "ecoli_growth_rate" ) *  world->get_sim_dt() * volume;
    //printf("Dvolume: %f, Div-Vol: %f, In-Vol: %f\n", dvolume, this->div_vol, this->in_vol);
    this->gt_inst = (log(this->div_vol/this->in_vol))/get_param ( "ecoli_growth_rate" );
    volume += dvolume;

    /*float length = body->getLength();
  cout << "PRE" << length << endl;*/

    //body->grow(0.1);
    ceGrowBody(body, 10*dvolume/(0.25*CE_PI));
    //cout << "Valor raro: " << (dvolume/(0.25*PI) << endl;

    /*length = body->getLength();
  cout << "POST" << length << endl;*/


    if ( volume > div_vol && frand() < lambda * world->get_sim_dt() )
        div_count++;

    if ( program != NULL )
        program->update ( world, this );

}

Value * EColi::eval ( Expr * e ) {

    if ( program != NULL )
        program->eval ( world, this, e );

}

EColi * EColi::divide ( void ) {

    // si la madre tiene alguna proteina a 1, la hija tambien. (Comentario Alfonso Glip)

    if ( div_count >= 10 || force_div ) {

        int r = frand() > 0.5 ? 1 : -1;

        div_count = 0;
        force_div = false;

        float frac = 0.5 + 0.1 * ( frand() - 0.5 );
        float oldvol = volume;
        float oldsize = DEFAULT_ECOLI_SCALE * get_length();

        volume = frac * oldvol;
        float a = body->rotation;
        float da = 0.25 * (frand()-0.5);

        // OJO!!! Suponemos da en radianes!!!
        ceBody* daughterBody = ceDivideBody(body, da, frac);

        float dvol = (1-frac)*oldvol;

        //EColi * daughter = new EColi ( world, this->nPlasmids() ,dvol, daughterBody );
        EColi * daughter = new EColi ( world, dvol, daughterBody );

        int i,j;

        for( i=0; i<this->nPlasmids(); i++)
        {
            daughter->plasmids[i] = this->plasmids[i];
            daughter->received_plasmids[i] = this->received_plasmids[i];
            daughter->just_conjugated[i] = this->just_conjugated[i];
            daughter->just_conjugated_reset[i] = this->just_conjugated_reset[i];
            daughter->eex[i] = this->eex[i];
        }

        for( i=0; i<this->num_proteins; i++)
        {
            daughter->internal_proteins[i]=this->internal_proteins[i];
            daughter->internal_RNAs[i]=this->internal_RNAs[i];
            daughter->degr_prot[i]=this->degr_prot[i];
            daughter->degr_rna[i]=this->degr_rna[i];
        }

        for ( i=0; i<world->num_operons(); i++ ){
            for(j=0;j<num_proteins;j++){
                daughter->proteins_operon[i][j] =this->proteins_operon[i];
                daughter->last_state[i][j] =this->last_state[i][j];
                daughter->rna_operon[i][j] =this->rna_operon[i][j];
                daughter->prot_actv_times[i][j]=this->prot_actv_times[i][j];
                daughter->prot_degr_times[i][j]=this->prot_degr_times[i][j];
                daughter->rna_actv_times[i][j]=this->rna_actv_times[i][j];
                daughter->rna_degr_times[i][j]=this->rna_degr_times[i][j];
            }
        }
        daughter->set_param_map ( get_param_map() );
        daughter->compute_parameter_derivatives();

        if ( gro_program != NULL ) {
            daughter->set_gro_program ( split_gro_program ( gro_program, frac ) );
        }

        daughter->init ( q, rep, 1-frac );



        for ( i=0; i<MAX_STATE_NUM; i++ ) q[i] = (int) ceil(frac*q[i]);
        for ( i=0; i<MAX_REP_NUM; i++ ) rep[i] = (int) ceil(frac*rep[i]);

        set_division_indicator(true);
        daughter->set_division_indicator(true);
        daughter->set_daughter_indicator(true);

        return daughter;

    } else return NULL;

}

