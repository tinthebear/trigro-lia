
#include "Micro.h"
//#include "statistics.h"
#ifdef _WIN32
#include "Rands.h"
#endif //_WIN32

Operon::Operon(World * w, bool *p, bool *rn, int *tf, int *rb, bool constit, float *upP, float * errP,float *upR, float * errR, int cir, double * probIn, double * probRep): world(w){

    char name1[20];
    char name2[20];

    proteins = p;
    rnas = rn;
    transFactor = tf;
    ribswitchs = rb;
    constitutive = constit;
    prot_up_times = upP;
    prot_time_error = errP;
    rna_up_times = upR;
    rna_time_error = errR;
    circuit = cir;
    prob_ind_output1_input1 = probIn[0];
    prob_ind_output0_input1 = probIn[1];
    prob_ind_output1_input0 = probIn[2];
    prob_ind_output0_input0 = probIn[3];
    prob_rep_output1_input1 = probRep[0];
    prob_rep_output0_input1 = probRep[1];
    prob_rep_output1_input0 = probRep[2];
    prob_rep_output0_input0 = probRep[3];

    num_proteins = w->get_param("num_proteins");
    operon_id = w->get_next_operon_id();
    w->inc_operon_id();

    name.clear();
    sprintf(name1,"%s","operon");
    sprintf(name2,"%d",operon_id);
    strcat(name1,name2);
    name = name1;
}

Operon::Operon(World * w): world(w){

    char name1[20];
    char name2[20];

    num_proteins = world->get_param("num_proteins");

    proteins = new bool [num_proteins];
    rnas = new bool [num_proteins];
    transFactor = new int [num_proteins];
    ribswitchs = new int [num_proteins];
    constitutive = false;
    prot_up_times = new float [num_proteins];
    prot_time_error = new float [num_proteins];
    rna_up_times = new float [num_proteins];
    rna_time_error = new float [num_proteins];
    circuit = 0;
    prob_ind_output1_input1 = 1.0;
    prob_ind_output0_input1 = 0.0;
    prob_ind_output1_input0 = 0.0;
    prob_ind_output0_input0 = 1.0;
    prob_rep_output1_input1 = 0.0;
    prob_rep_output0_input1 = 1.0;
    prob_rep_output1_input0 = 1.0;
    prob_rep_output0_input0 = 0.0;

    int i;
    for(i=0;i<num_proteins;i++){
        proteins[i]=false;
        rnas[i]=false;
        transFactor[i]=0;
        ribswitchs[i]=false;
        prot_up_times[i]=0.0;
        prot_time_error[i]=0.0;
        rna_up_times[i]=0.0;
        rna_time_error[i]=0.0;
    }

    operon_id = w->get_next_operon_id();
    w->inc_operon_id();

    name.clear();
    sprintf(name1,"%s","operon");
    sprintf(name2,"%d",operon_id);
    strcat(name1,name2);
    name = name1;
}

// COSAS QUE HACER:

// Tiempos en caso de que en una OR se active una cuando la otra se desectiva y aun la proteina no se habia activado. Que tiempo coge?
// En caso de tener un gen un riboswitch delante, este gen no se va a activar nunca por ruido
//


void Operon::update(Cell * c,int op){ //Comprobar si algun FT activa la expresion de algo y comprobar si ya es hora de expresar alguna proteina
    int tf;
    int sal=0;
    int n=0;
    int tf1=-1;
    int tf2=-1;
    bool none = true;
    /*if(is_constitutive()){
        for(sal=0;sal<num_proteins;sal++){
            if(proteins[sal]){

                if(c->get_prot_actv_time(op,sal)==-1 && !c->get_internal_protein(sal) && (ribswitchs[sal]==0 || c->get_internal_RNA(ribswitchs[sal]-1))){
                    float prob = drand48();
                    //std::cout<<"Prob const"<<prob<<std::endl;
                    if(prob<prob_ind_output1_input1){
                        activate(c,op,sal);
                    }
                }
                else
                    check_times(c,op,sal);

            }
        }
    }
    else*/

    if(is_constitutive())
    {
        if(circuit==0)
        {
            for(tf=0;tf<num_proteins;tf++){
                if(transFactor[tf]!=0 ){//&& c->can_express_protein(tf)){
                    none = false;
                }
            }
            if(!none)
            {
                for(tf=0;tf<num_proteins;tf++){
                    if(transFactor[tf]!=0 ){//&& c->can_express_protein(tf)){
                        for(sal=0;sal<num_proteins;sal++){
                            //if(proteins[sal]){
                            if(proteins[sal]){
                                if(ribswitchs[sal]==0 || c->get_internal_RNA(ribswitchs[sal]-1) || c->ribo_is_open(ribswitchs[sal]) )
                                    yes_gate(c,op,tf,sal,is_constitutive(),1);
                            }
                            if(rnas[sal]){
                                if(ribswitchs[sal]==0 || c->get_internal_RNA(ribswitchs[sal]-1) || c->ribo_is_open(ribswitchs[sal]) )
                                    yes_gate(c,op,tf,sal,is_constitutive(),2);
                            }
                        }
                        c->set_last_state(op,tf);
                        c->set_last_state_rna(op,tf);
                    }
                }
            }
            else
            {
                for(sal=0;sal<num_proteins;sal++){
                    //if(proteins[sal]){
                    if(proteins[sal]){
                        if(c->get_prot_actv_time(op,sal)==-1 && !c->get_internal_protein(sal) && (ribswitchs[sal]==0 || c->get_internal_RNA(ribswitchs[sal]-1) || c->ribo_is_open(ribswitchs[sal]))){
                            float prob = drand48();
                            //std::cout<<"Prob const"<<prob<<std::endl;
                            if(prob<prob_ind_output1_input1){
                                c->set_prot_degr_time(op,sal,-1);
                                activate(c,op,sal,1);
                            }
                        }
                        else
                            check_times(c,op,sal);
                    }
                    if(rnas[sal]){
                        if(c->get_rna_actv_time(op,sal)==-1 && !c->get_internal_RNA(sal) && (ribswitchs[sal]==0 || c->get_internal_RNA(ribswitchs[sal]-1) || c->ribo_is_open(ribswitchs[sal]))){
                            float prob = drand48();
                            //std::cout<<"Prob const"<<prob<<std::endl;
                            if(prob<prob_ind_output1_input1){
                                c->set_rna_degr_time(op,sal,-1);
                                activate(c,op,sal,2);
                            }
                        }
                        else
                            check_times(c,op,sal);
                    }
                }
            }
        }
        else if(circuit==1 ||circuit==2 ){ // AND/OR
            for(tf=0; tf<num_proteins;tf++){
                if(n<1 && transFactor[tf]!=0){
                    tf1=tf;
                    n++;
                }
                else if(n==1 && transFactor[tf]!=0){
                    tf2=tf;
                    n++;
                }
            }
            if(n==2)
            {
                if(tf1!=-1 && tf2!=-1 ){//&& c->can_express_protein(tf1) && c->can_express_protein(tf2)){
                    for(sal=0;sal<num_proteins;sal++){
                        //if(proteins[sal] && circuit==1) //AND

                        if((proteins[sal]) && circuit==1) //AND
                            if(c->get_internal_RNA(ribswitchs[sal]-1) || ribswitchs[sal]==0 || c->ribo_is_open(ribswitchs[sal]))
                                and_gate(c,op,tf1,tf2,sal,is_constitutive(),1);

                        if((rnas[sal]) && circuit==1) //AND
                            if(c->get_internal_RNA(ribswitchs[sal]-1) || ribswitchs[sal]==0 || c->ribo_is_open(ribswitchs[sal]))
                                and_gate(c,op,tf1,tf2,sal,is_constitutive(),2);

                        //if(proteins[sal] && circuit==2) //OR
                        if((proteins[sal]) && circuit==2) //OR
                            if(c->get_internal_RNA(ribswitchs[sal]-1) || ribswitchs[sal]==0 || c->ribo_is_open(ribswitchs[sal]))
                                or_gate(c,op,tf1,tf2,sal,is_constitutive(),1);

                        if((rnas[sal]) && circuit==2) //OR
                            if(c->get_internal_RNA(ribswitchs[sal]-1) || ribswitchs[sal]==0 || c->ribo_is_open(ribswitchs[sal]))
                                or_gate(c,op,tf1,tf2,sal,is_constitutive(),2);
                    }
                    c->set_last_state(op,tf1);
                    c->set_last_state(op,tf2);
                    c->set_last_state_rna(op,tf1);
                    c->set_last_state_rna(op,tf2);
                }
            }
            else if(n<2)
            {

            }
            //else
            //    std::cout<<"Se necesitan dos TF para los circuitos AND/OR"<<std::endl;
        }
    }

    else
    {
        if(circuit==0){ //YES
            for(tf=0;tf<num_proteins;tf++){
                if(transFactor[tf]!=0 ){//&& c->can_express_protein(tf)){
                    for(sal=0;sal<num_proteins;sal++){
                        //if(proteins[sal]){
                        if(proteins[sal]){
                            if(ribswitchs[sal]==0 || c->get_internal_RNA(ribswitchs[sal]-1) || c->ribo_is_open(ribswitchs[sal]))
                                yes_gate(c,op,tf,sal,is_constitutive(),1);
                        }
                        if(rnas[sal]){
                            if(ribswitchs[sal]==0 || c->get_internal_RNA(ribswitchs[sal]-1) || c->ribo_is_open(ribswitchs[sal]))
                                yes_gate(c,op,tf,sal,is_constitutive(),2);
                        }
                    }
                    c->set_last_state(op,tf);
                    c->set_last_state_rna(op,tf);
                }
            }
        }
        else if(circuit==1 ||circuit==2 ){ // AND/OR
            for(tf=0; tf<num_proteins;tf++){
                if(n<1 && transFactor[tf]!=0){
                    tf1=tf;
                    n++;
                }
                else if(n==1 && transFactor[tf]!=0){
                    tf2=tf;
                    n++;
                }
            }
            if(tf1!=-1 && tf2!=-1 ){//&& c->can_express_protein(tf1) && c->can_express_protein(tf2)){
                for(sal=0;sal<num_proteins;sal++){
                    //if(proteins[sal] && circuit==1) //AND
                    if((proteins[sal]) && circuit==1) //AND
                        if(c->get_internal_RNA(ribswitchs[sal]-1) || ribswitchs[sal]==0 || c->ribo_is_open(ribswitchs[sal]))
                            and_gate(c,op,tf1,tf2,sal,is_constitutive(),1);
                    if((rnas[sal]) && circuit==1) //AND
                        if(c->get_internal_RNA(ribswitchs[sal]-1) || ribswitchs[sal]==0 || c->ribo_is_open(ribswitchs[sal]))
                            and_gate(c,op,tf1,tf2,sal,is_constitutive(),2);
                    //if(proteins[sal] && circuit==2) //OR
                    if((proteins[sal]) && circuit==2) //OR
                        if(c->get_internal_RNA(ribswitchs[sal]-1) || ribswitchs[sal]==0 || c->ribo_is_open(ribswitchs[sal]))
                            or_gate(c,op,tf1,tf2,sal,is_constitutive(),1);
                    if((rnas[sal]) && circuit==2) //OR
                        if(c->get_internal_RNA(ribswitchs[sal]-1) || ribswitchs[sal]==0 || c->ribo_is_open(ribswitchs[sal]))
                            or_gate(c,op,tf1,tf2,sal,is_constitutive(),2);
                }
                c->set_last_state(op,tf1);
                c->set_last_state(op,tf2);
                c->set_last_state_rna(op,tf1);
                c->set_last_state_rna(op,tf2);
            }
            //else
            //    std::cout<<"Se necesitan dos TF para los circuitos AND/OR"<<std::endl;
        }
    }
    c->update_internal_proteins();
}


//void Operon::yes_gate(Cell * c,int op, int tf,int sal){
void Operon::yes_gate(Cell * c,int op, int tf,int sal, bool constit, int mode){
    //std::cout<<"YES: P"<<tf+1<<"= "<< c->get_internal_protein(tf)<<", TF= "<<transFactor[tf]<<", last= "<<c->get_last_state(op,tf)<<std::endl;
    /* Casos en los que el ruido no se nota:
     if (c->get_internal_protein(tf) && transFactor[tf]==1 && c->get_last_state(op,tf))
     if(!c->get_internal_protein(tf) && transFactor[tf]==-1 && !c->get_last_state(op,tf))
     */
    float prob=drand48()*(1/world->get_sim_dt())*30; //para prob por minuto
    float prob2=drand48();
    double get_x_actv_time, get_x_degr_time;
    bool get_internal_x;
    switch(mode)
    {
        case 1:
            get_x_actv_time = c->get_prot_actv_time(op,sal);
            get_internal_x = c->get_internal_protein(sal);
            get_x_degr_time = c->get_prot_degr_time(op,sal);
            break;
        case 2:
            get_x_actv_time = c->get_rna_actv_time(op,sal);
            get_internal_x = c->get_internal_RNA(sal);
            get_x_degr_time = c->get_rna_degr_time(op,sal);
            break;
        default:
            break;
    }

    if(transFactor[tf]==1 && c->get_internal_protein(tf) && c->can_express_protein(tf)){ // tengo TF INDUCTOR
        if(!c->get_last_state(op,tf)){ //Acaba de haber FT
            if(prob2< prob_ind_output1_input1){
                if(mode == 1)
                {
                    activate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    activate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
            //else
            //    std::cout<<"Ruido: No se ha activado la P"<<sal+1<<" del operon "<<op<<"."<<std::endl;
            //Statistics::GetInstance()->AddInputOutput(op,true,true);
        }
        else if(c->get_last_state(op,tf) && (get_x_actv_time == -1 || get_internal_x)){ //Tengo FT INDUCTOR (esto es por si por ruido no se activo justo cuando hubo TF)
            if(prob2< prob_ind_output1_input1){
                if(mode == 1)
                {
                    activate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    activate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
            //else
            //    std::cout<<"Ruido: No se ha activado la P"<<sal+1<<" del operon "<<op<<"."<<std::endl;
            //Statistics::GetInstance()->AddInputOutput(op,true,true);
        }
    }
    else if (transFactor[tf]==1 && !c->get_internal_protein(tf)){ // NO tengo TF INDUCTOR
        if(!c->get_last_state(op,tf) && !get_internal_x){ //RUIDO en inductor
            if(prob< prob_ind_output1_input0){
                if(mode == 1)
                {
                    c->set_prot_degr_time(op,sal,-1);
                    c->activate_protein(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,1);
                    }
                }
                else if(mode == 2)
                {
                    c->set_rna_degr_time(op,sal,-1);
                    c->activate_rna(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                //std::cout<<"Ruido: Se ha activado la P"<<sal+1<<" del operon "<<op<<"."<<std::endl;
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,true);
            }
            //Statistics::GetInstance()->AddInputOutput(op,false,true);
            check_times(c,op,sal);
        }
        else if(c->get_last_state(op,tf)){ //deja de haber FT PROMOTOR
            if(prob2< prob_ind_output0_input0){
                if(!constit)
                {
                    if(mode == 1)
                    {
                        degradate(c,op,sal,1);
                    }
                    else if(mode == 2)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,false);
            }
            //else
            //    std::cout<<"Ruido: No se ha degradado la P"<<sal+1<<" del operon "<<op<<"."<<std::endl;
            //Statistics::GetInstance()->AddInputOutput(op,false,false);
        }
        else if(!c->get_last_state(op,tf) && get_internal_x && get_x_degr_time == -1){ //no hay FT PROMOTOR (por ruido no se degrado antes)
            if(prob2< prob_ind_output0_input0){
                if(!constit)
                {
                    if(mode == 1)
                    {
                        degradate(c,op,sal,1);
                    }
                    else if(mode == 2)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,false);
            }
            //else
            //    std::cout<<"Ruido: No se ha degradado la P"<<sal+1<<" del operon "<<op<<"."<<std::endl;
            //Statistics::GetInstance()->AddInputOutput(op,false,false);
        }
    }

    else if(transFactor[tf]==-1 && c->get_internal_protein(tf) && c->can_express_protein(tf)){ // tengo TF REPRESOR
        if(c->get_last_state(op,tf) && !get_internal_x){ //RUIDO en represor
            if(prob< prob_rep_output1_input1){
                if(mode == 1)
                {
                    c->activate_protein(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,1);
                    }
                }
                else if(mode == 2)
                {
                    //c->set_rna_degr_time(op,sal,-1);
                    c->activate_rna(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,2);
                    }
                }
            }
            //Statistics::GetInstance()->AddInputOutput(op,true,true);
            check_times(c,op,sal);
        }
        else if(!c->get_last_state(op,tf)){ //Acaba de haber FT REPRESOR
            if(prob2< prob_rep_output0_input1){
                if(mode == 1)
                {
                    degradate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    degradate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(false,c->get_id(),sal,op,world->get_time(),true,false);
            }
            //else
            //    std::cout<<"Ruido: No se ha degradado la P"<<sal+1<<" del operon "<<op<<"."<<std::endl;
            //Statistics::GetInstance()->AddInputOutput(op,true,false);
        }
        else if(c->get_last_state(op,tf) && get_internal_x && get_x_degr_time ==-1){ //Antes habia FT REPRESOR y ahora tambien (por ruido no se degrado antes)
            if(prob2< prob_rep_output0_input1){
                if(mode == 1)
                {
                    degradate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    degradate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(false,c->get_id(),sal,op,world->get_time(),true,false);
            }
            //else
            //    std::cout<<"Ruido: No se ha degradado la P"<<sal+1<<" del operon "<<op<<"."<<std::endl;
            //Statistics::GetInstance()->AddInputOutput(op,true,false);
        }
    }
    else if(transFactor[tf]==-1 && !c->get_internal_protein(tf)){ // NO tengo TF REPRESOR
        if(c->get_last_state(op,tf)){// && !c->get_internal_protein(sal)){ //Acaba de no haber FT REPRESOR
            if(prob2< prob_rep_output1_input0){
                if(!constit)
                {
                    if(mode == 1)
                    {
                        degradate(c,op,sal,1);
                    }
                    else if(mode == 2)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                else
                {
                    if(mode == 1)
                    {
                        c->set_prot_degr_time(op,sal,-1);
                        activate(c,op,sal,1);
                    }
                    else if(mode == 2)
                    {
                        c->set_rna_degr_time(op,sal,-1);
                        activate(c,op,sal,2);
                    }
                }
                //Statistics::GetInstance()->AddInductor(false,c->get_id(),sal,op,world->get_time(),false,true);
            }
           // else
           //     std::cout<<"Ruido: No se ha activado la P"<<sal+1<<" del operon "<<op<<"."<<std::endl;
            //Statistics::GetInstance()->AddInputOutput(op,false,true);
        }
        else if(!c->get_last_state(op,tf) && get_x_actv_time ==-1 && !get_internal_x){ //no tengo FT REPRESOR y antes tampoco (esto es por si por ruido no se activo justo cuando dejo de haber TF)
            if(prob2< prob_rep_output1_input0){
                if(!constit)
                {
                    if(mode == 1)
                    {
                        degradate(c,op,sal,1);
                    }
                    else if(mode == 2)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                else
                {
                    if(mode == 1)
                    {
                        c->set_prot_degr_time(op,sal,-1);
                        activate(c,op,sal,1);
                    }
                    else if(mode == 2)
                    {
                        c->set_rna_degr_time(op,sal,-1);
                        activate(c,op,sal,2);
                    }
                }
                //Statistics::GetInstance()->AddInductor(false,c->get_id(),sal,op,world->get_time(),false,true);
            }
            //else
             //   std::cout<<"Ruido: No se ha activado la P"<<sal+1<<" del operon "<<op<<"."<<std::endl;
            //Statistics::GetInstance()->AddInputOutput(op,false,true);
        }
    }
    check_times(c,op,sal);
}

//void Operon::and_gate(Cell * c,int op, int tf1, int tf2,int sal){
void Operon::and_gate(Cell * c,int op, int tf1, int tf2,int sal, bool constit, int mode){
    //std::cout<<"AND: interna"<<tf1+1<<"= "<< c->get_internal_protein(tf1)<<", TF: "<<transFactor[tf1]<<", last: "<<c->get_last_state(op,tf1)<<std::endl;
    //std::cout<<"AND: interna"<<tf2+1<<"= "<< c->get_internal_protein(tf2)<<", TF: "<<transFactor[tf2]<<", last: "<<c->get_last_state(op,tf2)<<std::endl;
    /*Casos en los que el ruido no se nota (input1/output0):
      TF1=1,  TF2=1,  ambos estan ahora y antes tambien
      TF1=-1, TF2=-1, no hay ninguno y antes tampoco
      TF1=-1, TF2 =1, no hay represor,si inductor y antes igual
      TF1=1,  TF2=-1, no hay represor,si inductor y antes igual
    */
    float prob=drand48()*(1/world->get_sim_dt());
    float prob2=drand48();

    double get_x_actv_time, get_x_degr_time;
    bool get_internal_x;
    switch(mode)
    {
        case 1:
            get_x_actv_time = c->get_prot_actv_time(op,sal);
            get_internal_x = c->get_internal_protein(sal);
            get_x_degr_time = c->get_prot_degr_time(op,sal);
            break;
        case 2:
            get_x_actv_time = c->get_rna_actv_time(op,sal);
            get_internal_x = c->get_internal_RNA(sal);
            get_x_degr_time = c->get_rna_degr_time(op,sal);
            break;
        default:
            break;
    }

    if(transFactor[tf1]==1 && transFactor[tf2]==1){
        if((c->get_internal_protein(tf1) && c->get_internal_protein(tf2) && c->can_express_protein(tf1) && c->can_express_protein(tf2) && c->get_last_state(op,tf1) && c->get_last_state(op,tf2) && !constit) ||
           (constit && (c->get_last_state(op,tf1) && c->get_last_state(op,tf2))))
            //Creo que aqui va el 4to ruido...
            check_times(c,op,sal);

        else if((c->get_internal_protein(tf1) && c->can_express_protein(tf1) && c->get_internal_protein(tf2) && c->can_express_protein(tf2) && !constit) ||
               (constit && !(c->get_last_state(op,tf1) && c->get_last_state(op,tf2)))){
            if(prob2< prob_ind_output1_input1){
                if(mode == 1)
                {
                    c->set_prot_degr_time(op,sal,-1);
                    activate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    c->set_rna_degr_time(op,sal,-1);
                    activate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }
        else if(c->get_last_state(op,tf1) && c->get_last_state(op,tf2)){
            if(prob2< prob_ind_output0_input0){
                if(!constit)
                {
                    if(mode == 1)
                    {
                        degradate(c,op,sal,1);
                    }
                    else if(mode == 2)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                /*else
                {
                    activate(c,op,sal);
                }*/
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,false);
            }
        }
        else{
            if(prob<prob_ind_output1_input0){
                if (mode == 1)
                {
                    c->activate_protein(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,1);
                    }
                }
                else if (mode == 2)
                {
                    c->activate_rna(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,true);
            }
            check_times(c,op,sal);
        }
    }
    else if(transFactor[tf1]==-1 && transFactor[tf2]==-1){
        if(!c->get_internal_protein(tf1) && !c->get_internal_protein(tf2) && !c->get_last_state(op,tf1) && !c->get_last_state(op,tf2) && c->get_prot_actv_time(op,sal)!=-1 && c->get_rna_actv_time(op,sal)!=-1){
            //4to ruido
            check_times(c,op,sal);
        }

        /*else if(!c->get_internal_protein(tf1) && !c->get_internal_protein(tf2) && c->get_prot_actv_time(op,sal)==-1 && c->get_rna_actv_time(op,sal)==-1){
            if(prob2< prob_ind_output1_input1){
                activate(c,op,sal);
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }*/
        else if(!c->get_internal_protein(tf1) && !c->get_internal_protein(tf2) && get_x_actv_time == -1){
            if(prob2< prob_ind_output1_input1){
                if(mode == 1)
                {
                    c->set_prot_degr_time(op,sal,-1);
                    activate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    c->set_rna_degr_time(op,sal,-1);
                    activate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }
        else if(!c->get_last_state(op,tf1) && !c->get_last_state(op,tf2) && (c->get_internal_protein(tf1) || c->get_internal_protein(tf2))){
            if(prob2< prob_ind_output0_input0){
                if(mode == 1)
                {
                    degradate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    degradate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,false);
            }
        }
        else{
            if(prob<prob_ind_output1_input0){
                if(mode == 1)
                {
                    c->activate_protein(op,sal);
                    degradate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    c->activate_rna(op,sal);
                    degradate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,true);
            }
            check_times(c,op,sal);
        }
    }
    else if(transFactor[tf1]==-1 && transFactor[tf2]==1){
        /*if((!c->get_last_state(op,tf1) && c->get_last_state(op,tf2) && !c->get_internal_protein(tf1) && c->get_internal_protein(tf2) && c->can_express_protein(tf1) && !constit) ||
           (constit && (!c->get_last_state(op,tf1) && !c->get_internal_protein(tf1)) && (c->get_prot_actv_time(op,sal)!=-1 || c->get_rna_actv_time(op,sal)!=-1)))
            //4to ruido
            check_times(c,op,sal);*/
        if((!c->get_last_state(op,tf1) && c->get_last_state(op,tf2) && !c->get_internal_protein(tf1) && c->get_internal_protein(tf2) && c->can_express_protein(tf1) && !constit) ||
          (constit && (!c->get_last_state(op,tf1) && !c->get_internal_protein(tf1)) && get_x_actv_time!=-1))
            //4to ruido
            check_times(c,op,sal);

        /*else if((!c->get_internal_protein(tf1) && c->get_internal_protein(tf2) && c->can_express_protein(tf2) && !constit) ||
                (constit && !c->get_internal_protein(tf1) && c->get_prot_actv_time(op,sal)==-1 && c->get_rna_actv_time(op,sal)==-1)){
            if(prob2< prob_ind_output1_input1){
                if(mode == 1)
                {
                    activate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    activate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }*/
        else if((!c->get_internal_protein(tf1) && c->get_internal_protein(tf2) && c->can_express_protein(tf2) && !constit) ||
               (constit && !c->get_internal_protein(tf1) && get_x_actv_time == -1)){
            if(prob2< prob_ind_output1_input1){
                if(mode == 1)
                {
                    c->set_prot_degr_time(op,sal,-1);
                    activate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    c->set_rna_degr_time(op,sal,-1);
                    activate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }

        else if((!c->get_last_state(op,tf1) && c->get_last_state(op,tf2) && !constit) ||
                (constit && (c->get_internal_protein(tf1)))){
            if(prob2< prob_ind_output0_input0){
                /*if(!c->get_internal_protein(tf1) && constit)
                {
                    activate(c,op,sal);
                }
                else
                {*/
                if(mode == 1)
                {
                    degradate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    degradate(c,op,sal,2);
                }
                //}
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,false);
            }
        }
        else{
            if(prob<prob_ind_output1_input0){
                if(mode == 1)
                {
                    c->activate_protein(op,sal);
                    if(c->get_internal_protein(tf1) && !constit)
                    {
                        degradate(c,op,sal,1);
                    }
                }
                else if(mode == 2)
                {
                    c->activate_rna(op,sal);
                    if(c->get_internal_protein(tf1) && !constit)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,true);
            }
            check_times(c,op,sal);
        }
    }
    else if(transFactor[tf1]==1 && transFactor[tf2]==-1){
        /*if((!c->get_last_state(op,tf2) && c->get_last_state(op,tf1) && !c->get_internal_protein(tf2) && c->get_internal_protein(tf1) && c->can_express_protein(tf2) && !constit) ||
           (constit && (!c->get_last_state(op,tf2) && !c->get_internal_protein(tf2)) && (c->get_prot_actv_time(op,sal)!=-1 || c->get_rna_actv_time(op,sal)!=-1)))
            //4to ruido
            check_times(c,op,sal);*/
        if((!c->get_last_state(op,tf2) && c->get_last_state(op,tf1) && !c->get_internal_protein(tf2) && c->get_internal_protein(tf1) && c->can_express_protein(tf2) && !constit) ||
           (constit && (!c->get_last_state(op,tf2) && !c->get_internal_protein(tf2)) && get_x_actv_time!=-1))
            //4to ruido
            check_times(c,op,sal);

        /*else if((c->get_internal_protein(tf1) && !c->get_internal_protein(tf2) && c->can_express_protein(tf1) && !constit) ||
                (constit && !c->get_internal_protein(tf2) && c->get_prot_actv_time(op,sal)==-1 && c->get_rna_actv_time(op,sal)==-1)){
            if(prob2< prob_ind_output1_input1){
                activate(c,op,sal);
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }*/
        else if((c->get_internal_protein(tf1) && !c->get_internal_protein(tf2) && c->can_express_protein(tf1) && !constit) ||
                (constit && !c->get_internal_protein(tf2) && get_x_actv_time ==-1)){
            if(prob2< prob_ind_output1_input1){
                if(mode == 1)
                {
                    c->set_prot_degr_time(op,sal,-1);
                    activate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    c->set_rna_degr_time(op,sal,-1);
                    activate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }
        else if((c->get_last_state(op,tf1) && !c->get_last_state(op,tf2) && !constit) ||
                (constit && (c->get_internal_protein(tf2)))){
            if(prob2< prob_ind_output0_input0){
                /*if(!c->get_internal_protein(tf2) && constit)
                {
                    activate(c,op,sal);
                }
                else
                {*/
                if(mode == 1)
                {
                    degradate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    degradate(c,op,sal,2);
                }
                //}
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,false);
            }
        }
        else{
            if(prob<prob_ind_output1_input0){
                if(mode == 1)
                {
                    c->activate_protein(op,sal);
                    if(c->get_internal_protein(tf2) && !constit)
                    {
                        degradate(c,op,sal,1);
                    }
                }
                else if (mode == 2)
                {
                    c->activate_rna(op,sal);
                    if(c->get_internal_protein(tf2) && !constit)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,true);
            }
            check_times(c,op,sal);
        }
    }
}

//void Operon::or_gate(Cell *c,int op, int tf1, int tf2,int sal){
void Operon::or_gate(Cell *c,int op, int tf1, int tf2,int sal, bool constit, int mode){
    //std::cout<<"OR: interna"<<tf1+1<<"= "<< c->get_internal_protein(tf1)<<", TF: "<<transFactor[tf1]<<", last: "<<c->get_last_state(op,tf1)<<std::endl;
    //std::cout<<"OR: interna"<<tf2+1<<"= "<< c->get_internal_protein(tf2)<<", TF: "<<transFactor[tf2]<<", last: "<<c->get_last_state(op,tf2)<<std::endl;
    /* Casos en los que el ruido no se nota (input1/output0):
      TF1=1,  TF2=1,  no hay ninguno de los dos y antes tampoco
      TF1=-1, TF2=-1, estan los dos y antes tambien
      TF1=-1, TF2 =1, solo hay represor y antes tambien
      TF1=1,  TF2=-1, solo hay represor y antes tambien
     */
    float prob=drand48()*(1/world->get_sim_dt());
    float prob2=drand48();

    double get_x_actv_time, get_x_degr_time;
    bool get_internal_x;
    switch(mode)
    {
        case 1:
            get_x_actv_time = c->get_prot_actv_time(op,sal);
            get_internal_x = c->get_internal_protein(sal);
            get_x_degr_time = c->get_prot_degr_time(op,sal);
            break;
        case 2:
            get_x_actv_time = c->get_rna_actv_time(op,sal);
            get_internal_x = c->get_internal_RNA(sal);
            get_x_degr_time = c->get_rna_degr_time(op,sal);
            break;
        default:
            break;
    }

    if(transFactor[tf1]==1 && transFactor[tf2]==1){
        if((!c->get_internal_protein(tf1) && !c->get_internal_protein(tf2) && !c->get_last_state(op,tf1) && !c->get_last_state(op,tf2) && !constit) ||
           (constit && (c->get_last_state(op,tf1) || c->get_last_state(op,tf2))))
            check_times(c,op,sal);

        else if(!c->get_internal_protein(tf1) && !c->get_internal_protein(tf2) && !constit){
            if(prob2< prob_ind_output0_input0){
                //if(!constit)
                //{
                if(mode == 1)
                {
                    degradate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    degradate(c,op,sal,2);
                }
                /*}
                else
                {
                    activate(c,op,sal);
                }*/
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,false);
            }
        }

        else if((!c->get_last_state(op,tf1) && !c->get_last_state(op,tf2) && !constit) ||
                (constit && (!c->get_last_state(op,tf1) && !c->get_last_state(op,tf2)))){
            if(prob2< prob_ind_output1_input1){
                if(mode == 1)
                {
                    c->set_prot_degr_time(op,sal,-1);
                    activate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    c->set_rna_degr_time(op,sal,-1);
                    activate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }

        else{
            if(prob<prob_ind_output1_input0){
                if(mode == 1)
                {
                    c->activate_protein(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,1);
                    }
                }
                else if (mode == 2)
                {
                    c->activate_rna(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,true);
            }
            check_times(c,op,sal);
        }
    }
    else if(transFactor[tf1]==-1 && transFactor[tf2]==-1){
        if(c->get_last_state(op,tf1) && c->get_last_state(op,tf2) && c->can_express_protein(tf1) && c->can_express_protein(tf2) && c->get_internal_protein(tf1) && c->get_internal_protein(tf2))
            check_times(c,op,sal);

        else if(c->get_internal_protein(tf1) && c->get_internal_protein(tf2) && c->can_express_protein(tf1) && c->can_express_protein(tf2)){
            if(prob2< prob_ind_output0_input0){
                if(mode == 1)
                {
                    degradate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    degradate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,false);
            }
        }

        //else if(c->get_last_state(op,tf1) && c->get_last_state(op,tf2) && (!c->get_internal_protein(tf1) || !c->get_internal_protein(tf2))){
        else if((!c->get_internal_protein(tf1) || !c->get_internal_protein(tf2)) && ((get_x_actv_time ==-1))){
            if(prob2< prob_ind_output1_input1){
                //OJO CON ESTE!!! SE ESTA SUPONIENDO QUE POR SER COMPUESTO Y PUERTA OR, INCLUSO CON REPRESOR NO BLOQUEA
                //TAMBIEN OJO CON EL RNA!!! Solo estoy comprobando tiempos de activacion de proteina
                if(mode == 1)
                {
                    c->set_prot_degr_time(op,sal,-1);
                    activate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    c->set_rna_degr_time(op,sal,-1);
                    activate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }

        else{
            if(prob<prob_ind_output1_input0){
                if(mode == 1)
                {
                    c->activate_protein(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,1);
                    }
                }
                else if(mode == 2)
                {
                    c->activate_rna(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,true);
            }
            check_times(c,op,sal);
        }
    }
    else if(transFactor[tf1]==-1 && transFactor[tf2]==1){
        /*if((c->get_last_state(op,tf1) && !c->get_last_state(op,tf2) && c->get_internal_protein(tf1) && !c->get_internal_protein(tf2) && c->can_express_protein(tf1) && !constit) ||
           (constit && ((c->get_prot_actv_time(op,sal)!=-1) || (c->get_rna_actv_time(op,sal)!=-1))))
            check_times(c,op,sal);*/

        if((c->get_last_state(op,tf1) && !c->get_last_state(op,tf2) && c->get_internal_protein(tf1) && !c->get_internal_protein(tf2) && c->can_express_protein(tf1) && !constit) ||
           (constit && get_x_actv_time!=-1))
            check_times(c,op,sal);

        else if(c->get_internal_protein(tf1) && c->can_express_protein(tf1) && !c->get_internal_protein(tf2) && !constit){
            if(prob2< prob_ind_output0_input0){
                if(mode == 1)
                {
                    degradate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    degradate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,false);
            }
        }

        //else if(((!c->get_last_state(op,tf1) || !c->get_last_state(op,tf2)) && (!c->get_internal_protein(tf1) || c->get_internal_protein(tf2)) && !constit) ||
        /*else if(((c->get_prot_actv_time(op,sal)==-1 && c->get_rna_actv_time(op,sal)==-1) && (!c->get_internal_protein(tf1) || c->get_internal_protein(tf2)) && !constit) ||
                //OJO... Revisa lo de la funcion de void set_last_initial_state en Cell.cpp y Gro.cpp... parece muy rara.
                (constit && c->get_prot_actv_time(op,sal)==-1 && c->get_rna_actv_time(op,sal)==-1)){
            if(prob2< prob_ind_output1_input1){
                activate(c,op,sal);
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }*/

        else if(((get_x_actv_time ==-1) && (!c->get_internal_protein(tf1) || c->get_internal_protein(tf2)) && !constit) ||
                //OJO... Revisa lo de la funcion de void set_last_initial_state en Cell.cpp y Gro.cpp... parece muy rara.
                (constit && get_x_actv_time==-1)){
            if(prob2< prob_ind_output1_input1){
                if(mode == 1)
                {
                    c->set_prot_degr_time(op,sal,-1);
                    activate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    c->set_rna_degr_time(op,sal,-1);
                    activate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }

        else{
            if(prob<prob_ind_output1_input0){
                if(mode == 1)
                {
                    c->activate_protein(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,1);
                    }
                }
                else if(mode == 2)
                {
                    c->activate_rna(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,true);
            }
            check_times(c,op,sal);
        }
    }
    else if(transFactor[tf1]==1 && transFactor[tf2]==-1){
        /*if((!c->get_last_state(op,tf1) && c->get_last_state(op,tf2) && !c->get_internal_protein(tf1) && c->get_internal_protein(tf2) && c->can_express_protein(tf2) && !constit) ||
           (constit && ((c->get_prot_actv_time(op,sal)!=-1) || (c->get_rna_actv_time(op,sal)!=-1))))
            check_times(c,op,sal);*/

        if((!c->get_last_state(op,tf1) && c->get_last_state(op,tf2) && !c->get_internal_protein(tf1) && c->get_internal_protein(tf2) && c->can_express_protein(tf2) && !constit) ||
           (constit && get_x_actv_time!=-1))
            check_times(c,op,sal);

        else if(!c->get_internal_protein(tf1) && c->get_internal_protein(tf2) && c->can_express_protein(tf2) && !constit){
            if(prob2< prob_ind_output0_input0){
                if(mode == 1)
                {
                    degradate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    degradate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,false);
            }
        }

        //else if((!c->get_last_state(op,tf1) && c->get_last_state(op,tf2) && (c->get_internal_protein(tf1) || !c->get_internal_protein(tf2)) && !constit) ||
        /*else if(((c->get_prot_actv_time(op,sal)==-1 && c->get_rna_actv_time(op,sal)==-1) && (c->get_internal_protein(tf1) || !c->get_internal_protein(tf2)) && !constit) ||
                (constit && c->get_prot_actv_time(op,sal)==-1 && c->get_rna_actv_time(op,sal)==-1)){
            if(prob2< prob_ind_output1_input1){
                activate(c,op,sal);
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }*/

        else if(((get_x_actv_time == -1) && (c->get_internal_protein(tf1) || !c->get_internal_protein(tf2)) && !constit) ||
                (constit && get_x_actv_time==-1)){
            if(prob2< prob_ind_output1_input1){
                if(mode == 1)
                {
                    c->set_prot_degr_time(op,sal,-1);
                    activate(c,op,sal,1);
                }
                else if(mode == 2)
                {
                    c->set_rna_degr_time(op,sal,-1);
                    activate(c,op,sal,2);
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),true,true);
            }
        }

        else{
            if(prob<prob_ind_output1_input0){
                if(mode == 1)
                {
                    c->activate_protein(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,1);
                    }
                }
                else if (mode == 2)
                {
                    c->activate_rna(op,sal);
                    if(!constit)
                    {
                        degradate(c,op,sal,2);
                    }
                }
                //Statistics::GetInstance()->AddInductor(true,c->get_id(),sal,op,world->get_time(),false,true);
            }
            check_times(c,op,sal);
        }
    }
}

void Operon::activate(Cell * c,int op,int sal, int mode){
    if(mode == 1)
    {
        if(c->get_prot_degr_time(op,sal)!=-1){
            c->set_prot_degr_time(op,sal,-1);
        }
        else if(c->get_prot_actv_time(op,sal)==-1 && !c->get_protein_operon(op,sal))
        {
                c->set_prot_actv_time(op,sal,world->get_time() + drand48() * ((prot_up_times[sal]+prot_time_error[sal])-(prot_up_times[sal]-prot_time_error[sal]))+(prot_up_times[sal]-prot_time_error[sal])); //guardamos su tiempo de activacion
                //std::cout<<"Se va a activar la P"<<sal+1<<" del operon"<<op+1<<" en el minuto "<< c->get_prot_actv_time(op,sal)<<std::endl;
        }
    }
    else if(mode == 2)
    {
        if(c->get_rna_degr_time(op,sal)!=-1){
            c->set_rna_degr_time(op,sal,-1);
        }
        else if(c->get_rna_actv_time(op,sal)==-1 && !c->get_rna_operon(op,sal))
        {
            c->set_rna_actv_time(op,sal,world->get_time() + drand48() * ((world->get_rna_down_time(sal)+world->get_rna_down_time_error(sal))-(world->get_rna_down_time(sal)-world->get_rna_down_time_error(sal)))+(world->get_rna_down_time(sal)-world->get_rna_down_time_error(sal))); //guardamos su tiempo de degradacion
            //std::cout<<"Se va a activar el RNA"<<sal+1<<" del operon"<<op+1<<" en el minuto "<< c->get_rna_actv_time(op,sal)<<std::endl;
        }
    }

    /*if(c->get_prot_degr_time(op,sal)!=-1){
        if(c->get_rna_degr_time(op,sal)!=-1){//si se estaban degradando la proteina y el rna
            c->set_prot_degr_time(op,sal,-1);
            c->set_rna_degr_time(op,sal,-1);
        }
        else{//si se estaba degradando solo la proteina
            c->set_prot_degr_time(op,sal,-1);
            c->set_rna_actv_time(op,sal,world->get_time() + drand48() * ((world->get_rna_down_time(sal)+world->get_rna_down_time_error(sal))-(world->get_rna_down_time(sal)-world->get_rna_down_time_error(sal)))+(world->get_rna_down_time(sal)-world->get_rna_down_time_error(sal))); //guardamos su tiempo de degradacion
            //std::cout<<"Se va a activar el RNA"<<sal+1<<" del operon"<<op+1<<" en el minuto "<< c->get_rna_actv_time(op,sal)<<std::endl;
        }
    }
    else{
        if(c->get_prot_actv_time(op,sal)==-1 && !c->get_protein_operon(op,sal)){
            c->set_prot_actv_time(op,sal,world->get_time() + drand48() * ((prot_up_times[sal]+prot_time_error[sal])-(prot_up_times[sal]-prot_time_error[sal]))+(prot_up_times[sal]-prot_time_error[sal])); //guardamos su tiempo de activacion
            //std::cout<<"Se va a activar la P"<<sal+1<<" del operon"<<op+1<<" en el minuto "<< c->get_prot_actv_time(op,sal)<<std::endl;
        }
        if(c->get_rna_actv_time(op,sal)==-1 && !c->get_rna_operon(op,sal)){
            c->set_rna_actv_time(op,sal,world->get_time() + drand48() * ((rna_up_times[sal]+rna_time_error[sal])-(rna_up_times[sal]-rna_time_error[sal]))+(rna_up_times[sal]-rna_time_error[sal])); //guardamos su tiempo de activacion
            //std::cout<<"Se va a activar el RNA"<<sal+1<<" del operon"<<op+1<<" en el minuto "<< c->get_rna_actv_time(op,sal)<<std::endl;
        }
    }*/
}

void Operon::degradate(Cell * c,int op,int sal, int mode){

    if(mode == 1)
    {
        if(c->get_prot_actv_time(op,sal)!=-1){
            c->set_prot_actv_time(op,sal,-1);
        }
        else if(c->get_prot_degr_time(op,sal)==-1 && c->get_protein_operon(op,sal))
        {
            c->set_prot_degr_time(op,sal,world->get_time() + drand48() * ((world->get_prot_down_time(sal)+world->get_prot_down_time_error(sal))-(world->get_prot_down_time(sal)-world->get_prot_down_time_error(sal)))+(world->get_prot_down_time(sal)-world->get_prot_down_time_error(sal))); //guardamos su tiempo de degradacion
           // std::cout<<"Se va a degradar la P"<<sal+1<<" del operon"<<op+1<<" en el minuto "<< c->get_prot_degr_time(op,sal)<<std::endl;
        }
    }
    else if (mode == 2)
    {
        if(c->get_rna_actv_time(op,sal)!=-1){
            c->set_rna_actv_time(op,sal,-1);
        }
        else if(c->get_rna_degr_time(op,sal)==-1 && c->get_rna_operon(op,sal))
        {
            c->set_rna_degr_time(op,sal,world->get_time() + drand48() * ((world->get_rna_down_time(sal)+world->get_rna_down_time_error(sal))-(world->get_rna_down_time(sal)-world->get_rna_down_time_error(sal)))+(world->get_rna_down_time(sal)-world->get_rna_down_time_error(sal))); //guardamos su tiempo de degradacion
            //std::cout<<"Se va a degradar el RNA"<<sal+1<<" del operon"<<op+1<<" en el minuto "<< c->get_rna_degr_time(op,sal)<<std::endl;
        }
    }

    //std::cout<<"degradate:"<<"P"<<sal+1<<" del operon"<<op+1<<": prot_degr_time"<<c->get_prot_degr_time(op,sal)<<", rna_degr_time: "<< c->get_rna_degr_time(op,sal)<<", prot_op: "<<c->get_protein_operon(op,sal)<<", rna_op: "<<c->get_rna_operon(op,sal)<<std::endl;
    /*if(c->get_prot_actv_time(op,sal)!=-1){
        if(c->get_rna_actv_time(op,sal)!=-1){ //si se estaban activando la proteina y el rna
            c->set_prot_actv_time(op,sal,-1);
            c->set_rna_actv_time(op,sal,-1);
        }
        else{ //si se estaba activando solo la proteina
            c->set_prot_actv_time(op,sal,-1);
            c->set_rna_degr_time(op,sal,world->get_time() + drand48() * ((world->get_rna_down_time(sal)+world->get_rna_down_time_error(sal))-(world->get_rna_down_time(sal)-world->get_rna_down_time_error(sal)))+(world->get_rna_down_time(sal)-world->get_rna_down_time_error(sal))); //guardamos su tiempo de degradacion
           // std::cout<<"Se va a degradar el RNA"<<sal+1<<" del operon"<<op+1<<" en el minuto "<< c->get_rna_degr_time(op,sal)<<std::endl;
        }
    }
    else{ //no se estaba activando ninguno
        if(c->get_prot_degr_time(op,sal)==-1 && c->get_protein_operon(op,sal)){
            c->set_prot_degr_time(op,sal,world->get_time() + drand48() * ((world->get_prot_down_time(sal)+world->get_prot_down_time_error(sal))-(world->get_prot_down_time(sal)-world->get_prot_down_time_error(sal)))+(world->get_prot_down_time(sal)-world->get_prot_down_time_error(sal))); //guardamos su tiempo de degradacion
           // std::cout<<"Se va a degradar la P"<<sal+1<<" del operon"<<op+1<<" en el minuto "<< c->get_prot_degr_time(op,sal)<<std::endl;
        }
        if(c->get_rna_degr_time(op,sal)==-1 && c->get_rna_operon(op,sal)){
            c->set_rna_degr_time(op,sal,world->get_time() + drand48() * ((world->get_rna_down_time(sal)+world->get_rna_down_time_error(sal))-(world->get_rna_down_time(sal)-world->get_rna_down_time_error(sal)))+(world->get_rna_down_time(sal)-world->get_rna_down_time_error(sal))); //guardamos su tiempo de degradacion
            //std::cout<<"Se va a degradar el RNA"<<sal+1<<" del operon"<<op+1<<" en el minuto "<< c->get_rna_degr_time(op,sal)<<std::endl;
        }
    }*/
}

void Operon::check_times(Cell * c,int op, int i){
    //std::cout<<"P"<<i+1<<" del operon"<<op+1<<": prot_actv_time="<< c->get_prot_actv_time(op,i)<<", rna_actv_time="<< c->get_rna_actv_time(op,i) <<std::endl;
    if(c->get_prot_actv_time(op,i)!=-1 && world->get_time() >= c->get_prot_actv_time(op,i)){ // ya es hora de activar la proteina i
        //std::cout<<"Se ha activado la P"<<i+1<<" del operon"<<op+1<<" en el minuto "<< world->get_time()<<std::endl;
        c->activate_protein(op,i);
        c->set_prot_actv_time(op,i,-1);
        /*c->update_internal_proteins();
        c->check_action(i);*/
    }
    else if(c->get_prot_degr_time(op,i)!=-1 && world->get_time() >= c->get_prot_degr_time(op,i)){ // ya es hora de degradar la proteina i
        //std::cout<<"Se ha degradado la P"<<i+1<<" del operon"<<op+1<<" en el minuto "<< world->get_time()<<std::endl;
        c->repress_protein(op,i);
        c->set_prot_degr_time(op,i,-1);
        /*c->update_internal_proteins();
        c->check_action(i);*/
    }
    if(c->get_rna_actv_time(op,i)!=-1 && world->get_time() >= c->get_rna_actv_time(op,i)){ // ya es hora de activar el rna i
        //std::cout<<"actv time"<<c->get_rna_actv_time(op,i)<<std::endl; //
        //std::cout<<"Se ha activado el RNA"<<i+1<<" del operon"<<op+1<<" en el minuto "<< world->get_time()<<std::endl;
        c->activate_rna(op,i);
        c->set_rna_actv_time(op,i,-1);
    }
    else if(c->get_rna_degr_time(op,i)!=-1 && world->get_time() >= c->get_rna_degr_time(op,i)){ // ya es hora de degradar el rna i
        //std::cout<<"Se ha degradado el RNA"<<i+1<<" del operon"<<op+1<<" en el minuto "<< world->get_time()<<std::endl;
        c->repress_rna(op,i);
        c->set_rna_degr_time(op,i,-1);
    }
}


