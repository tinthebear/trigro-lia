/////////////////////////////////////////////////////////////////////////////////////////
//
// gro is protected by the UW OPEN SOURCE LICENSE, which is summarized here.
// Please see the file LICENSE.txt for the complete license.
//
// THE SOFTWARE (AS DEFINED BELOW) AND HARDWARE DESIGNS (AS DEFINED BELOW) IS PROVIDED
// UNDER THE TERMS OF THISS OPEN SOURCE LICENSE (“LICENSE”).  THE SOFTWARE IS PROTECTED
// BY COPYRIGHT AND/OR OTHER APPLICABLE LAW.  ANY USE OF THISS SOFTWARE OTHER THAN AS
// AUTHORIZED UNDER THISS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
//
// BY EXERCISING ANY RIGHTS TO THE SOFTWARE AND/OR HARDWARE PROVIDED HERE, YOU ACCEPT AND
// AGREE TO BE BOUND BY THE TERMS OF THISS LICENSE.  TO THE EXTENT THISS LICENSE MAY BE
// CONSIDERED A CONTRACT, THE UNIVERSITY OF WASHINGTON (“UW”) GRANTS YOU THE RIGHTS
// CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
//
// TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
//
//

#include "Micro.h"
#include "EColi.h"
#include "Yeast.h"
#include "Programs.h"
#include "Rands.h"
//#include "CellEngine.h"

static gro_Program * current_gro_program = NULL;
static Cell * current_cell = NULL;

bool parse_error = false;

Program * split_gro_program ( Program * parent, float frac ) {

    Program * child = parent->copy();
    SymbolTable * symtab = parent->get_symtab();
    SymbolTable * new_symtab = child->get_symtab();

    //printf ( "before: %d %d\n", symtab->get("yfp")->int_value(), new_symtab->get("yfp")->int_value() );

    symtab->divide ( new_symtab, frac );

    //printf ( "after: %d %d\n", symtab->get("yfp")->int_value(), new_symtab->get("yfp")->int_value() );

    return child;

}

//REVISAR!!!
Value * new_ecoli ( std::list<Value *> * args, Scope * s ) {

    std::list<Value *>::iterator i = args->begin();
    Value * settings = *i; i++;
    Value * Plas = *i; i++;
    Value * Prot = *i; i++;
    Value * mrna = *i;i++;

    World * world = current_gro_program->get_world();

    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();
    std::map<std::string,int> prMap = world->get_protein_and_rna_map();

    ASSERT ( args->size() == 5 );

    Program * prog = (*i)->program_value()->copy(); // this copy is deleted in ~Cell?
    //printf ( ">> prog = %x in new_ecoli\n", prog );

    float x = 0, y = 0, theta = 0, vol = (float) fRand(1.57, 3.14);
    int n = 0, j=0;

    if ( settings->get_type() == Value::RECORD ) {

        if ( settings->getField ( "nplasmids" ) )
            n = settings->getField ( "nplasmids" )->int_value();

        if ( settings->getField ( "x" ) )
            x = settings->getField ( "x" )->real_value();

        if ( settings->getField ( "y" ) )
            y = settings->getField ( "y" )->real_value();

        if ( settings->getField ( "theta" ) )
            theta = settings->getField ( "theta" )->real_value();

        if ( settings->getField ( "volume" ) )
            vol = settings->getField ( "volume" )->real_value();
        else
            //vol = DEFAULT_ECOLI_INIT_SIZE;
            vol = (float) fRand(1.57, 2.355);

    } else {

        fprintf ( stderr, "First argument to 'ecoli' should be a record\n" );
        exit ( -1 );

    }

    EColi * c = new EColi ( world, x, y, theta, vol );

    //int k=0;
    for ( j=0; j<plasmids_Map.size(); j++ ) {
        c->setPlasmid(j, false);
    }

    for ( i=Plas->list_value()->begin(); i != Plas->list_value()->end(); i++ ) {
        c->setPlasmid(plasmids_Map[(*i)->string_value()], true);
    }
    /*for ( i=Plas->list_value()->begin(); i != Plas->list_value()->end(); i++ ) {
        c->setPlasmid(k, (*i)->bool_value());
        k++;
    }
    k=0;*/
    for ( j=0; j<world->get_param("num_proteins"); j++ ) {
        c->set_initial_protein(j, false);
    }
    for ( i=Prot->list_value()->begin(); i != Prot->list_value()->end(); i++ ) {
        c->set_initial_protein(prMap[(*i)->string_value()], true);
    }
    /*for ( i=Prot->list_value()->begin(); i != Prot->list_value()->end(); i++ ) {
        c->set_initial_protein(k, (*i)->bool_value());
        k++;
    }
    k=0;*/

    for ( j=0; j<world->get_param("num_proteins"); j++ ) {
        c->set_initial_rna(j, false);
    }
    for ( i=mrna->list_value()->begin(); i != mrna->list_value()->end(); i++ ) {
        c->set_initial_rna(prMap[(*i)->string_value()], true);
    }
    /*for ( i=mrna->list_value()->begin(); i != mrna->list_value()->end(); i++ ) {
        c->set_initial_rna(k, (*i)->bool_value());
        k++;
    }*/
    //c->update_internal_proteins();

    //for (k=0;k<world->num_operons();k++) {
    //    for (j=0;j<world->get_param("num_proteins");j++) {
    //        c->set_last_state(k,j);
    //    }
    //}

    current_cell = c;
    c->set_gro_program ( prog ); // prog is deleted if/when the cell is deleted in ~Cell
    world->add_cell ( c );
    prog->init_params ( current_gro_program->get_scope() );
    prog->init ( current_gro_program->get_scope() );
    c->set_initial_last_state();
    current_cell = NULL;

    return new Value(Value::UNIT);

}

//REVISAR!!!
Value * new_ecolis_random_circle ( std::list<Value *> * args, Scope * s ) {

    std::list<Value *>::iterator i = args->begin();
    std::list<Value *>::iterator h;
    int j = 0;


    int n_ecoli = (*i)->int_value();
    i++;
    double x_center = (*i)->real_value();
    i++;
    double y_center = (*i)->real_value();
    i++;
    double max_radius = (*i)->real_value();
    i++;
    /*int n = (*i)->int_value();
    i++;*/
    Value * Plas = *i; i++;
    Value * Prot = *i; i++;
    Value * mrna = *i; i++;

    //Value * settings = *i; i++;

    World * world = current_gro_program->get_world();

    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();
    std::map<std::string,int> prMap = world->get_protein_and_rna_map();

    //ASSERT ( args->size() == 6 );
    ASSERT ( args->size() == 8 );

    //Program * prog = (*i)->program_value()->copy(); // this copy is deleted in ~Cell?

    while(j < n_ecoli)
    {
        Program * prog = (*i)->program_value()->copy(); // this copy is deleted in ~Cell?
        //printf ( ">> prog = %x in new_ecoli\n", prog );

        World * world = current_gro_program->get_world();

        float x = 0, y = 0, theta = 0, vol= (float) fRand(1.57, 3.14);

        /*x = (float) fRand(-max_radius, max_radius);
        y = (float) fRand(-max_radius, max_radius);*/

        x = (float) fRand((x_center-max_radius), (x_center+max_radius));
        y = (float) fRand((y_center-max_radius), (y_center+max_radius));

        //while(((x*x) + (y*y)) > (max_radius*max_radius))
        while((((x-x_center)*(x-x_center)) + ((y-y_center)*(y-y_center))) > (max_radius*max_radius))
        {
            /*x = (float) fRand(-max_radius, max_radius);
            y = (float) fRand(-max_radius, max_radius);*/
            x = (float) fRand((x_center-max_radius), (x_center+max_radius));
            y = (float) fRand((y_center-max_radius), (y_center+max_radius));
        }

        theta = (float) fRand(0.0, 6.28);
        //vol = DEFAULT_ECOLI_INIT_SIZE;
        //vol = (float) fRand(1.57, 3.14);
        vol = (float) fRand(1.57, 2.355);

        /*if ( settings->get_type() == Value::RECORD ) {

                                                                if ( settings->getField ( "x" ) )
                                                                  x = settings->getField ( "x" )->real_value();

                                                                if ( settings->getField ( "y" ) )
                                                                  y = settings->getField ( "y" )->real_value();

                                                                if ( settings->getField ( "theta" ) )
                                                                  theta = settings->getField ( "theta" )->real_value();

                                                                if ( settings->getField ( "volume" ) )
                                                                  vol = settings->getField ( "volume" )->real_value();
                                                                else
                                                                  vol = DEFAULT_ECOLI_INIT_SIZE;

                                                              } else {

                                                                fprintf ( stderr, "First argument to 'ecoli' should be a record\n" );
                                                                exit ( -1 );

                                                              }*/

        EColi * c = new EColi ( world, x, y, theta, vol );

        int k=0;

        for ( k=0; k<plasmids_Map.size(); k++ ) {
            c->setPlasmid(k, false);
        }

        for ( k=0; k<world->get_param("num_proteins"); k++ ) {
            c->set_initial_protein(k, false);
        }

        for ( k=0; k<world->get_param("num_proteins"); k++ ) {
            c->set_initial_rna(k, false);
        }

        for ( h=Plas->list_value()->begin(); h != Plas->list_value()->end(); h++ ) {
            c->setPlasmid(plasmids_Map[(*h)->string_value()], true);
        }
        /*for ( h=Plas->list_value()->begin(); h != Plas->list_value()->end(); h++ ) {
            c->setPlasmid(k, (*h)->bool_value());
            k++;
        }
        k=0;*/
        for ( h=Prot->list_value()->begin(); h != Prot->list_value()->end(); h++ ) {
            c->set_initial_protein(prMap[(*h)->string_value()], true);
        }
        /*for ( h=Prot->list_value()->begin(); h != Prot->list_value()->end(); h++ ) {
            c->set_initial_protein(k, (*h)->bool_value());
            k++;
        }
        k=0;*/
        for ( h=mrna->list_value()->begin(); h != mrna->list_value()->end(); h++ ) {
            c->set_initial_rna(prMap[(*h)->string_value()], true);
        }
        /*for ( h=mrna->list_value()->begin(); h != mrna->list_value()->end(); h++ ) {
            c->set_initial_rna(k, (*h)->bool_value());
            k++;
        }*/

        current_cell = c;
        c->set_gro_program ( prog ); // prog is deleted if/when the cell is deleted in ~Cell
        world->add_cell ( c );
        prog->init_params ( current_gro_program->get_scope() );
        prog->init ( current_gro_program->get_scope() );
        current_cell = NULL;
        j++;
    }

    return new Value(Value::UNIT);

}

Value * new_yeast ( std::list<Value *> * args, Scope * s ) {

    std::list<Value *>::iterator i = args->begin();
    Value * settings = *i; i++;

    ASSERT ( args->size() == 2 );

    Program * prog = (*i)->program_value()->copy(); // this copy is deleted in ~Cell?
    //printf ( ">> prog = %x in new_ecoli\n", prog );

    World * world = current_gro_program->get_world();

    float x = 0, y = 0, theta = 0, vol;
    int n = 0;

    if ( settings->get_type() == Value::RECORD ) {

        if ( settings->getField ( "nplasmids" ) )
            n = settings->getField ( "nplasmids" )->int_value();

        if ( settings->getField ( "x" ) )
            x = settings->getField ( "x" )->real_value();

        if ( settings->getField ( "y" ) )
            y = settings->getField ( "y" )->real_value();

        if ( settings->getField ( "theta" ) )
            theta = settings->getField ( "theta" )->real_value();

        if ( settings->getField ( "volume" ) )
            vol = settings->getField ( "volume" )->real_value();
        else
            vol = 1.0;

    } else {

        fprintf ( stderr, "First argument to 'yeast' should be a record\n" );
        exit ( -1 );

    }

    Yeast * c = new Yeast ( world, n, x, y, theta, vol, false );

    current_cell = c;
    c->set_gro_program ( prog ); // prog is deleted if/when the cell is deleted in ~Cell
    world->add_cell ( c );
    prog->init_params ( current_gro_program->get_scope() );
    prog->init ( current_gro_program->get_scope() );
    current_cell = NULL;

    return new Value(Value::UNIT);

}


Value * new_signal ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();

    std::list<Value *>::iterator i = args->begin();
    Value * kdi = *i; i++;
    Value * kde = *i;

    int w, h, numx, numy;

    w = world->get_param ( "signal_grid_width" );
    h = world->get_param ( "signal_grid_height" );
    numx = w / world->get_param ( "signal_element_size" );
    numy = h / world->get_param ( "signal_element_size" );

    Signal * sig = new Signal (
                ceGetVector2 ( -w/2, -h/2 ), ceGetVector2 ( w/2, h/2 ), numx, numy,
                kdi->num_value(), kde->num_value() );

    world->add_signal ( sig );

    return new Value ( world->num_signals() - 1 );
}

/*Value * new_cross_feeding_signal ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();

    std::list<Value *>::iterator i = args->begin();
    Value * kdi = *i; i++;
    Value * kde = *i; i++;
    Value * conc_max = *i;

    int w, h, numx, numy, id;

    w = world->get_param ( "signal_grid_width" );
    h = world->get_param ( "signal_grid_height" );
    numx = w / world->get_param ( "signal_element_size" );
    numy = h / world->get_param ( "signal_element_size" );

    Signal * sig = new Signal (
                ceGetVector2 ( -w/2, -h/2 ), ceGetVector2 ( w/2, h/2 ), numx, numy,
                kdi->num_value(), kde->num_value() );

    world->add_signal ( sig );

    id = world->num_signals() - 1;
    world->add_cross_feeding_max_emit(id,conc_max->num_value());

    return new Value ( world->num_signals() - 1 );
}*/

Value * s_new_signal ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();

    std::list<Value *>::iterator i = args->begin();
    Value * kdi = *i; i++;
    Value * kde = *i;

    world->handler->add_signal(world->s_signal_id, kdi->num_value(), kde->num_value());
    world->signal_concs[world->s_signal_id] = 0;
    world->s_signal_id++;

    return new Value ( world->s_signal_id - 1 );
}

/*Value * s_new_cross_feeding_signal ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();

    std::list<Value *>::iterator i = args->begin();
    Value * kdi = *i; i++;
    Value * kde = *i; i++;
    Value * emit_max = *i; i++;
    Value * absorb_max = *i;

    world->handler->add_signal(world->s_signal_id, kdi->num_value(), kde->num_value());
    world->signal_concs[world->s_signal_id] = 0;
    world->add_cross_feeding_max_emit(world->s_signal_id,emit_max->num_value());
    world->add_cross_feeding_max_absorb(world->s_signal_id,absorb_max->num_value());

    world->s_signal_id++;

    return new Value ( world->s_signal_id - 1);
}*/


Value * add_reaction ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();

    // args are: reactants, products, rate
    std::list<Value *>::iterator i = args->begin();
    Value * R = *i; i++;
    Value * P = *i; i++;
    Value * k = *i;

    Reaction r (k->num_value());

    for ( i=R->list_value()->begin(); i != R->list_value()->end(); i++ ) {
        if ( (*i)->int_value() < 0 || (*i)->int_value() >= world->num_signals() )
            throw std::string ( "Reaction refers to a non-existant reactant." );
        r.add_reactant( (*i)->int_value() );
    }

    for ( i=P->list_value()->begin(); i != P->list_value()->end(); i++ ) {
        if ( (*i)->int_value() < 0 || (*i)->int_value() >= world->num_signals() )
            throw std::string ( "Reaction refers to a non-existant product." );
        r.add_product( (*i)->int_value() );
    }

    world->add_reaction(r);

    return new Value ( Value::UNIT );

}

Value * set_signal ( std::list<Value *> * args, Scope * s ) {

    // n, x, y, v


    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    ASSERT ( args->size() == 4 );

    Value * n = *i; i++;
    Value * x = *i; i++;
    Value * y = *i; i++;
    Value * val = *i;

    world->set_signal ( n->int_value(), x->num_value(), y->num_value(), val->num_value() );

    return new Value ( Value::UNIT );

}

Value * set_signal_rect ( std::list<Value *> * args, Scope * s ) {

    // n, x1, y1, x2, y2, v

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    ASSERT ( args->size() == 6 );

    Value * n = *i; i++;

    Value * x1 = *i; i++;
    Value * y1 = *i; i++;
    Value * x2 = *i; i++;
    Value * y2 = *i; i++;

    Value * val = *i;

    world->set_signal_rect ( n->int_value(), x1->num_value(), y1->num_value(), x2->num_value(), y2->num_value(), val->num_value() );

    return new Value ( Value::UNIT );

}

Value * get_signal ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * n = *i;

    if ( current_cell != NULL ) {
        ASSERT ( n->int_value() < world->num_signals() );
        return new Value ( (double) world->get_signal_value ( current_cell, n->int_value() ) );
    } else {
        printf ( "Warning: Tried to get signal value from outside a cell program. No action taken\n" );
        return new Value ( 0.0 );
    }

}

Value * emit_signal ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * n = *i; i++;
    Value * ds = *i;

    if ( current_cell != NULL )
        world->emit_signal ( current_cell, n->int_value(), ds->num_value() );
    else
        printf ( "Warning: Tried to emit signal from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );

}

Value * what_proteins (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    ASSERT ( args->size() == 1);

    std::list<Value *>::iterator i = args->begin();
    Value * data = *i;
    map<std::string,int> prMap;
    bool insert_success = false;
    prMap = world->get_protein_and_rna_map();

    int k = prMap.size();
    //int k = (world->get_n_proteins() + world->get_n_rnas());
    for ( i=data->list_value()->begin(); i != data->list_value()->end(); i++ ) {
        insert_success = world->add_protein_or_rna_to_map((*i)->string_value(), k);
        if(insert_success)
        {
            k++;
        }
    }

    if(k > world->get_n_rnas())
    {
        world->set_n_proteins(k - world->get_n_rnas());
    }
    //world->print_protein_and_rna_map();
    //world->init_prot_down_arrays(world->get_n_rnas()+world->get_n_proteins());
    world->init_prot_down_arrays(world->get_param("num_proteins"));

    //cout << "\n" << (k - world->get_n_rnas()) << "\n";
    return new Value ( Value::UNIT );
}

Value * what_rnas (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    ASSERT ( args->size() == 1);

    std::list<Value *>::iterator i = args->begin();
    Value * data = *i;
    map<std::string,int> prMap;
    bool insert_success = false;
    prMap = world->get_protein_and_rna_map();

    int k = prMap.size();
    //int k = (world->get_n_proteins() + world->get_n_rnas());
    for ( i=data->list_value()->begin(); i != data->list_value()->end(); i++ ) {
        insert_success = world->add_protein_or_rna_to_map((*i)->string_value(), k);
        if(insert_success)
        {
            k++;
        }
    }

    if(k > world->get_n_proteins())
    {
        world->set_n_rnas(k - world->get_n_proteins());
    }
    world->print_protein_and_rna_map();
    world->init_rna_down_arrays(world->get_param("num_proteins"));


    //cout << "\n" << (k - world->get_n_proteins()) << "\n";
    return new Value ( Value::UNIT );
}

Value * what_plasmids (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    ASSERT ( args->size() == 1);

    std::list<Value *>::iterator i = args->begin();
    Value * data = *i;
    bool insert_success = false;
    int n_plasmids = world->get_param("num_plasmids"), j=0;

    int k = 0;

    for ( i=data->list_value()->begin(); (i != data->list_value()->end() && j < n_plasmids); i++ ) {
        insert_success = world->add_plasmid_to_map((*i)->string_value(), k);
        if(insert_success)
        {
            k++;
        }
        j++;
    }

    //world->print_plasmids_map();

    //cout << "\n" << (k - world->get_n_proteins()) << "\n";
    return new Value ( Value::UNIT );
}

Value * what_riboswitches (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    ASSERT ( args->size() == 1);

    std::list<Value *>::iterator i = args->begin();
    Value * data = *i;
    bool insert_success = false;
    int n_riboswitches = world->get_param("num_riboswitchs"), j=0;

    int k = 1;

    for ( i=data->list_value()->begin(); (i != data->list_value()->end() && j < n_riboswitches); i++ ) {
        insert_success = world->add_riboswitch_to_map((*i)->string_value(), k);
        if(insert_success)
        {
            k++;
        }
        j++;
    }

    world->print_riboswitches_map();

    //cout << "\n" << (k - world->get_n_proteins()) << "\n";
    return new Value ( Value::UNIT );
}

Value * what_molecules (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    ASSERT ( args->size() == 1);

    std::list<Value *>::iterator i = args->begin();
    Value * data = *i;
    bool insert_success = false;
    int n_molecules = world->get_param("num_molecules"), j=0;

    int k = 0;

    for ( i=data->list_value()->begin(); (i != data->list_value()->end() && j < n_molecules); i++ ) {
        insert_success = world->add_molecule_to_map((*i)->string_value(), k);
        if(insert_success)
        {
            k++;
        }
        j++;
    }

    world->print_molecules_map();

    //cout << "\n" << (k - world->get_n_proteins()) << "\n";
    return new Value ( Value::UNIT );
}

Value * what_analog_molecules (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    ASSERT ( args->size() == 1);

    std::list<Value *>::iterator i = args->begin();
    Value * data = *i;
    bool insert_success = false;
    int n_analog_molecules = world->get_param("num_analog_molecules"), j=0;

    int k = 0;

    for ( i=data->list_value()->begin(); (i != data->list_value()->end() && j < n_analog_molecules); i++ ) {
        insert_success = world->add_analog_molecule_to_map((*i)->string_value(), k);
        if(insert_success)
        {
            k++;
        }
        j++;
    }

    //world->print_analog_molecules_map();

    //cout << "\n" << (k - world->get_n_proteins()) << "\n";
    return new Value ( Value::UNIT );
}


Value * new_genes (std::list<Value *> * args, Scope * s ) {
    World * world = current_gro_program->get_world();

    ASSERT ( args->size() == 1);

    int n_tf = 0, tf_index = 0, regulation_value = 0, counter = 0, which_riboswitch = 0, protein_index = 0, rna_index = 0;
    bool constitutive = false, operon_add_ok = false;
    float i_noise[4], r_noise[4];
    std::list<Value *>::iterator i = args->begin();
    std::list<Value *>::iterator j,k,l;
    Value * data = *i;
    std::list<Value*> * proteins = NULL;
    std::list<Value*> * rnas = NULL;
    std::list<Value*> * transcription_factors = NULL;
    std::list<Value*> * riboswitches = NULL;
    std::list<Value*> * temp_riboswitch = NULL;
    std::list<Value*> * protein_times = NULL;
    std::list<Value*> * protein_variabilities = NULL;
    std::list<Value*> * rna_times = NULL;
    std::list<Value*> * rna_variabilities = NULL;
    Value * promoter = NULL;
    Value * inductor_noise = NULL;
    Value * repressor_noise = NULL;
    Value * protein_act_times = NULL;
    Value * rna_act_times = NULL;
    std::string circuit;
    std::string temp_tf;
    std::string riboswitch_location_gene;
    std::string operon_name;
    map<std::string,int> pMap;
    map<std::string,int> rMap;
    map<std::string,int> riboMap;

    Operon * op = new Operon(world);

    for(counter = 0; counter < 4; counter++)
    {
            i_noise[counter] = 0.0;
            r_noise[counter] = 0.0;
    }
    counter = 0;
    pMap.clear();
    rMap.clear();
    circuit.clear();
    temp_tf.clear();
    riboswitch_location_gene.clear();

    if ( data->get_type() == Value::RECORD )
    {
        if ( data->getField ( "proteins" ) && data->getField("proteins")->get_type() == Value::LIST)
        {
            proteins = data->getField ( "proteins" )->list_value();
        }
        if ( data->getField ( "rnas" ) && data->getField("rnas")->get_type() == Value::LIST)
        {
            rnas = data->getField ( "rnas" )->list_value();
        }
        if ( data->getField ( "name" ) && data->getField("name")->get_type() == Value::STRING)
        {
            operon_name = data->getField ( "name" )->string_value();
            op->set_operon_name(operon_name);
        }
        if ( data->getField ( "promoter" ) && data->getField("promoter")->get_type() == Value::RECORD)
        {
            promoter = data->getField ( "promoter" );
            if(promoter != NULL)
            {
                if( promoter->getField ( "function" ) && promoter->getField ( "function" )->get_type() == Value::STRING)
                {
                    circuit = promoter->getField( "function" )->string_value();
                    if(!circuit.empty())
                    {
                        if(circuit.compare("YES") == 0 || circuit.compare("yes") == 0)
                        {
                            op->add_circuit(0);
                            n_tf = 1;
                        }
                        else if(circuit.compare("AND") == 0 || circuit.compare("and") == 0)
                        {
                            op->add_circuit(1);
                            n_tf = 2;
                        }
                        else if(circuit.compare("OR") == 0 || circuit.compare("or") == 0)
                        {
                            op->add_circuit(2);
                            n_tf = 2;
                        }
                    }
                }
                if( promoter->getField( "transcription_factors" ) && promoter->getField( "transcription_factors" )->get_type() == Value::LIST)
                {
                    transcription_factors = promoter->getField( "transcription_factors" )->list_value();
                    if(transcription_factors != NULL)
                    {
                        j = transcription_factors->begin();
                        /***************************************************/
                        //cout << "Protein Map - TFs: " << endl;
                        //world->print_protein_and_rna_map();
                        /***************************************************/
                        pMap = world->get_protein_and_rna_map();
                        if(!pMap.empty())
                        {
                            switch(n_tf)
                            {
                                case 1:
                                    temp_tf = (*j)->string_value();
                                    if(temp_tf.at(0) == '-')
                                    {
                                        regulation_value = -1;
                                        temp_tf.erase(temp_tf.begin());
                                    }
                                    else
                                    {
                                        regulation_value = 1;
                                    }
                                    tf_index = pMap[temp_tf];
                                    op->add_transFac(tf_index, regulation_value);
                                    break;

                                case 2:
                                    temp_tf = (*j)->string_value();
                                    if(temp_tf.at(0) == '-')
                                    {
                                        regulation_value = -1;
                                        temp_tf.erase(temp_tf.begin());
                                    }
                                    else
                                    {
                                        regulation_value = 1;
                                    }
                                    tf_index = pMap[temp_tf];
                                    op->add_transFac(tf_index, regulation_value);
                                    j++;
                                    temp_tf = (*j)->string_value();
                                    if(temp_tf.at(0) == '-')
                                    {
                                        regulation_value = -1;
                                        temp_tf.erase(temp_tf.begin());
                                    }
                                    else
                                    {
                                        regulation_value = 1;
                                    }
                                    tf_index = pMap[temp_tf];
                                    op->add_transFac(tf_index, regulation_value);
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }
                if( promoter->getField( "constitutive" ) && promoter->getField( "constitutive" )->get_type() == Value::BOOLEAN)
                {
                    constitutive = promoter->getField( "constitutive" )->bool_value();
                    op->set_constitutive(constitutive);
                }
                if( promoter->getField( "inductor_noise_probability" ) && promoter->getField("inductor_noise_probability")->get_type() == Value::RECORD)
                {
                    inductor_noise = promoter->getField( "inductor_noise_probability" );
                    if(inductor_noise != NULL)
                    {
                        if( inductor_noise->getField( "output0input1" ) && inductor_noise->getField( "output0input1" )->get_type() == Value::REAL )
                        {
                            i_noise[1] = (float)(inductor_noise->getField( "output0input1" )->real_value());
                            if(i_noise[1] > 1.0)
                            {
                                i_noise[1] = 1.0;
                            }
                            else if(i_noise[1] < 0.0)
                            {
                                i_noise[1] = 0.0;
                            }
                            i_noise[0] = 1.0 - (i_noise[1]);
                            op->add_prob_ind_output1_input1(i_noise[0]);
                            op->add_prob_ind_output0_input1(i_noise[1]);
                        }
                        if( inductor_noise->getField( "output1input0" ) && inductor_noise->getField( "output1input0" )->get_type() == Value::REAL )
                        {
                            i_noise[2] = (float)(inductor_noise->getField( "output0input1" )->real_value());
                            if(i_noise[2] > 1.0)
                            {
                                i_noise[2] = 1.0;
                            }
                            else if(i_noise[2] < 0.0)
                            {
                                i_noise[2] = 0.0;
                            }
                            i_noise[3] = 1.0 - (i_noise[2]);
                            op->add_prob_ind_output1_input0(i_noise[2]);
                            op->add_prob_ind_output0_input0(i_noise[3]);
                        }
                    }
                }
                if( promoter->getField( "repressor_noise_probability" ) && promoter->getField("repressor_noise_probability")->get_type() == Value::RECORD)
                {
                    repressor_noise = promoter->getField( "repressor_noise_probability" );
                    if(repressor_noise != NULL)
                    {
                        if( repressor_noise->getField( "output1input1" ) && repressor_noise->getField( "output1input1" )->get_type() == Value::REAL )
                        {
                            r_noise[0] = repressor_noise->getField( "output1input1" )->real_value();
                            if(r_noise[0] > 1.0)
                            {
                                r_noise[0] = 1.0;
                            }
                            else if(r_noise[0] < 0.0)
                            {
                                r_noise[0] = 0.0;
                            }
                            r_noise[1] = 1.0 - (r_noise[0]);
                            op->add_prob_rep_output1_input1(r_noise[0]);
                            op->add_prob_rep_output0_input1(r_noise[1]);
                        }
                        if( repressor_noise->getField( "output0input0" ) && repressor_noise->getField( "output0input0" )->get_type() == Value::REAL )
                        {
                            r_noise[3] = repressor_noise->getField( "output0input0" )->real_value();
                            if(r_noise[3] > 1.0)
                            {
                                r_noise[3] = 1.0;
                            }
                            else if(r_noise[3] < 0.0)
                            {
                                r_noise[3] = 0.0;
                            }
                            r_noise[2] = 1.0 - (r_noise[3]);
                            op->add_prob_rep_output1_input0(r_noise[2]);
                            op->add_prob_rep_output0_input0(r_noise[3]);
                        }
                    }
                }
            }

        }
        if ( data->getField ( "riboswitches" ) && data->getField( "riboswitches" )->get_type() == Value::LIST)
        {
            riboswitches = data->getField ( "riboswitches" )->list_value();
            /***************************************************/
            //cout << "Protein Map - Riboswitches: " << endl;
            //world->print_protein_and_rna_map();
            /***************************************************/
            pMap = world->get_protein_and_rna_map();
            riboMap = world->get_riboswitches_map();
            if(riboswitches != NULL && !pMap.empty())
            {
                for(j=riboswitches->begin(); j!=riboswitches->end(); j++)
                {
                    temp_riboswitch = (*j)->list_value();
                    if(temp_riboswitch != NULL)
                    {
                        l=temp_riboswitch->begin();
                        //which_riboswitch = atoi(((*l)->string_value()).c_str());
                        which_riboswitch = riboMap[(*l)->string_value()];
                        l++;
                        riboswitch_location_gene = (*l)->string_value();
                        op->add_RBS(pMap[riboswitch_location_gene],which_riboswitch);
                    }
                }
            }
        }
        if ( data->getField ( "prot_act_times" ) && data->getField( "prot_act_times" )->get_type() == Value::RECORD)
        {
            protein_act_times = data->getField( "prot_act_times" );
            if( protein_act_times != NULL )
            {
                if( protein_act_times->getField( "times" ) && protein_act_times->getField( "times" )->get_type() == Value::LIST )
                {
                    protein_times = protein_act_times->getField( "times" )->list_value();
                    if(protein_times != NULL)
                    {
                        if( protein_act_times->getField( "variabilities" ) && protein_act_times->getField( "variabilities" )->get_type() == Value::LIST )
                        {
                            protein_variabilities = protein_act_times->getField( "variabilities" )->list_value();
                        }
                    }
                }
            }
            
            if(proteins != NULL)
            {
                /***************************************************/
                //cout << "Protein Map - Protein assignation: " << endl;
                //world->print_protein_and_rna_map();
                /***************************************************/
                pMap = world->get_protein_and_rna_map();
                if(protein_times != NULL)
                {
                    j = protein_times->begin();
                }
                if(protein_variabilities != NULL)
                {
                    l = protein_variabilities->begin();
                }
                for(k=proteins->begin(); k!=proteins->end(); k++)
                {
                    protein_index = pMap[(*k)->string_value()];
                    if(protein_times != NULL)
                    {
                        op->add_prot_up_time(protein_index,(*j)->real_value());
                        op->add_protein(protein_index,true);
                        j++;
                    }
                    if(protein_variabilities != NULL)
                    {
                        op->add_prot_time_error(protein_index,(*l)->real_value());
                        l++;
                    }
                }
            }
        }
        if ( data->getField ( "rna_act_times" ) && data->getField( "rna_act_times" )->get_type() == Value::RECORD)
        {
            rna_act_times = data->getField( "rna_act_times" );
            if( rna_act_times != NULL )
            {
                if( rna_act_times->getField( "times" ) && rna_act_times->getField( "times" )->get_type() == Value::LIST )
                {
                    rna_times = rna_act_times->getField( "times" )->list_value();
                    if(rna_times != NULL)
                    {
                        if( rna_act_times->getField( "variabilities" ) && rna_act_times->getField( "variabilities" )->get_type() == Value::LIST )
                        {
                            rna_variabilities = rna_act_times->getField( "variabilities" )->list_value();
                        }
                    }
                }
            }
            
            if(rnas != NULL)
            {
                rMap = world->get_protein_and_rna_map();
                if(rna_times != NULL)
                {
                    j = rna_times->begin();
                }
                if(rna_variabilities != NULL)
                {
                    l = rna_variabilities->begin();
                }
                for(k=rnas->begin(); k!=rnas->end(); k++)
                {
                    rna_index = rMap[(*k)->string_value()];
                    if(rna_times != NULL)
                    {
                        op->add_rna_up_time(rna_index,(*j)->real_value());
                        op->add_RNA(rna_index,true);
                        j++;
                    }
                    if(rna_variabilities != NULL)
                    {
                        op->add_rna_time_error(rna_index,(*l)->real_value());
                        l++;
                    }
                }
            }
        }
    }
    operon_add_ok = world->add_operon_to_map(op->get_operon_name(),op->get_operon_id());
    world->add_operon(op);

    return new Value ( Value::UNIT );
}


Value * new_prot_deg_times(std::list<Value *> * args, Scope * s){
    World * world = current_gro_program->get_world();

    ASSERT ( args->size() == 1);

    std::list<Value *>::iterator i = args->begin();
    std::list<Value *>::iterator j,l;
    Value * data = *i;
    std::list<Value*> * proteins = NULL;
    std::list<Value*> * times = NULL;
    std::list<Value*> * variabilities = NULL;
    map<std::string,int> pMap;
    int index = 0;

    //cout << "Proteins " <<"Parametro: " << (int)world->get_param("num_proteins") << ", valor interno proteinas: " << world->get_n_proteins() << ", valor interno rnas: " << world->get_n_rnas() << endl;

    if ( data->get_type() == Value::RECORD )
    {
        if ( data->getField("proteins") &&
             data->getField("proteins")->get_type() == Value::LIST)
        {
            proteins = data->getField("proteins")->list_value();
            if(data->getField("times") &&
               data->getField("times")->get_type() == Value::LIST)
            {
                times = data->getField("times")->list_value();
                if(data->getField("variabilities") &&
                   data->getField("variabilities")->get_type() == Value::LIST)
                {
                    variabilities = data->getField("variabilities")->list_value();
                }
            }
        }
    }

    if(proteins != NULL)
    {
        //world->init_prot_arrays(world->get_n_rnas()+world->get_n_proteins());
        /***************************************************/
        //cout << "Protein Map - Protein degradation times: " << endl;
        //world->print_protein_and_rna_map();
        /***************************************************/
        pMap = world->get_protein_and_rna_map();
        if(times != NULL)
        {
            j = times->begin();
        }
        if(variabilities != NULL)
        {
            l = variabilities->begin();
        }
        for(i=proteins->begin(); i != proteins->end(); i++)
        {
            index = pMap[(*i)->string_value()];
            /*cout << "Name: " << (*i)->string_value() << ", Index: " << index << "\n";
            printf("Time: %f, Var: %f\n", (*j)->real_value(), (*l)->real_value());*/
            if(times != NULL)
            {
                world->set_prot_down_time(index, (*j)->real_value());
                j++;
            }
            if(variabilities != NULL)
            {
                world->set_prot_down_time_error(index, (*l)->real_value());
                l++;
            }
        }
    }

    //cout << "Chau!!! - Proteinas" << endl;

    return new Value ( Value::UNIT );

}

Value * new_rna_deg_times(std::list<Value *> * args, Scope * s){
    World * world = current_gro_program->get_world();

    ASSERT ( args->size() == 1);

    std::list<Value *>::iterator i = args->begin();
    std::list<Value *>::iterator j,l;
    Value * data = *i;
    std::list<Value*> * rnas = NULL;
    std::list<Value*> * times = NULL;
    std::list<Value*> * variabilities = NULL;
    map<std::string,int> rMap;
    int index = 0;

    //cout << "RNA " << "Parametro: " << (int)world->get_param("num_proteins") << ", valor interno proteinas: " << world->get_n_proteins() << ", valor interno rnas: " << world->get_n_rnas() << endl;

    if ( data->get_type() == Value::RECORD )
    {
        if ( data->getField("rnas") &&
             data->getField("rnas")->get_type() == Value::LIST)
        {
            rnas = data->getField("rnas")->list_value();
            if(data->getField("times") &&
               data->getField("times")->get_type() == Value::LIST)
            {
                times = data->getField("times")->list_value();
                if(data->getField("variabilities") &&
                   data->getField("variabilities")->get_type() == Value::LIST)
                {
                    variabilities = data->getField("variabilities")->list_value();
                }
            }
        }
    }

    if(rnas != NULL)
    {
        rMap = world->get_protein_and_rna_map();
        if(times != NULL)
        {
            j = times->begin();
        }
        if(variabilities != NULL)
        {
            l = variabilities->begin();
        }
        for(i=rnas->begin(); i != rnas->end(); i++)
        {
            index = rMap[(*i)->string_value()];
            /*cout << "Name: " << (*i)->string_value() << ", Index: " << index << "\n";
            printf("Time: %f, Var: %f\n", (*j)->real_value(), (*l)->real_value());*/
            if(times != NULL)
            {
                world->set_rna_down_time(index, (*j)->real_value());
                j++;
            }
            if(variabilities != NULL)
            {
                world->set_rna_down_time_error(index, (*l)->real_value());
                l++;
            }
        }
    }

    //cout << "Chau!!! - RNAs" << endl;

    return new Value ( Value::UNIT );

}

Value * new_operon (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();

    // args are: proteins, trnasFact, up_times, circuit, prob
    std::list<Value *>::iterator i = args->begin();
    Value * P = *i; i++;
    Value * R = *i; i++;
    Value * TF = *i; i++;
    Value * RBS = *i; i++;
    Value * constit = *i;i++;
    Value * up = *i; i++;
    Value * desv = *i; i++;
    Value * upR = *i; i++;
    Value * desvR = *i; i++;
    Value * circuit = *i; i++;
    Value * probI = *i;i++;
    Value * probR = *i;


    Operon * op = new Operon(world);

    int k=0;
    for ( i=P->list_value()->begin(); i != P->list_value()->end(); i++ ) {
        op->add_protein( k,(*i)->bool_value() );
        k++;
    }

    k=0;
    for ( i=R->list_value()->begin(); i != R->list_value()->end(); i++ ) {
        op->add_RNA( k,(*i)->bool_value() );
        k++;
    }

    k=0;
    for ( i=TF->list_value()->begin(); i != TF->list_value()->end(); i++ ) {
        if ( (*i)->int_value() != 0 && (*i)->int_value() !=1 && (*i)->int_value() !=-1)
            throw std::string ( "Transcription Factor value must be {0:Not a TF, 1:Promoter, -1:Repressor}" );
        op->add_transFac(k,(*i)->int_value() );
        k++;
    }

    k=0;
    for ( i=RBS->list_value()->begin(); i != RBS->list_value()->end(); i++ ) {
        op->add_RBS( k,(*i)->int_value() );
        k++;
    }

    k=0;
    for ( i=up->list_value()->begin(); i != up->list_value()->end(); i++ ) {
        op->add_prot_up_time( k, (*i)->real_value() );
        k++;
    }

    k=0;
    for ( i=desv->list_value()->begin(); i != desv->list_value()->end(); i++ ) {
        op->add_prot_time_error(k, (*i)->real_value() );
        k++;
    }

    k=0;
    for ( i=upR->list_value()->begin(); i != upR->list_value()->end(); i++ ) {
        op->add_rna_up_time( k, (*i)->real_value() );
        k++;
    }

    k=0;
    for ( i=desvR->list_value()->begin(); i != desvR->list_value()->end(); i++ ) {
        op->add_rna_time_error(k, (*i)->real_value() );
        k++;
    }

    k=0;
    for ( i=probI->list_value()->begin(); i != probI->list_value()->end(); i++ ) {
        if(k==0)
            op->add_prob_ind_output1_input1((*i)->real_value() );
        else if(k==1)
            op->add_prob_ind_output0_input1((*i)->real_value() );
        else if(k==2)
            op->add_prob_ind_output1_input0((*i)->real_value() );
        else
            op->add_prob_ind_output0_input0((*i)->real_value() );
        k++;
    }
    k=0;
    for ( i=probR->list_value()->begin(); i != probR->list_value()->end(); i++ ) {
        if(k==0)
            op->add_prob_rep_output1_input1((*i)->real_value() );
        else if(k==1)
            op->add_prob_rep_output0_input1((*i)->real_value() );
        else if(k==2)
            op->add_prob_rep_output1_input0((*i)->real_value() );
        else
            op->add_prob_rep_output0_input0((*i)->real_value() );
        k++;
    }
    op->set_constitutive(constit->bool_value());
    op->add_circuit(circuit->int_value());

    world->add_operon(op);

    return new Value ( Value::UNIT );

}

Value * set_degradation_times(std::list<Value *> * args, Scope * s){
    World * world = current_gro_program->get_world();

    std::list<Value *>::iterator i = args->begin();
    Value * downP = *i; i++;
    Value * errorP = *i;i++;
    Value * downR = *i;i++;
    Value * errorR = *i;

    int k=0;
    for ( i=downP->list_value()->begin(); i != downP->list_value()->end(); i++ ) {
        world->set_prot_down_time(k, (*i)->real_value());
        k++;
    }

    k=0;
    for ( i=errorP->list_value()->begin(); i != errorP->list_value()->end(); i++ ) {
        world->set_prot_down_time_error(k, (*i)->real_value());
        k++;
    }

    k=0;
    for ( i=downR->list_value()->begin(); i != downR->list_value()->end(); i++ ) {
        world->set_rna_down_time(k, (*i)->real_value());
        k++;
    }

    k=0;
    for ( i=errorR->list_value()->begin(); i != errorR->list_value()->end(); i++ ) {
        world->set_rna_down_time_error(k, (*i)->real_value());
        k++;
    }

    return new Value ( Value::UNIT );
}

Value * set_plasmids_matrix (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 1);

    Value * list = *i;
    std::list<Value *>::iterator j = list->list_value()->begin();

    int num_plasmids = world->get_param("num_plasmids");
    int num_operons = world->num_operons();
    int f,c;
    world->init_plasmids_matrix();

    for(f=0;f<num_plasmids;f++){
        for (c=0;c<num_operons;c++){
            world->set_plasmids_matrix(f,c,(*j)->bool_value());
            j++;
        }
    }

    return new Value ( Value::UNIT );
}

//REVISAR!!!
Value * set_plasmids_matrix2 (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 1);

    Value * data = *i;
    std::list<Value*> * genes = NULL;
    std::list<Value *>::iterator j;

    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();
    std::map<std::string,int> operons_Map = world->get_operon_map();
    std::map<std::string,int>::iterator operon_search;

    /*int num_plasmids = world->get_param("num_plasmids");
    int num_operons = world->num_operons();*/
    int f = 0, c = 0;
    world->init_plasmids_matrix();

    if ( data->get_type() == Value::RECORD )
    {
        for(std::map<std::string,int>::iterator iter = plasmids_Map.begin(); iter != plasmids_Map.end(); ++iter)
        {
            std::string k =  iter->first;
            f = iter->second;
            if ( data->getField ( k.c_str() ) && data->getField( k.c_str() )->get_type() == Value::LIST)
            {
                genes = data->getField ( k.c_str() )->list_value();
                if(genes != NULL)
                {
                    for(j=genes->begin(); j!=genes->end(); j++)
                    {
                        operon_search = operons_Map.find((*j)->string_value());
                        if (operon_search != operons_Map.end())
                        {
                            c = operon_search->second;
                            world->set_plasmids_matrix(f,c,true);
                        }
                    }
                }
            }
        }
    }
    return new Value ( Value::UNIT );
}

Value * set_RNA_matrix (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 1);

    Value * list = *i;
    std::list<Value *>::iterator j = list->list_value()->begin();

    int num_proteins = world->get_param("num_proteins");
    int num_ribs = world->get_param("num_riboswitchs");
    int f,c;

    world->init_rna_matrix();

    for(f=0;f<num_proteins;f++){
        for (c=0;c<num_ribs;c++){
            world->set_rna_matrix(f,c,(*j)->bool_value());
            j++;
        }
    }

    return new Value ( Value::UNIT );
}

//Check!!! - PROGRAM A METHOD TO CHECK WHO OPENS WHAT RIBOSWITCH
Value * set_RNA_matrix2 (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 1);

    Value * data = *i;
    std::list<Value*> * ribos = NULL;
    std::list<Value *>::iterator j;

    // proteinas (rna) -> filas
    // riboswitches -> columnas

    std::map<std::string,int> proteins_and_rna_Map = world->get_protein_and_rna_map();
    std::map<std::string,int> riboswitches_Map = world->get_riboswitches_map();
    std::map<std::string,int>::iterator riboswitch_search;

    int f=0, c=0;
    /*int num_proteins = world->get_param("num_proteins");
    int num_ribs = world->get_param("num_riboswitchs");*/
    world->init_rna_matrix();

    /*for(f=0;f<num_proteins;f++){
        for (c=0;c<num_ribs;c++){
            world->set_rna_matrix(f,c,(*j)->bool_value());
            j++;
        }
    }*/

    if ( data->get_type() == Value::RECORD )
    {
        for(std::map<std::string,int>::iterator iter = proteins_and_rna_Map.begin(); iter != proteins_and_rna_Map.end(); ++iter)
        {
            std::string k =  iter->first;
            f = iter->second;
            if ( data->getField ( k.c_str() ) && data->getField( k.c_str() )->get_type() == Value::LIST)
            {
                ribos = data->getField ( k.c_str() )->list_value();
                if(ribos != NULL)
                {
                    for(j=ribos->begin(); j!=ribos->end(); j++)
                    {
                        //DUDA!!! -1 a los ribos? -> riboswitches deben empezar en 1

                        riboswitch_search = riboswitches_Map.find((*j)->string_value());
                        if (riboswitch_search != riboswitches_Map.end())
                        {
                            c = riboswitch_search->second;
                            world->set_rna_matrix(f,c,true);
                        }
                    }
                }
            }
        }
    }
    return new Value ( Value::UNIT );
}

Value * set_molecules_matrix (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 1);

    Value * list = *i;
    std::list<Value *>::iterator j = list->list_value()->begin();

    int num_proteins = world->get_param("num_proteins");
    int num_molecules = world->get_param("num_molecules");
    int f,c;
    world->init_molecules_matrix();

    for(f=0;f<num_molecules;f++){
        for (c=0;c<num_proteins;c++){
            world->set_molecules_matrix(f,c,(*j)->int_value());
            j++;
        }
    }

    return new Value ( Value::UNIT );
}

//Check!!
Value * set_molecules_matrix2 (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 1);

    // moleculas -> filas
    // proteinas (rna) -> columnas

    Value * data = *i;
    std::list<Value*> * prots = NULL;
    std::list<Value *>::iterator j;

    std::map<std::string,int> molecules_Map = world->get_molecules_map();
    std::map<std::string,int> proteins_Map = world->get_protein_and_rna_map();
    std::map<std::string,int>::iterator protein_search;

    int f=0, c=0;
    int regulation_value = 0;
    std::string regulated;
    world->init_molecules_matrix();
    regulated.clear();

    if ( data->get_type() == Value::RECORD )
    {
        for(std::map<std::string,int>::iterator iter = molecules_Map.begin(); iter != molecules_Map.end(); ++iter)
        {
            std::string k =  iter->first;
            f = iter->second;
            if ( data->getField ( k.c_str() ) && data->getField( k.c_str() )->get_type() == Value::LIST)
            {
                prots = data->getField ( k.c_str() )->list_value();
                if(prots != NULL)
                {
                    for(j=prots->begin(); j!=prots->end(); j++)
                    {
                        regulated = (*j)->string_value();
                        if(regulated.at(0) == '-')
                        {
                            regulation_value = -1;
                            regulated.erase(regulated.begin());
                        }
                        else
                        {
                            regulation_value = 1;
                        }
                        protein_search = proteins_Map.find(regulated);
                        if (protein_search != proteins_Map.end())
                        {
                            c = protein_search->second;
                            world->set_molecules_matrix(f,c,regulation_value);
                        }
                    }
                }
            }
        }
    }
    return new Value ( Value::UNIT );
}

Value * set_analog_molecules_matrix (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 1);

    Value * list = *i;
    std::list<Value *>::iterator j = list->list_value()->begin();

    int num_proteins = world->get_param("num_proteins");
    int num_analog_molecules = world->get_param("num_analog_molecules");
    int f=0, c=0;
    world->init_analog_molecules_matrix();

    for(f=0;f<num_analog_molecules;f++){
        for (c=0;c<num_proteins;c++){
            world->set_analog_molecules_matrix(f,c,(*j)->int_value());
            j++;
        }
    }

    return new Value ( Value::UNIT );
}

Value * set_analog_molecules_matrix2 (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 1);

    // moleculas -> filas
    // proteinas (rna) -> columnas

    Value * data = *i;
    std::list<Value*> * prots = NULL;
    std::list<Value *>::iterator j;

    std::map<std::string,int> analog_molecules_Map = world->get_analog_molecules_map();
    std::map<std::string,int> proteins_Map = world->get_protein_and_rna_map();
    std::map<std::string,int>::iterator protein_search;

    int f=0, c=0;
    int regulation_value = 0;
    std::string regulated;
    world->init_analog_molecules_matrix();
    regulated.clear();

    if ( data->get_type() == Value::RECORD )
    {
        for(std::map<std::string,int>::iterator iter = analog_molecules_Map.begin(); iter != analog_molecules_Map.end(); ++iter)
        {
            std::string k =  iter->first;
            f = iter->second;
            if ( data->getField ( k.c_str() ) && data->getField( k.c_str() )->get_type() == Value::LIST)
            {
                prots = data->getField ( k.c_str() )->list_value();
                if(prots != NULL)
                {
                    for(j=prots->begin(); j!=prots->end(); j++)
                    {
                        regulated = (*j)->string_value();
                        if(regulated.at(0) == '-')
                        {
                            regulation_value = -1;
                            regulated.erase(regulated.begin());
                        }
                        else
                        {
                            regulation_value = 1;
                        }
                        protein_search = proteins_Map.find(regulated);
                        if (protein_search != proteins_Map.end())
                        {
                            c = protein_search->second;
                            world->set_analog_molecules_matrix(f,c,regulation_value);
                        }
                    }
                }
            }
        }
    }
    return new Value ( Value::UNIT );
}

//REVISAR!!
Value * set_action (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 3 || args->size() == 2);

    Value * prot_list = *i;i++;
    Value * func_name = *i;i++;
    Value * param_list;
    if(args->size() == 3)
    {
        param_list = *i;
    }

    std::vector<int> prot;
    std::map<std::string,int> protein_Map = world->get_protein_and_rna_map();

    std::list<std::string> params;

    int n_proteins = (int)world->get_param("num_proteins"), j=0, prot_value=0, prot_index=0;
    std::string protein_name;
    std::string zero("0");

    protein_name.clear();
    for(j=0; j<n_proteins; j++)
    {
        prot.push_back(0);
    }

    for ( i=prot_list->list_value()->begin(); i != prot_list->list_value()->end(); i++ )
    {
        protein_name = (*i)->string_value();
        //prot.push_back((*i)->int_value());

        if(protein_name.at(0) == '-')
        {
            prot_value = -1;
            protein_name.erase(protein_name.begin());
        }
        else
        {
            prot_value = 1;
        }
        prot_index = protein_Map[protein_name];
        prot[prot_index] = prot_value;
    }

    world->add_action_prot(prot);
    world->add_action_names(func_name->string_value());

    /*if(args->size() == 4)
    {

        for ( i=param_list->list_value()->begin(); i != param_list->list_value()->end(); i++ ) {
            params.push_back((*i)->string_value());
        }
    }
    else*/ if(args->size() == 3)
    {
        for ( i=param_list->list_value()->begin(); i != param_list->list_value()->end(); i++ ) {
            params.push_back((*i)->string_value());
        }
    }
    else
    {
        params.push_back(zero);
    }
    world->add_action_param(params);
    world->set_num_actions(world->get_num_actions()+1);
    return new Value ( Value::UNIT );
}

Value * reset_actions (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    world->remove_all_actions();
    world->clear_operon_map();
    world->clear_protein_and_rna_map();
    world->clear_plasmids_map();
    world->clear_riboswitches_map();
    world->clear_molecules_map();
    world->clear_analog_molecules_map();
    world->set_num_actions(0);
    return new Value ( Value::UNIT );
}

Value * set_molecule (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 2);

    Value * id = *i;i++;
    Value * val = *i;

    int molecule_id = ((id->int_value())-1);
    int molecule_val = val->int_value();

    if(molecule_id >= 0 && molecule_val >= 0)
    {
        world->set_molecule(molecule_id,molecule_val);
    }

    return new Value ( Value::UNIT );
}

Value * set_analog_molecule (std::list<Value *> * args, Scope * s ) {

    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 2);

    Value * id = *i;i++;
    Value * val = *i;

    int molecule_id = ((id->int_value())-1);
    int molecule_val = val->int_value();

    if ( current_cell != NULL )
        current_cell->set_analog_molecule(molecule_id,molecule_val);
    else
        printf ( "Warning: Tried to set analog signal value from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );

}

Value * get_analog_molecule (std::list<Value *> * args, Scope * s ) {

    std::list<Value *>::iterator i = args->begin();
    bool hasIt = false;
    ASSERT(args->size() == 1);

    Value * val = *i;
    int n_molecule = (val->int_value()-1);

    if ( current_cell != NULL && n_molecule >= 0)
        //hasIt = current_cell->get_analog_molecule(val->int_value()-1);
        hasIt = current_cell->get_analog_molecule(n_molecule);
    else
        printf ( "Warning: Tried to read analog signal sensing value from outside a cell program. No action taken\n" );

    return new Value ( hasIt );

}

Value * has_protein (std::list<Value *> * args, Scope * s ) {

    std::list<Value *>::iterator i = args->begin();

    ASSERT(args->size() == 1);

    int n_protein = -1;
    bool hasIt = false;
    n_protein = ((*i)->int_value()-1);

    if ( current_cell != NULL && n_protein >= 0)
        hasIt = current_cell->get_internal_protein(n_protein);
    else
        printf ( "Warning: Tried to check for a plasmid from outside a cell program. No action taken\n" );

    return new Value ( hasIt );
}

Value * has_rna (std::list<Value *> * args, Scope * s ) {

    std::list<Value *>::iterator i = args->begin();

    ASSERT(args->size() == 1);

    int n_rna = -1;
    bool hasIt = false;
    n_rna = ((*i)->int_value()-1);

    if ( current_cell != NULL && n_rna >= 0 )
        hasIt = current_cell->get_internal_RNA(n_rna);
    else
        printf ( "Warning: Tried to check for a plasmid from outside a cell program. No action taken\n" );

    return new Value ( hasIt );
}


/*Value * conjugate (std::list<Value *> * args, Scope * s ) {

    std::list<Value *>::iterator i = args->begin();

    ASSERT(args->size() == 2);

    int plasmidNo = -1;
    int mode = 0;
    plasmidNo = ((*i)->int_value())-1;
    i++;
    mode = ((*i)->int_value());

    if ( current_cell != NULL && plasmidNo >= 0 )
    {
        if(current_cell->hasPlasmid(plasmidNo))
        {
            current_cell->markForConjugation(plasmidNo, mode);
        }
    }
    else
        printf ( "Warning: Tried to conjugate from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );
}*/

Value * conjugate (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 1);

    //Value * data = *i;

    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();

    ASSERT(args->size() == 1);
    int plasmidNo = plasmids_Map[(*i)->string_value()];

    if ( current_cell != NULL && plasmidNo >= 0 )
    {
        if(current_cell->hasPlasmid(plasmidNo))
        {
            current_cell->transfer_plasmid(plasmidNo);
        }
    }
    else
        printf ( "Warning: Tried to conjugate from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );
}


Value * has_plasmid (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    ASSERT(args->size() == 1);

    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();
    int plasmidNo = plasmids_Map[(*i)->string_value()];

    bool hasIt = false;

    if ( current_cell != NULL && plasmidNo >= 0 )
        hasIt = current_cell->hasPlasmid(plasmidNo);
    else
        printf ( "Warning: Tried to check for a plasmid from outside a cell program. No action taken\n" );

    return new Value ( hasIt );
}

Value * set_plasmid (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();

    ASSERT(args->size() == 2);

    int plasmidNo = plasmids_Map[(*i)->string_value()];
    bool value;
    i++;
    value = (*i)->bool_value();

    if ( current_cell != NULL && plasmidNo >= 0)
        current_cell->setPlasmid(plasmidNo, value);
    else
        printf ( "Warning: Tried to set a value for plasmids from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );
}

Value * has_eex (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();

    ASSERT(args->size() == 1);

    int plasmidNo = plasmids_Map[(*i)->string_value()];
    bool hasIt = false;

    if ( current_cell != NULL && plasmidNo >= 0 )
        hasIt = current_cell->hasEEX(plasmidNo);
    else
        printf ( "Warning: Tried to check for a plasmid from outside a cell program. No action taken\n" );

    return new Value ( hasIt );
}

Value * set_eex (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();

    ASSERT(args->size() == 2);

    int plasmidNo = plasmids_Map[(*i)->string_value()];
    bool value;
    i++;
    value = (*i)->bool_value();

    if ( current_cell != NULL && plasmidNo >= 0)
        current_cell->setEEX(plasmidNo, value);
    else
        printf ( "Warning: Tried to set a value for plasmids from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );
}


Value * received_plasmid (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();

    ASSERT(args->size() == 1);

    int plasmidNo = plasmids_Map[(*i)->string_value()];
    bool receivedIt = false;

    if ( current_cell != NULL && plasmidNo >= 0)
        receivedIt = current_cell->receivedPlasmid(plasmidNo);
    else
        printf ( "Warning: Tried to check for a plasmid from outside a cell program. No action taken\n" );

    return new Value ( receivedIt );
}

Value * was_just_conjugated (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();

    ASSERT(args->size() == 1);

    int plasmidNo = plasmids_Map[(*i)->string_value()];
    bool jc = false;

    if ( current_cell != NULL && plasmidNo >= 0)
        jc = current_cell->was_just_conjugated(plasmidNo);
    else
        printf ( "Warning: Tried to check for a plasmid from outside a cell program. No action taken\n" );

    return new Value ( jc );
}

Value * acquire_plasmid (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();

    ASSERT(args->size() == 1);

    int plasmidNo = plasmids_Map[(*i)->string_value()];

    if ( current_cell != NULL && plasmidNo >= 0 )
        current_cell->acquirePlasmid(plasmidNo);
    else
        printf ( "Warning: Tried to acquire a plasmid from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );
}

Value * lose_plasmid (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();

    ASSERT(args->size() == 1);

    int plasmidNo = plasmids_Map[(*i)->string_value()];

    if ( current_cell != NULL && plasmidNo >= 0 )
        current_cell->losePlasmid(plasmidNo);
    else
        printf ( "Warning: Tried to lose a plasmid from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );
}


Value * emit_signal_conj ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * n = *i; i++;
    Value * ds = *i;

    if ( current_cell != NULL )
        world->emit_signal_conj ( current_cell, n->int_value(), ds->num_value() );
    else
        printf ( "Warning: Tried to emit signal from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );

}

Value * emit_signal_nh ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * n = *i; i++;
    Value * ds = *i;

    if ( current_cell != NULL )
        world->emit_signal_nh ( current_cell, n->int_value(), ds->num_value() );
    else
        printf ( "Warning: Tried to emit signal from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );

}

Value * absorb_signal ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * n = *i; i++;
    Value * ds = *i;

    if ( current_cell != NULL )
        world->absorb_signal ( current_cell, n->int_value(), ds->num_value() );
    else
        printf ( "Warning: Tried to absorb signal from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );

}

//Cellsignals instructions

Value * s_absorb_signal_area ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * signal_id = *i; i++; //int
    Value * conc = *i; //double

    double x = current_cell->get_x();
    double y = current_cell->get_y();
    double x_length = current_cell->get_vec_x();
    double y_length = current_cell->get_vec_y();

    double coords[4];
    coords[0] = x;
    coords[1] = y;
    coords[2] = x_length;
    coords[3] = y_length;

    //printf("Coords_absorb: x = %f, y = %f, len_x = %f, len_y = %f\n", x, y, x_length, y_length);
    world->handler->absorb_signal_random(signal_id->int_value(), conc->real_value(), coords);

    return new Value ( Value::UNIT );
}

Value * s_absorb_signal ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * signal_id = *i; i++; //int
    Value * conc = *i; //double

    double x = current_cell->get_x();
    double y = current_cell->get_y();
    double x_length = 0;
    double y_length = 0;

    double coords[4];
    coords[0] = x;
    coords[1] = y;
    coords[2] = x_length;
    coords[3] = y_length;

    //printf("Coords_absorb: x = %f, y = %f, len_x = %f, len_y = %f\n", x, y, x_length, y_length);
    world->handler->absorb_signal_random(signal_id->int_value(), conc->real_value(), coords);

    return new Value ( Value::UNIT );
}

Value * s_emit_signal_area ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * signal_id = *i; i++; //int
    Value * conc = *i; //double

    double x = current_cell->get_x();
    double y = current_cell->get_y();
    double x_length = current_cell->get_vec_x();
    double y_length = current_cell->get_vec_y();

    double coords[4];
    coords[0] = x;
    coords[1] = y;
    coords[2] = x_length;
    coords[3] = y_length;

    //printf("Coords_absorb: x = %f, y = %f, len_x = %f, len_y = %f\n", x, y, x_length, y_length);
    world->handler->emit_signal_average(signal_id->int_value(), conc->real_value(), coords);

    return new Value ( Value::UNIT );
}

Value * s_emit_signal ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * signal_id = *i; i++; //int
    Value * conc = *i; //double

    double x = current_cell->get_x();
    double y = current_cell->get_y();
    double x_length = 0;
    double y_length = 0;

    double coords[4];
    coords[0] = x;
    coords[1] = y;
    coords[2] = x_length;
    coords[3] = y_length;

    //printf("Coords_absorb: x = %f, y = %f, len_x = %f, len_y = %f\n", x, y, x_length, y_length);
    world->handler->emit_signal(signal_id->int_value(), conc->real_value(), coords);

    return new Value ( Value::UNIT );
}

Value * s_get_signal_area ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * signal_id = *i; //int

    double x = current_cell->get_x();
    double y = current_cell->get_y();
    double x_length = current_cell->get_vec_x();
    double y_length = current_cell->get_vec_y();

    double coords[4];
    coords[0] = x;
    coords[1] = y;
    coords[2] = x_length;
    coords[3] = y_length;

    double sig_val;
    sig_val =  world->handler->getSignalValueRange(signal_id->int_value(),coords);
    //printf("Get value: %f\n", sig_val);

    if(sig_val != -1)
    {
        world->signal_concs[signal_id->int_value()] = sig_val;
    }

    return new Value ( sig_val );
}

Value * s_get_signal ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * signal_id = *i; i++; //int

    double x = current_cell->get_x();
    double y = current_cell->get_y();
    double x_length = 0;
    double y_length = 0;

    double coords[4];
    coords[0] = x;
    coords[1] = y;
    coords[2] = x_length;
    coords[3] = y_length;

    double sig_val;
    sig_val =  world->handler->getSignalValue(signal_id->int_value(),coords);
    //printf("Get value: %f\n", sig_val);

    if(sig_val != -1)
    {
        world->signal_concs[signal_id->int_value()] = sig_val;
    }

    return new Value ( sig_val );
}

Value * s_set_signal_rect ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * signal_id = *i; i++;
    Value * conc = *i; i++;
    Value * x = *i; i++;
    Value * y = *i; i++;
    Value * x_length = *i; i++;
    Value * y_length = *i;

    double coords[4];
    coords[0] = x->real_value();
    coords[1] = y->real_value();
    coords[2] = x_length->real_value();
    coords[3] = y_length->real_value();

    world->handler->setSignalValue(signal_id->int_value(), conc->real_value(), coords);

    return new Value ( Value::UNIT );
}

Value * s_set_signal ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * signal_id = *i; i++;
    Value * conc = *i; i++;
    Value * x = *i; i++;
    Value * y = *i;
    double x_length = 0.0;
    double y_length = 0.0;

    double coords[4];
    coords[0] = x->real_value();
    coords[1] = y->real_value();
    coords[2] = x_length;
    coords[3] = y_length;

    world->handler->setSignalValue(signal_id->int_value(), conc->real_value(), coords);

    return new Value ( Value::UNIT );
}

Value * local_MOI ( std::list<Value *> * args, Scope * s )
{
    std::list<Value *>::iterator i = args->begin();

    Value * id = *i; i++;
    Value * dist = *i;
    float n_bact = 0, sig_conc = 0, local_moi = 0;

    n_bact = current_cell->get_n_cells_d((float)dist->real_value());
    sig_conc = current_cell->area_concentration(id->int_value(),(float)dist->real_value());
    if(n_bact == 0 || sig_conc == -1)
    {
        local_moi = 0;
    }
    else
    {
        local_moi = sig_conc/n_bact;
    }
    //printf("Neighbor bacteria: %d, signal concentration: %f, LOCAL MOI: %f\n", (int)n_bact, sig_conc, local_moi);

    return new Value(local_moi);

}

Value * delta_vol ( std::list<Value *> * args, Scope * s )
{
    return new Value ( current_cell->get_d_vol() );
}

Value * my_available ( std::list<Value *> * args, Scope * s )
{
    return new Value ( current_cell->get_available());
}

Value * die ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    if ( current_cell != NULL )
        current_cell->mark_for_death();
    else
        printf ( "Warning: Called die() from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );

}

Value * die_c ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    ASSERT(args->size() == 1);

    float radius = 0;
    radius = (*i)->real_value();


    if (current_cell != NULL)
    {
        float x = current_cell->get_x();
        float y = current_cell->get_y();
        if((x*x + y*y) >= radius*radius)
        {
            current_cell->mark_for_death();
        }
    }
    else
        printf ( "Warning: Called die_c() from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );

}

Value * reset ( std::list<Value *> * args, Scope * s ) {

    current_gro_program->get_world()->restart();
    return new Value ( Value::UNIT );

}

Value * replate ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    ASSERT(args->size() == 1);

    float perc = (*i)->real_value();

    world->replating(perc);

    return new Value ( Value::UNIT );

}

Value * select_random_cell ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    world->select_random_cell();
    return new Value ( Value::UNIT );

}

Value * stop ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    world->set_stop_flag(true);
    return new Value ( Value::UNIT );

}

Value * start ( std::list<Value *> * args, Scope * s ) {

    //run_simulation ( true );
    return new Value ( Value::UNIT );

}

Value * set_param ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * name = *i; i++;
    Value * val = *i;

    if ( current_cell == NULL ) { // This is a global parameter //////////////////////////////////

        if ( ( name->string_value() != "signal_grid_width"
               && name->string_value() != "signal_grid_height"
               && name->string_value() != "signal_element_size" ) || world->num_signals() == 0 ) {

            world->set_param ( name->string_value(), val->num_value() );

        }

    } else { /////////////////////// This is a cell-specific parameter ///////////////////////////

        current_cell->set_param ( name->string_value(), val->num_value() );
        current_cell->compute_parameter_derivatives();

    }
    //if ( name->string_value() == "throttle" ) set_throttle ( val->num_value() != 0.0 );

    return new Value ( Value::UNIT );

}

Value * world_stats ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * name = *i;

    if ( name->string_value() == "pop_size" ) {

        return new Value ( world->get_pop_size() );

    } else printf ( "unknown statistic %s in call to 'stat'\n", name->string_value().c_str() );

    return new Value ( 0 );

}

Value * message ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * n = *i;
    i++;
    Value * mes = *i;

    world->message ( n->int_value(), mes->string_value() );

    return new Value ( Value::UNIT );

}

Value * clear_messages ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * n = *i;

    world->clear_messages ( n->int_value() );

    return new Value ( Value::UNIT );

}

Value * create_dirs ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    ASSERT(args->size() == 2);

    Value * path = *i;
    i++;
    Value * n = *i;
    world->create_dirs(path->string_value().c_str(), n->int_value());

    return new Value ( Value::UNIT );
}

Value * snapshot ( std::list<Value *> * args, Scope * s ) {

    if ( true /* used to check for whether the gui was instantiated */ ) {
        World * world = current_gro_program->get_world();
        std::list<Value *>::iterator i = args->begin();
        Value * name = *i;
        world->snapshot ( name->string_value().c_str() );
    }

    return new Value ( 0 );

}

Value * force_divide ( std::list<Value *> * args, Scope * s ) {

    if ( current_cell != NULL ) {

        current_cell->force_divide();

    } else fprintf ( stderr, "Warning: divide() called from outside a cell\n" );

    return new Value ( Value::UNIT );

}

Value * chemostat ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    Value * val = *i;

    world->set_chemostat_mode(val->bool_value());

    return new Value ( Value::UNIT );

}

Value * World::map_to_cells ( Expr * e ) {

    std::list<Value *> * L = new std::list<Value *>;
    std::list<Cell *>::iterator j;

    for ( j=population->begin(); j!=population->end(); j++ ) {
        L->push_back ( (*j)->eval ( e ) );
    }

    return new Value ( L );

}

Value * map_to_cells (  std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();

    std::list<Value *>::iterator i = args->begin();
    Expr * expression = (*i)->func_body();

    while ( expression->get_type() == Expr::CONSTANT ) {
        expression = expression->get_val()->func_body();
    }

    return world->map_to_cells ( expression );

}


// ESTO NO ESTA HECHO!!!
Value * run ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    float dvel = (*i)->num_value();

    if ( current_cell != NULL ) {

        float a = current_cell->get_theta();
        ceBody * body = current_cell->get_body();
        /*cpVect v = cpBodyGetVel ( body );
                                                            cpFloat adot = cpBodyGetAngVel ( body );

                                                            cpBodySetTorque ( body, -adot ); // damp angular rotation

                                                            cpBodyApplyForce ( // apply force
                                                              current_cell->get_shape()->body,
                                                              cpv (
                                                                ( dvel*cos(a) - v.x ) * world->get_sim_dt(),
                                                                ( dvel*sin(a) - v.y ) * world->get_sim_dt()
                                                              ),
                                                              cpv ( 0, 0 ) );*/

    } else

        printf ( "Warning: Tried to emit signal from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );

}

// ESTO NO FUNCIONA!!!
Value * tumble ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    float vel = (*i)->num_value();

    if ( current_cell != NULL ) {

        float a = current_cell->get_theta();
        ceBody * body = current_cell->get_body();
        /*cpVect v = cpBodyGetVel ( body );
                                                            cpFloat adot = cpBodyGetAngVel ( body );

                                                            cpBodySetTorque ( body, vel - adot ); // apply torque

                                                            cpBodyApplyForce ( // damp translation
                                                              current_cell->get_shape()->body,
                                                              cpv (
                                                                - v.x * world->get_sim_dt(),
                                                                - v.y * world->get_sim_dt()
                                                              ),
                                                              cpv ( 0, 0 ) );*/

    } else

        printf ( "Warning: Tried to emit signal from outside a cell program. No action taken\n" );

    return new Value ( Value::UNIT );

}

Value * zoom ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();

    float z = (*i)->num_value();

    if ( z > 0 ) {

        world->set_zoom ( z );

    }

    return new Value ( Value::UNIT );

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Gro Class Methods
//
//

gro_Program::gro_Program ( const char * path, int ac, char ** av ) : MicroProgram(), pathname(path), scope(NULL), argc(ac), argv(av) {

    current_gro_program = this;

}

void gro_Program::add_method ( Value::TYPE t, int num_args, const char * name, EXTERNAL_CCLI_FUNCTION f ) {

    ASSERT ( scope != NULL );

    int i;
    TypeExpr * R = new TypeExpr ( t );
    std::list<TypeExpr *> * Ta = new std::list<TypeExpr *>;
    for ( i=0; i<num_args; i++ )
        Ta->push_back ( new TypeExpr ( true ) );
    scope->add ( name, new Value ( f, R, Ta ) ); // Ta and its elements should be deleted when scope is deleted

}

bool init_ccl = false;

void gro_Program::init ( World * w ) {

    world = w;

    scope = new Scope; // deleted in ::destroy

    scope->push ( new SymbolTable ( 100 ) );

    // Initialize Globals : this is in the top scope.
    scope->add ( "dt", new Value ( world->get_sim_dt() ) );
    scope->add ( "just_divided", new Value ( false ) );
    scope->add ( "daughter", new Value ( false ) );
    scope->add ( "selected", new Value ( false ) );
    scope->add ( "volume", new Value ( 0.0000000001 ) );
    scope->add ( "id", new Value ( 0 ) ); // these values should be deleted when scope is deleted

    // command line arguments
    std::list<Value *> * args = new std::list<Value *>;

    int i;
    for ( i=0; i<argc; i++ ) {
        args->push_back ( new Value ( argv[i] ) );
    }

    scope->add ( "ARGC", new Value ( (int) args->size() ) );
    scope->add ( "ARGV", new Value ( args ) );

    parse_error = false;

    current_cell = NULL;

    int parse_ok;

    try {

        parse_ok = readOrganismProgram ( scope, pathname.c_str() );

    }

    catch ( std::string err ) {

        parse_error = true;
        throw err;

    }

    if ( parse_ok < 0 ) {
        parse_error = true;
        throw std::string ( "un-ported parse error" );
    }

    world_update_program = scope->get_program ( "main" );

    if ( world_update_program ) {
        world_update_program->init_params ( scope );
        world_update_program->init ( scope );
    }

}

Value * gro_Program::eval ( World * world, Cell * cell, Expr * e ) {

    current_cell = cell;

    Program * gro = cell->get_gro_program();

    ASSERT ( gro != NULL );

    SymbolTable * symtab = gro->get_symtab();

    if ( cell->just_divided() ) {

        scope->assign ( "just_divided", new Value ( true ) );
        cell->set_division_indicator ( false );

        if ( cell->is_daughter() ) {
            scope->assign ( "daughter", new Value ( true ) );
            cell->set_daughter_indicator ( false );
        } else {
            scope->assign ( "daughter", new Value ( false ) );
        }

    } else {

        scope->assign ( "just_divided", new Value ( false ) );
        scope->assign ( "daughter", new Value ( false ) );

    }

    cell->jc_reset();

    scope->assign ( "dt", new Value ( world->get_sim_dt() ) );
    scope->assign ( "volume", new Value ( cell->get_volume() ) );
    scope->assign ( "selected", new Value ( cell->is_selected() ) );
    scope->assign ( "id", new Value ( cell->get_id() ) );

    scope->push ( gro->get_symtab() );
    Value * v = e->eval ( scope );
    scope->pop();

    return v;

}

void gro_Program::update ( World * world, Cell * cell ) {

    if ( !parse_error ) {

        current_cell = cell;

        Program * gro = cell->get_gro_program();

        ASSERT ( gro != NULL );

        SymbolTable * symtab = gro->get_symtab();

        if ( cell->just_divided() ) {

            scope->assign ( "just_divided", new Value ( true ) );
            cell->set_division_indicator ( false );

            if ( cell->is_daughter() ) {
                scope->assign ( "daughter", new Value ( true ) );
                cell->set_daughter_indicator ( false );
            } else {
                scope->assign ( "daughter", new Value ( false ) );
            }

        } else {

            scope->assign ( "just_divided", new Value ( false ) );
            scope->assign ( "daughter", new Value ( false ) );

        }

        cell->jc_reset();

        scope->assign ( "dt", new Value ( world->get_sim_dt() ) );
        scope->assign ( "volume", new Value ( cell->get_volume() ) );
        scope->assign ( "selected", new Value ( cell->is_selected() ) );
        scope->assign ( "id", new Value ( cell->get_id() ) );

        gro->step(scope);

        Value * v = symtab->get ( "gfp" );
        if ( v != NULL )
            cell->set_rep ( GFP, v->real_value() );

        v = symtab->get ( "rfp" );
        if ( v != NULL )
            cell->set_rep ( RFP, v->real_value() );

        v = symtab->get ( "yfp" );
        if ( v != NULL )
            cell->set_rep ( YFP, v->real_value() );

        v = symtab->get ( "cfp" );
        if ( v != NULL )
            cell->set_rep ( CFP, v->real_value() );

        current_cell = NULL;

    }

}

void gro_Program::world_update ( World * world ) {

    if ( !parse_error ) {

        if ( world_update_program )
            world_update_program->step ( scope );

    }

}

void gro_Program::destroy ( World * world ) {

    if ( scope != NULL ) {
        delete scope; // this would be the case if this isn't the first time init was called
    }

}

std::string gro_Program::name ( void ) const {

    return pathname;

}

Value * set_theme ( std::list<Value *> * args, Scope * s ) {

#ifndef NOGUI

    ASSERT ( args->size() == 1 );

    std::list<Value *>::iterator i = args->begin();
    current_gro_program->get_world()->set_theme ( *i );

#endif

    return new Value ( Value::UNIT );

}

Value * gro_print ( std::list<Value *> * args, Scope * s ) {

    std::list<Value *>::iterator i;
    std::stringstream strm;

    for ( i=args->begin(); i!=args->end(); i++ ) {
        if ( (*i)->get_type() == Value::STRING )
            strm << (*i)->string_value();
        else
            strm << (*i)->tostring();
    }

#ifndef NOGUI
    current_gro_program->get_world()->emit_message(strm.str());
#else
    std::cout << strm.str();
#endif

    return new Value(Value::UNIT);

}

Value * gro_clear ( std::list<Value *> * args, Scope * s ) {

    std::string str = "";
    current_gro_program->get_world()->emit_message(str,true);
    return new Value(Value::UNIT);

}

Value * gro_fopen ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    Value * name = *i; i++;
    Value * mod = *i;

    FILE * f = fopen ( name->string_value().c_str(), mod->string_value().c_str() );

    if ( f ) {

        world->fileio_list.push_back(f);
        return new Value ( (int) world->fileio_list.size() - 1 );
        //return new Value(f);

    } else {

        return new Value(-1);

    }
}

/*Value * gro_fclose ( std::list<Value *> * args, Scope * s ) {

                                                            World * world = current_gro_program->get_world();
                                                            std::list<Value *>::iterator i = args->begin();
                                                            Value * fp = *i;


                                                            // Remember to check how to do an erase/delete from a vector (fileio_list) BY VALUE, not by position.
                                                            // If you find nothing, just try a cycle.
                                                            fclose ( fp );

                                                            if ( f ) {

                                                                world->fileio_list.push_back(f);
                                                                return new Value ( (int) world->fileio_list.size() - 1 );

                                                            } else {

                                                                return new Value(-1);

                                                            }
                                                        }*/

Value * gro_fprint ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    int index = (*i)->int_value(); i++;

    if ( 0 <= index && index < world->fileio_list.size() ) {

        std::stringstream strm;

        for ( ; i!=args->end(); i++ ) {
            if ( (*i)->get_type() == Value::STRING )
                strm << (*i)->string_value();
            else
                strm << (*i)->tostring();
        }

        fprintf ( world->fileio_list[index], strm.str().c_str() );
        fflush ( world->fileio_list[index] );
    }

    return new Value ( Value::UNIT );

}

Value * gro_dump ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    int index = (*i)->int_value(); i++;

    if ( 0 <= index && index < world->fileio_list.size() ) {

        world->dump(world->fileio_list[index]);
        fflush ( world->fileio_list[index] );

    }

    return new Value ( Value::UNIT );

}

Value * gro_dump_single (std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    std::list<Value *>::iterator i = args->begin();
    int index = (*i)->int_value(); i++;
    int j = 0;

    if(!world->get_output_started())
    {
        fprintf(world->fileio_list[index], "time, id, x, y, theta, volume, gt_inst, gfp, rfp, yfp, cfp, ");
        for(j = 0; j < (current_cell->get_num_proteins()-1); j++)
        {
            fprintf(world->fileio_list[index], "Protein %d, ", j);
        }
        fprintf(world->fileio_list[index], "Protein %d\n", j);
        world->set_output_started(true);
    }

    if ( 0 <= index && index < world->fileio_list.size() ) {

        current_cell->state_to_file(world->fileio_list[index]);
        fflush ( world->fileio_list[index] );

    }

    return new Value ( Value::UNIT );
}

Value * gro_dump_multiple(std::list<Value *> * args, Scope * s){

    World * world = current_gro_program->get_world();
    int n_cond = 0, j = 0, k = 0;
    int *counts, *int_cond;

    std::list<Value *>::iterator i = args->begin();
    std::list<Value *>::iterator cds;
    std::list<Value *>::iterator l;
    std::map<std::string,int> protein_Map = world->get_protein_and_rna_map();
    std::string protein_name;
    int prot_value = 0, prot_index = 0;
    int index = (*i)->int_value();
    i++;
    cds = i;


    Value *cond;
    //std::list<Value *>::iterator i = args->begin();
    //std::list<Value *>* cond = (*i)->list_value();

    for (i=cds ; i != args->end(); i++ ) {
        n_cond++;
    }
    counts = new int[n_cond];
    int_cond = new int[(int)(world->get_param("num_proteins"))];


    /*if(!world->get_output_started2())
    {
        fprintf(world->fileio_list[index],"Time, ");
        for (j=0; j<n_cond-1; j++)
        {
            fprintf(world->fileio_list[index],"Condition %d, ", j);
        }
        fprintf(world->fileio_list[index],"Condition %d\n", j);
        world->set_output_started2(true);
    }*/

    for(j=0; j<n_cond; j++)
    {
        counts[j] = 0;
    }
    j = 0;

    for( i=cds ; i != args->end(); i++ )
    {
        for(k=0; k<(int)(world->get_param("num_proteins")); k++)
        {
            int_cond[k] = 0;
        }

        for(l = (*i)->list_value()->begin(); l != (*i)->list_value()->end(); l++)
        {
            protein_name = (*l)->string_value();
            if(protein_name.at(0) == '-')
            {
                prot_value = -1;
                protein_name.erase(protein_name.begin());
            }
            else
            {
                prot_value = 1;
            }
            prot_index = protein_Map[protein_name];
            int_cond[prot_index] = prot_value;
        }
        //printf("\n");
        counts[j] += world->check_gen_cond_pop(int_cond);
        j++;
    }

    fprintf(world->fileio_list[index],"%f, ", world->get_time());
    for (j=0; j<n_cond-1; j++)
    {
        fprintf(world->fileio_list[index],"%d, ", counts[j]);
    }
    fprintf(world->fileio_list[index],"%d\n", counts[j]);

    fflush (world->fileio_list[index] );

    delete counts;
    delete int_cond;

    return new Value ( Value::UNIT );
}

Value * gro_dump_multiple_plasmids(std::list<Value *> * args, Scope * s){

    World * world = current_gro_program->get_world();
    int n_cond = 0, j = 0, k = 0;
    int *counts, *int_cond;

    std::list<Value *>::iterator i = args->begin();
    std::list<Value *>::iterator cds;
    std::list<Value *>::iterator l;
    std::map<std::string,int> plasmids_Map = world->get_plasmids_map();
    std::string plasmid_name;
    int plasmid_value = 0, plasmid_index = 0;
    int index = (*i)->int_value();
    i++;
    cds = i;


    Value *cond;
    //std::list<Value *>::iterator i = args->begin();
    //std::list<Value *>* cond = (*i)->list_value();

    for (i=cds ; i != args->end(); i++ ) {
        n_cond++;
    }
    counts = new int[n_cond];
    int_cond = new int[(int)(world->get_param("num_plasmids"))];


    /*if(!world->get_output_started2())
    {
        fprintf(world->fileio_list[index],"Time, ");
        for (j=0; j<n_cond-1; j++)
        {
            fprintf(world->fileio_list[index],"Condition %d, ", j);
        }
        fprintf(world->fileio_list[index],"Condition %d\n", j);
        world->set_output_started2(true);
    }*/

    for(j=0; j<n_cond; j++)
    {
        counts[j] = 0;
    }
    j = 0;

    for( i=cds ; i != args->end(); i++ )
    {
        for(k=0; k<(int)(world->get_param("num_plasmids")); k++)
        {
            int_cond[k] = 0;
        }

        for(l = (*i)->list_value()->begin(); l != (*i)->list_value()->end(); l++)
        {
            plasmid_name = (*l)->string_value();
            if(plasmid_name.at(0) == '-')
            {
                plasmid_value = -1;
                plasmid_name.erase(plasmid_name.begin());
            }
            else
            {
                plasmid_value = 1;
            }
            plasmid_index = plasmids_Map[plasmid_name];
            int_cond[plasmid_index] = plasmid_value;
        }
        counts[j] += world->check_plasmid_cond_pop(int_cond);
        j++;
    }

    fprintf(world->fileio_list[index],"%f, ", world->get_time());
    for (j=0; j<n_cond-1; j++)
    {
        fprintf(world->fileio_list[index],"%d, ", counts[j]);
    }
    fprintf(world->fileio_list[index],"%d\n", counts[j]);

    fflush (world->fileio_list[index] );

    delete counts;
    delete int_cond;

    return new Value ( Value::UNIT );
}

Value * gro_dump_left ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    float left;

    left = world->dump_left();
    return new Value ( left );

}

Value * gro_dump_right ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    float right;

    right = world->dump_right();
    return new Value ( right );

}

Value * gro_dump_top ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    float top;

    top = world->dump_top();
    return new Value ( top );

}

Value * gro_dump_bottom ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();
    float bottom;

    bottom = world->dump_bottom();
    return new Value ( bottom );

}

Value * gro_time ( std::list<Value *> * args, Scope * s ) {

    World * world = current_gro_program->get_world();

    return new Value ( world->get_time() );

}

void register_gro_functions ( void ) {

    // Parameters
    register_ccl_function ( "set", set_param );
    register_ccl_function ( "zoom", zoom );

    // Cell types
    register_ccl_function ( "ecoli", new_ecoli );
    register_ccl_function ( "c_ecolis", new_ecolis_random_circle );
    register_ccl_function ( "yeast", new_yeast );

    // Signals
    register_ccl_function ( "signal",        new_signal );  
    //register_ccl_function ( "cf_signal",        new_cross_feeding_signal );
    register_ccl_function ( "set_signal",    set_signal );
    register_ccl_function ( "set_signal_rect", set_signal_rect );
    register_ccl_function ( "get_signal",    get_signal );
    register_ccl_function ( "emit_signal",   emit_signal );
    register_ccl_function ( "emit_signal_conj",   emit_signal_conj );
    register_ccl_function ( "emit_signal_nh",   emit_signal_nh );
    register_ccl_function ( "absorb_signal", absorb_signal );
    register_ccl_function ( "reaction",      add_reaction );

    //Cellsignals
    register_ccl_function ( "s_signal",        s_new_signal );
    //register_ccl_function ( "s_cf_signal",        s_new_cross_feeding_signal );
    register_ccl_function ( "s_set_signal",    s_set_signal );
    register_ccl_function ( "s_set_signal_rect", s_set_signal_rect );
    register_ccl_function ( "s_get_signal",    s_get_signal );
    register_ccl_function ( "s_get_signal_area",    s_get_signal_area );
    register_ccl_function ( "s_emit_signal",   s_emit_signal );
    register_ccl_function ( "s_emit_signal_area",   s_emit_signal_area );
    register_ccl_function ( "s_absorb_signal", s_absorb_signal );
    register_ccl_function ( "s_absorb_signal_area", s_absorb_signal_area );

    // Gene Expression
    register_ccl_function ( "operon",        new_operon );
    register_ccl_function ( "genes",        new_genes );
    register_ccl_function ( "proteins",        what_proteins );
    register_ccl_function ( "rnas",        what_rnas );
    register_ccl_function ( "plasmids",        what_plasmids );
    register_ccl_function ( "riboswitches",        what_riboswitches );
    register_ccl_function ( "molecules",        what_molecules );
    register_ccl_function ( "analog_molecules",        what_analog_molecules );
    register_ccl_function ( "degradation_times",   set_degradation_times);
    register_ccl_function ( "protein_degradation",  new_prot_deg_times);
    register_ccl_function ( "rna_degradation",  new_rna_deg_times);
    register_ccl_function ( "plasmids_matrix",    set_plasmids_matrix );
    register_ccl_function ( "plasmids_genes",    set_plasmids_matrix2 );
    register_ccl_function ( "RNA_matrix",    set_RNA_matrix );
    register_ccl_function ( "RNAs_riboswitches",    set_RNA_matrix2 );
    register_ccl_function ( "molecules_matrix", set_molecules_matrix);
    register_ccl_function ( "molecules_proteins", set_molecules_matrix2);
    register_ccl_function ( "analog_molecules_matrix", set_analog_molecules_matrix);
    register_ccl_function ( "analog_molecules_proteins", set_analog_molecules_matrix2);
    register_ccl_function ( "action",    set_action);
    register_ccl_function ( "reset_actions",    reset_actions);
    register_ccl_function ( "set_molecule", set_molecule);
    register_ccl_function ( "set_analog_molecule", set_analog_molecule);
    register_ccl_function ( "get_analog_molecule", get_analog_molecule);
    register_ccl_function ( "has_protein",   has_protein );
    register_ccl_function ( "has_rna",   has_rna );

    // Conjugation and plasmids
    register_ccl_function ( "conjugate",   conjugate );
    register_ccl_function ( "has_plasmid",   has_plasmid );
    register_ccl_function ( "received_plasmid",   received_plasmid );
    register_ccl_function ( "set_plasmid",   set_plasmid );
    register_ccl_function ( "acquire_plasmid",   acquire_plasmid );
    register_ccl_function ( "lose_plasmid",   lose_plasmid );
    register_ccl_function ( "was_just_conjugated",   was_just_conjugated );
    register_ccl_function ( "has_eex",   has_eex );
    register_ccl_function ( "set_eex",   set_eex );

    // World control
    register_ccl_function ( "reset",          reset );
    register_ccl_function ( "replate",        replate );
    register_ccl_function ( "stop",           stop );
    register_ccl_function ( "start",          start );
    register_ccl_function ( "stats",          world_stats );
    register_ccl_function ( "create_dirs",    create_dirs );
    register_ccl_function ( "snapshot",       snapshot );
    register_ccl_function ( "message",        message );
    register_ccl_function ( "clear_messages", clear_messages );
    register_ccl_function ( "set_theme",      set_theme );
    register_ccl_function ( "dump",           gro_dump );
    register_ccl_function ( "dump_left",           gro_dump_left );
    register_ccl_function ( "dump_right",           gro_dump_right );
    register_ccl_function ( "dump_top",           gro_dump_top );
    register_ccl_function ( "dump_bottom",           gro_dump_bottom );
    register_ccl_function ( "time",           gro_time );
    register_ccl_function ( "select_random_cell",           select_random_cell );

    // Misc
    register_ccl_function ( "die", die );
    register_ccl_function ( "die_c", die_c );
    register_ccl_function ( "divide", force_divide );
    register_ccl_function ( "map_to_cells", map_to_cells );
    register_ccl_function ( "run", run );
    register_ccl_function ( "tumble", tumble );
    register_ccl_function ( "print", gro_print );
    register_ccl_function ( "clear", gro_clear );
    register_ccl_function ( "chemostat", chemostat );

    // File I/O
    register_ccl_function ( "fopen", gro_fopen );
    register_ccl_function ( "fprint", gro_fprint );
    register_ccl_function ( "dump_single", gro_dump_single );
    register_ccl_function ( "dump_multiple", gro_dump_multiple );
    register_ccl_function ( "dump_multiple_plasmids", gro_dump_multiple_plasmids );

    // Growth
    register_ccl_function ( "my_d_vol", delta_vol );
    register_ccl_function ( "available", my_available );

    //Phages
    register_ccl_function ( "local_moi", local_MOI );
}
