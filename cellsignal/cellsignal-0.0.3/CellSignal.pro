SOURCES += \
    src/CellSignal.c \
    src/Grid.c \
    src/Nutrients.c \
    src/vector.c \
    src/CSvector.c

HEADERS += \
    include/CECellSignal.h \
    include/CEGrid.h \
    include/CENutrients.h \
    include/vector.h \
    include/CSvector.h